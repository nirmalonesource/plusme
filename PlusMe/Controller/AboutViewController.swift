//
//  AboutViewController.swift
//  PlusMe
//
//  Created by rohit on 09/08/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit
import SWRevealViewController

class AboutViewController: UIViewController, SWRevealViewControllerDelegate, UIWebViewDelegate
{
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "ABOUT"
        //self.view.showEmptyMessage(message: "coming soon")
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }

        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        menuButton.addTarget(self, action: #selector(someAction(_:)), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        
        let url = Bundle.main.url(forResource: "aboutus", withExtension: "html")
        let request = URLRequest(url: url!)
        
        webView.loadRequest(request)
        webView.delegate = self
        showProgressHud(message: "Loading...")
    
//        let AccButton = UIButton(type: .system)
//        AccButton.setImage(#imageLiteral(resourceName: "home").withRenderingMode(.alwaysOriginal), for: .normal)
//        AccButton.tag = 1
//        AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
//        AccButton.addTarget(self, action: #selector(AccButton(_:)), for: .touchUpInside)
//        let barButtonItem = UIBarButtonItem(customView: AccButton)
//        self.navigationItem.rightBarButtonItems = [barButtonItem]
    }

    @objc func someAction(_ sender:UITapGestureRecognizer){
        // do other task

        //        let home = TabBarViewController()
        //        self.navigationController?.pushViewController(home, animated: true)
        navigationController?.popViewController(animated: true)
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {

        hideProgressHud()
    }

    @objc func AccButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")

        if button.tag == 1
        {
            let home = TabBarViewController()
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    @IBAction func btnMenuAction(_ sender: UIButton) {

//        perform(#selector(SWRevealViewController.revealToggle(_:)))
        sender.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
    }

    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {

            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {

            revealController.frontViewController.view.alpha = 1
            self.view.isUserInteractionEnabled = true
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
