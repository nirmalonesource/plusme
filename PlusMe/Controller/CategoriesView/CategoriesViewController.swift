//
//  CategoriesViewController.swift
//  PlusMe
//
//  Created by Priyank Jotangiya on 23/07/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit
import SWRevealViewController

class CategoriesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, SWRevealViewControllerDelegate, UISearchBarDelegate
{
    
    @IBOutlet weak var collectionView: UICollectionView!
    let icon = ["Model","Model","Model","Model","Model"]
    
    let Title = ["TRENDY","FORMAL","BUSINESS","CASUAL","CLASSIC"]

    var arrCategory = [[String: Any]]()


    var a : Int?
    @IBOutlet var btnmenu: UIBarButtonItem!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
      

        StoredData.shared.getCategory { (result) in
            self.arrCategory.removeAll()
            self.arrCategory = result
            self.arrResult = self.arrCategory

            self.collectionView.reloadData()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "VisionBoardViewController") as! VisionBoardViewController
        nav.resetview()
        
          //NavigationBar
              self.navigationController?.navigationBar.isHidden = false;
              self.tabBarController?.tabBar.isHidden = false
              
              self.title = "Categories"
              
              navigationController?.navigationBar.barTintColor = UIColor.white
              navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
              self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }
              
              let menuButton = UIButton(type: .system)
              menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
              menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
              navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
              
            //  let AccButton = UIButton(type: .system)
              let AccButton = SSBadgeButton()
                           if UserDefaults.standard.object(forKey: "notification") != nil {
                               let noticount = UserDefaults.standard.string(forKey: "notification")
                               AccButton.badge = noticount
                           }
                           else
                           {
                                AccButton.badge = nil
                           }
              AccButton.setImage(#imageLiteral(resourceName: "Notification").withRenderingMode(.alwaysOriginal), for: .normal)
              AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
              AccButton.tag = 1
              AccButton.addTarget(self, action: #selector(AccButton(_:)), for: .touchUpInside)
              let barButtonItem = UIBarButtonItem(customView: AccButton)
              
              let AccButton2 = UIButton(type: .system)
              AccButton2.tag = 1
              AccButton2.setImage(#imageLiteral(resourceName: "Search").withRenderingMode(.alwaysOriginal), for: .normal)
              AccButton2.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
              AccButton2.addTarget(self, action: #selector(AccButton2(_:)), for: .touchUpInside)
              let barButtonItem2 = UIBarButtonItem(customView: AccButton2)
              
              self.navigationItem.rightBarButtonItems = [barButtonItem, barButtonItem2]
              
              //SideBar
              if revealViewController() != nil
              {
                  self.revealViewController().delegate = self
                  menuButton.addTarget(revealViewController(), action: "revealToggle:", for: .touchUpInside)
                  self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                  view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
              }
    }
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//        guard let viewControllers = navigationController?.viewControllers,
//            let index = viewControllers.index(of: self) else { return }
//        navigationController?.viewControllers.remove(at: index)
//    }
    
    //MARK: - Actions👊🏻😠
    
    @objc func AccButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")

        if button.tag == 1
        {
            let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        
    }
    
    @objc func AccButton2(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        if button.tag == 1
        {
            navigationItem.rightBarButtonItems = nil

            let searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 200, height: 20))
            searchBar.placeholder = "Search"
            searchBar.delegate = self
            navigationItem.titleView = searchBar
            title = nil

            let menuButton = UIButton(type: .system)
            menuButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
            menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
            menuButton.addTarget(self, action: #selector(searchAction(_:)), for: .touchUpInside)
        }
    }

    @objc func searchAction(_ button: UIButton) {

        navigationItem.titleView = nil
        title = "Categories"

        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)

        //SideBar
        if revealViewController() != nil
        {
            self.revealViewController().delegate = self
            menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
  self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

        let searchButton = UIButton(type: .system)
        searchButton.setImage(#imageLiteral(resourceName: "Search").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.tag = 1
        searchButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        searchButton.addTarget(self, action: #selector(AccButton2(_:)), for: .touchUpInside)
        let barButtonItem = UIBarButtonItem(customView: searchButton)

     //   let AccButton = UIButton(type: .system)
        let AccButton = SSBadgeButton()
                     if UserDefaults.standard.object(forKey: "notification") != nil {
                         let noticount = UserDefaults.standard.string(forKey: "notification")
                         AccButton.badge = noticount
                     }
                     else
                     {
                          AccButton.badge = nil
                     }
        AccButton.setImage(#imageLiteral(resourceName: "Notification").withRenderingMode(.alwaysOriginal), for: .normal)
        AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        AccButton.tag = 1
        AccButton.addTarget(self, action: #selector(AccButton(_:)), for: .touchUpInside)
        let barButtonItem1 = UIBarButtonItem(customView: AccButton)

        self.navigationItem.rightBarButtonItems = [barButtonItem1, barButtonItem]

        arrResult = arrCategory
        collectionView.reloadData()
    }
    
    //MARK: - Methods(Functions)☝🏻😳
    
    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {
            
            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            
            revealController.frontViewController.view.alpha = 1
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        // get a reference to our storyboard cell
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: "item", for: indexPath as IndexPath) as! ClosetImagesCollectionView
        
        let flow = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        let itemSpacing: CGFloat = 5
        let itemsInOneLine: CGFloat = 5
        flow.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5)
        let width = UIScreen.main.bounds.size.width - itemSpacing * CGFloat(itemsInOneLine - 1)
        flow.itemSize = CGSize(width: floor(width/itemsInOneLine), height: width/itemsInOneLine)
        flow.minimumInteritemSpacing = 5
        flow.minimumLineSpacing = 3

        let record = arrResult[indexPath.row]
        item.Lbl_ClosetTitle.isHidden = false
        
        item.Lbl_ClosetTitle.text = record.getString(key: "CategoryName")
        
        item.ClosetImg.setImage(record.getString(key: "CategoryImageUrl"))
        
        return item
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        let record = arrResult[indexPath.row]
        StoredData.shared.selectedCateogryId = record.getInt(key: "CategoryID")
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "MyFeedViewController") as! MyFeedViewController
        nav.categoryName = record.getString(key: "CategoryName")
        nav.CateogryId = record.getInt(key: "CategoryID")
        nav.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(nav, animated: true)
        
       /*
        let record = arrResult[indexPath.row]
        StoredData.shared.selectedCateogryId = record.getInt(key: "CategoryID")
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        nav.categoryName = record.getString(key: "CategoryName")
        navigationController?.pushViewController(nav, animated: true)
      */
        
//        StoredData.shared.selectedCateogryIndex = indexPath.row
//        tabBarController?.selectedIndex = 2
//
//        performSegue(withIdentifier: "segueCategory", sender: record.getInt(key: "CategoryID"))
        
//        let home = TabBarViewController()
//        home.setUI()
//        self.navigationController?.pushViewController(home, animated: false)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueCategory"
        {
          // let home = segue.destination as! HomeViewController
          
        }
    }
    
    //collectionViewLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.row == 0 ||  indexPath.row == 1 || indexPath.row == 2 {
            let width = UIScreen.main.bounds.width
            return CGSize(width: (width - 21)/3, height: (width - 21)/3) // width & height are the same to make a square cell
        }
        else{
            let width = UIScreen.main.bounds.width
            return CGSize(width: (width - 15)/2, height: (width - 15)/2) // width & height are the same to make a square cell
        }
    }

    var arrResult = [[String: Any]]()
    //MARK: - Searchbar Delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        arrResult = arrCategory.filter { (dict) -> Bool in

            if dict.getString(key: "CategoryName").lowercased().contains(searchText.lowercased())
            {
                return true
            }
            return false
        }
        print(arrResult)
        if searchText == ""
        {
            arrResult = arrCategory
        }
        collectionView.reloadData()
    }
}
