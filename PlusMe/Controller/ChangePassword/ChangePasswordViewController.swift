//
//  ChangePasswordViewController.swift
//  PlusMe
//
//  Created by Priyank Jotangiya on 23/07/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit
import SWRevealViewController

class ChangePasswordViewController: UIViewController, SWRevealViewControllerDelegate {

    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet var Vw_Cancel: UIView!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtCurrentPassword: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        self.navigationController?.navigationBar.isHidden = true

        let gesture = UITapGestureRecognizer(target: self, action: #selector(ChangePasswordViewController.someAction(_:)))
        // or for swift 2 +
        _ = UITapGestureRecognizer(target: self, action:  #selector (self.someAction (_:)))
        Vw_Cancel.addGestureRecognizer(gesture)

        txtCurrentPassword.ChangePlaceholderColor(placeholder: "Old Password")
        txtNewPassword.ChangePlaceholderColor(placeholder: "New Password")
        txtConfirmPassword.ChangePlaceholderColor(placeholder: "Confirm New Password")
        btnChangePassword.roundCorner()


        self.title = "CHANGE PASSWORD"

        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }

        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        menuButton.addTarget(self, action: #selector(someAction(_:)), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)


//        let AccButton = UIButton(type: .system)
//        AccButton.setImage(#imageLiteral(resourceName: "home").withRenderingMode(.alwaysOriginal), for: .normal)
//        AccButton.tag = 1
//        AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
//        AccButton.addTarget(self, action: #selector(AccButton(_:)), for: .touchUpInside)
//        let barButtonItem = UIBarButtonItem(customView: AccButton)
//        self.navigationItem.rightBarButtonItems = [barButtonItem]
    }


    @objc func AccButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")

        if button.tag == 1
        {
            let home = TabBarViewController()
            self.navigationController?.pushViewController(home, animated: true)
        }
    }

    // or for Swift 4
    @objc func someAction(_ sender:UITapGestureRecognizer){
        // do other task
        
//        let home = TabBarViewController()
//        self.navigationController?.pushViewController(home, animated: true)
        navigationController?.popViewController(animated: true)
    }

    @IBAction func ChangeButtonPressed(_ sender: Any) {
        if validateData()
        {
            view.endEditing(true)

            let param = [
                "OldPassword": txtCurrentPassword.text!,
                "NewPassword": txtNewPassword.text!
            ]
            callApi(Path.changePassword, method: .post, param: param) { (result) in

                print(result)
                if result.getBool(key: "success")
                {
                    self.txtCurrentPassword.text = ""
                    self.txtNewPassword.text = ""
                    self.txtConfirmPassword.text = ""

                   self.showMessage(message: result.getString(key: "message"), completionHandler: {
                        StoredData.shared.rearViewController.logout()
                    })
                }
                else
                {
                    self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                }
            }
        }
    }

    fileprivate func validateData() -> Bool
    {
        if txtCurrentPassword.text!.isEmpty()
        {
            showMessage(message: "Please enter old password.", completionHandler: nil)
            return false
        }
        else if txtNewPassword.text!.isEmpty()
        {
            showMessage(message: "Please enter new password.", completionHandler: nil)
            return false
        }
        else if txtConfirmPassword.text!.isEmpty()
        {
            showMessage(message: "Please re-type the password.", completionHandler: nil)
            return false
        }
        else if txtNewPassword.text! != txtConfirmPassword.text!
        {
            showMessage(message: "Re-typed password does not match with new password.", completionHandler: nil)
            return false
        }
        return true
    }

    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {

            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {

            revealController.frontViewController.view.alpha = 1
            self.view.isUserInteractionEnabled = true
        }
    }
}
