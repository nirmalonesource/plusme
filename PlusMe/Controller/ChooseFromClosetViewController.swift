//
//  ChooseFromClosetViewController.swift
//  PlusMe
//
//  Created by rohit on 02/10/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit

class ChooseFromClosetViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!

    var arrClosets = [[String: Any]]()
    var delegate: ClosetImages?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "SELECT CLOSET"
        self.navigationController?.navigationBar.isHidden = false;

        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }

        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        menuButton.addTarget(self, action: #selector(AccButton1(_:)), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        // Do any additional setup after loading the view.
        collectionView.delegate = self
        collectionView.dataSource = self

        loadMyClosets()
    }
    @objc func AccButton1(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        navigationController?.popViewController(animated: true)
    }

    func loadMyClosets()
    {
        arrClosets.removeAll()

        callApi(Path.myClosetList, method: .post) { (result) in

            print(result)
            if result.getBool(key: "success")
            {
                self.arrClosets = result.getArrayofDictionary(key: "data")
            }
            else
            {
                self.collectionView.showEmptyMessage(message: result.getString(key: "message"))
            }
            self.collectionView.reloadData()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrClosets.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: "item", for: indexPath) as! ClosetImagesCollectionView

        item.ClosetImg.setImage(arrClosets[indexPath.row].getString(key: "ClosetImage"))

        return item
    }


    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        delegate?.selectedUrl(url: arrClosets[indexPath.row].getString(key: "ClosetImage"), id: arrClosets[indexPath.row].getInt(key: "ClosetID"))
        navigationController?.popViewController(animated: true);
        
    }

    //collectionViewLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = UIScreen.main.bounds.width
        return CGSize(width: width / 3 - 10, height: width / 3 - 10) // width & height are the same to make a square cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


protocol ClosetImages {
    func selectedUrl(url: String, id: Int)
}
