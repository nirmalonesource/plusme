//
//  CropingImageController.swift
//  PlusMe
//
//  Created by My Mac on 21/02/19.
//  Copyright © 2019 Priyank Jotangiya. All rights reserved.
//

import UIKit
import CoreGraphics
//import ZImageCropper
//import AlamofireImage

//protocol CropImageDelegate {
//    func cropImage(cropImage: UIImage,ShapLayer: CAShapeLayer)
//}
//
//class CropingImageController: UIViewController {
//    @IBOutlet weak var CropingImgOutlet: ZImageCropperView!
//    @IBOutlet weak var btnCrop: UIButton!
//
//    var delegate: CropImageDelegate?
//      var isCrop = false
//      var crImage = UIImage()
//    var GetCropImage = UIImageView()
//    var isRotation = Bool()
//    var shapeLayer1 = CAShapeLayer()
//
//    override func viewDidLoad() {
//           super.viewDidLoad()
//           StoredData.shared.closetImage = UIImage()
//           //btnCrop.setTitleColor(UIColor.gray, for: .disabled)
//        btnCrop.isEnabled = true
//        btnCrop.isUserInteractionEnabled = true
//           if isCrop {
//               CropingImgOutlet.image = crImage
//               let rotationGesture = UIRotationGestureRecognizer(target: self, action:#selector(self.rotationGesture(sender:)))
//               CropingImgOutlet.isUserInteractionEnabled = true
//               CropingImgOutlet.addGestureRecognizer(rotationGesture)
//           }
//
//           else {
//              CropingImgOutlet.image = GetCropImage.image
//           }
//
//           let rotationGesture = UIRotationGestureRecognizer(target: self, action:#selector(self.rotationGesture(sender:)))
//           CropingImgOutlet.isUserInteractionEnabled = true
//           CropingImgOutlet.addGestureRecognizer(rotationGesture)
//       }
//
//        @objc func rotationGesture(sender:UIRotationGestureRecognizer)
//        {
//            isRotation = true
//            sender.view?.transform = ((sender.view?.transform)?.rotated(by: sender.rotation))!
//            sender.rotation = 0
//        }
//
//    @IBAction func btnCroppingAction(_ sender: UIButton) {
//   //        shapeLayer1.fillColor = UIColor.black.cgColor
//   //        CropingImgOutlet.layer.mask = shapeLayer1
//   //    }
//        let  croppedImage = CropingImgOutlet.cropImage()
//        delegate?.cropImage(cropImage: croppedImage ?? UIImage(), ShapLayer: shapeLayer1)
//        self.navigationController?.popViewController(animated: true)
//}
//
//    }






protocol CropImageDelegate {
    func cropImage(cropImage: UIImage,ShapLayer: CAShapeLayer)
}

class CropingImageController: UIViewController {

    @IBOutlet weak var CropingImgOutlet: UIImageView!
    var GetCropImage = UIImageView()
    var shapeLayer1 = CAShapeLayer()
    
    @IBOutlet weak var btnCrop: UIButton!
    var delegate: CropImageDelegate?
    var isCrop = false
    var crImage = UIImage()
    
    //Update this for path line color
    let strokeColor:UIColor = UIColor.red
    
    //Update this for path line width
    let lineWidth:CGFloat = 2.0
     var touchPoint = CGPoint()
    var path = UIBezierPath()
    var croppedImage = UIImage()
    var isRotation = Bool()
    
    //MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        StoredData.shared.closetImage = UIImage()
        //btnCrop.setTitleColor(UIColor.gray, for: .disabled)
        
        if isCrop {
            CropingImgOutlet.image = crImage
            let rotationGesture = UIRotationGestureRecognizer(target: self, action:#selector(self.rotationGesture(sender:)))
            CropingImgOutlet.isUserInteractionEnabled = true
            CropingImgOutlet.addGestureRecognizer(rotationGesture)
        }
            
        else {
           CropingImgOutlet.image = GetCropImage.image
        }
    
        let rotationGesture = UIRotationGestureRecognizer(target: self, action:#selector(self.rotationGesture(sender:)))
        CropingImgOutlet.isUserInteractionEnabled = true
        CropingImgOutlet.addGestureRecognizer(rotationGesture)
    }
    
    @objc func rotationGesture(sender:UIRotationGestureRecognizer)
    {
        isRotation = true
        sender.view?.transform = ((sender.view?.transform)?.rotated(by: sender.rotation))!
        sender.rotation = 0
    }
    
    //
    //MARK:- Touch event methods
    //
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !isRotation
        {
            if let touch = touches.first as UITouch?{
               touchPoint = touch.location(in: self.CropingImgOutlet)
                print("touch begin to : \(touchPoint)")
                path.move(to: touchPoint)
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !isRotation
        {
            if let touch = touches.first as UITouch?{
                 touchPoint = touch.location(in: self.CropingImgOutlet)
                print("touch moved to : \(touchPoint)")
                path.addLine(to: touchPoint)
                addNewPathToImage()
            }
            btnCrop.isEnabled = true
            btnCrop.isUserInteractionEnabled = true
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !isRotation
        {
            if let touch = touches.first as UITouch?{
                 touchPoint = touch.location(in: self.CropingImgOutlet)
                print("touch ended at : \(touchPoint)")
                path.addLine(to: touchPoint)
                addNewPathToImage()
                path.close()
            }
        }
        
//         btnCrop.isEnabled = true
//         btnCrop.isUserInteractionEnabled = true
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !isRotation
        {
            if let touch = touches.first as UITouch?{
                touchPoint = touch.location(in: self.CropingImgOutlet)
                print("touch canceled at : \(touchPoint)")
                path.close()
            }
        }
    }
    
    func addNewPathToImage() {
        shapeLayer1.path = path.cgPath
        shapeLayer1.strokeColor = strokeColor.cgColor
        shapeLayer1.fillColor = UIColor.clear.cgColor
        shapeLayer1.lineWidth = lineWidth
        CropingImgOutlet.layer.addSublayer(shapeLayer1)
    }
    
    @IBAction func CropingBtnPressed(_ sender: UIButton) {
        shapeLayer1.fillColor = UIColor.black.cgColor
        CropingImgOutlet.layer.mask = shapeLayer1
    }
    
    @IBAction func btnCroppingAction(_ sender: UIButton) {
            shapeLayer1.fillColor = UIColor.black.cgColor
            CropingImgOutlet.layer.mask = shapeLayer1
            
            // print(CropingImgOutlet.image?.cropAlpha())
        
           // delegate?.cropImage(cropImage: GetCropImage.image ?? UIImage(), ShapLayer: shapeLayer1)
         delegate?.cropImage(cropImage: CropingImgOutlet.image ?? UIImage(), ShapLayer: shapeLayer1)
            self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func DeletePressed(_ sender: UIButton) {
        btnCrop.isEnabled = false
        btnCrop.isUserInteractionEnabled = false
        isRotation = false
        shapeLayer1.removeFromSuperlayer()
        CropingImgOutlet.layer.sublayers?.removeAll(keepingCapacity: true)
        path = UIBezierPath()
        shapeLayer1 = CAShapeLayer()
        addNewPathToImage()
    }
   
    /*
     var imageView : UIImageView
     imageView  = UIImageView(frame:CGRectMake(10, 50, 100, 300));
     imageView.image = UIImage(named:"image.jpg")
     self.view.addSubview(imageView)
     viewMain.addSubview(myCustomView!)
     
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
}

//UIImageView

class DraggedImageView: UIView {
    
    var startLocation: CGPoint?
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        startLocation = touches.first?.location(in: self)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let currentLocation = touches.first?.location(in: self)
        let dx = currentLocation!.x - startLocation!.x
        let dy = currentLocation!.y - startLocation!.y
        
        let coolArea = (self.superview?.bounds)!
        
        let newCenter = CGPoint(x: self.center.x+dx, y: self.center.y+dy)
        
        // If the allowed area contains the new point, we can assign it
        if coolArea.contains(newCenter) {
            self.center = newCenter
            print("touchesMoved")
            
        }
        else {
            print("Out of boundaries!")
        }
    }
}

extension UIImage {
    
    func cropAlpha() -> UIImage {
        
        let cgImage = self.cgImage!;
        
        let width = cgImage.width
        let height = cgImage.height
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bytesPerPixel:Int = 4
        let bytesPerRow = bytesPerPixel * width
        let bitsPerComponent = 8
        let bitmapInfo: UInt32 = CGImageAlphaInfo.premultipliedLast.rawValue | CGBitmapInfo.byteOrder32Big.rawValue
        
        guard let context = CGContext(data: nil, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo),
            let ptr = context.data?.assumingMemoryBound(to: UInt8.self) else {
                return self
        }
        
        context.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: width, height: height))
        
        var minX = width
        var minY = height
        var maxX: Int = 0
        var maxY: Int = 0
        
        for x in 1 ..< width {
            for y in 1 ..< height {
                
                let i = bytesPerRow * Int(y) + bytesPerPixel * Int(x)
                let a = CGFloat(ptr[i + 3]) / 270.0  //255.0
                
                if(a>0) {
                    if (x < minX) { minX = x };
                    if (x > maxX) { maxX = x };
                    if (y < minY) { minY = y};
                    if (y > maxY) { maxY = y};
                }
            }
        }
        
        let rect = CGRect(x: CGFloat(minX),y: CGFloat(minY), width: CGFloat(maxX-minX), height: CGFloat(maxY-minY))
        let imageScale:CGFloat = self.scale
        let croppedImage = self.cgImage!.cropping(to: rect)!
        let ret = UIImage(cgImage: croppedImage, scale: imageScale, orientation: self.imageOrientation)
        
        return ret;
    }
}
