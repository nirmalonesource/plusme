//
//  DesignerViewController.swift
//  PlusMe
//
//  Created by rohit on 11/09/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit
import SWRevealViewController
import ActionSheetPicker_3_0

class filterTableCell: UITableViewCell {

    @IBOutlet weak var btnOutlet: UIButton!
    
}

class DesignerViewController: UIViewController, SWRevealViewControllerDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imgBlur: UIImageView!
    
    @IBOutlet weak var ViewLeading: NSLayoutConstraint!
    
    @IBOutlet weak var VwSecondOutlet: UIView!
    @IBOutlet weak var lblLineZip: UILabel!
    
    var type = 1
    var arrResults = [[String: Any]]()
    var countryResults = [[String: Any]]()
    var stateResults = [[String: Any]]()
    var CityResults = [[String: Any]]()

    var FilterArray = [String]()
    
    var CountyId , StateID , CityID: String?
    
    @IBOutlet weak var lblCountry: UILabel!
    
    @IBOutlet weak var lblState: UILabel!
    
    @IBOutlet weak var lblCity: UILabel!
    
    @IBOutlet weak var lblZip: UILabel!
    @IBOutlet weak var txtZipView: UITextField!
    
    //MARK:- BUTTON ACTION
    @IBAction func btnCountryAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if countryResults.count != 0
        {
            ActionSheetStringPicker.show(withTitle: "SELECT COUNTRY", rows: countryResults.map({ $0["Name"] as? String ?? ""}) as [Any], initialSelection: 0, doneBlock: {
                picker, indexes, values in
               // button.setTitle("\(values!)", for: .normal)
                self.lblCountry.text = "\(values!)"
                self.CountyId = self.countryResults[indexes]["Id"] as? String ?? ""
                self.GetState()
                return
            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        }
    }
    
    @IBAction func BtnStateAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if stateResults.count != 0
        {
            ActionSheetStringPicker.show(withTitle: "SELECT STATE", rows: stateResults.map({ $0["Name"] as? String ?? ""}) as [Any], initialSelection: 0, doneBlock: {
                picker, indexes, values in
                //button.setTitle("\(values!)", for: .normal)
                self.lblState.text = "\(values!)"
                
                self.StateID = self.stateResults[indexes]["Id"] as? String ?? ""
                
                return
            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        }
    }
    
    @IBAction func btnCityAction(_ sender: UIButton) {
         self.view.endEditing(true)
        let param: [String: Any] = [
            "StateID": StateID ?? 0
        ]
        callApi(Path.getCity, method: .post, param: param) { (result) in
            
            print(result)
            
            if result.getBool(key: "status")
            {
                self.CityResults = result.getArrayofDictionary(key: "data")
                
                if self.CityResults.count != 0
                {
                    ActionSheetStringPicker.show(withTitle: "SELECT CITY", rows: self.CityResults.map({ $0["Name"] as? String ?? ""}) as [Any], initialSelection: 0, doneBlock: {
                        picker, indexes, values in
                        // button.setTitle("\(values!)", for: .normal)
                        self.lblCity.text = "\(values!)"
                        self.CityID = self.CityResults[indexes]["Id"] as? String ?? ""
                        return
                    }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
                }
                self.txtZipView.text = ""
            }
        }
        
    }
    
    //MARK:- VIEW LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        VwSecondOutlet.isHidden = true
        
        txtZipView.attributedPlaceholder = NSAttributedString(string: "ZIP CODE",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        lblLineZip.isHidden = true
        GetCountry()
        
        FilterArray = ["SELECT COUNTRY","SELECT STATE","SELECT CITY","ZIP CODE"]
        
        // Do any additional setup after loading the view.
        if type == 1
        {
            self.title = "Designers/Stylists".capitalized
        }
        else
        {
            self.title = "Trendsetters".capitalized
        }

        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }

        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)

        //SideBar
        if revealViewController() != nil
        {
            self.revealViewController().delegate = self
            menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
       self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
             view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

        let HomeButton = UIButton(type: .system)
        HomeButton.setImage(#imageLiteral(resourceName: "home").withRenderingMode(.alwaysOriginal), for: .normal)
        HomeButton.tag = 1
        HomeButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        HomeButton.addTarget(self, action: #selector(HomeButton(_:)), for: .touchUpInside)
        let barButtonItem = UIBarButtonItem(customView: HomeButton)

        let SearchButton = UIButton(type: .system)
        SearchButton.setImage(#imageLiteral(resourceName: "Search").withRenderingMode(.alwaysOriginal), for: .normal)
        SearchButton.tag = 1
        SearchButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        SearchButton.addTarget(self, action: #selector(SearchButton(_:)), for: .touchUpInside)
        let barButtonItem1 = UIBarButtonItem(customView: SearchButton)
        
        let FilterButton = UIButton(type: .system)
        FilterButton.setImage(#imageLiteral(resourceName: "filter-icon").withRenderingMode(.alwaysOriginal), for: .normal)
        FilterButton.tag = 1
        FilterButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        FilterButton.addTarget(self, action: #selector(FilterButton(_:)), for: .touchUpInside)
        let barButtonItem2 = UIBarButtonItem(customView: FilterButton)

        self.navigationItem.rightBarButtonItems = [barButtonItem2, barButtonItem ,barButtonItem1]
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
//               cell.Blog_list_lbl_Banner.setImage(record.getString(key: "PostImage"))
//               let tapGesture = UITapGestureRecognizer(target: self, action: #selector(gesterHandeler(_:)))
//                cell.Blog_list_lbl_Banner.isUserInteractionEnabled = true
//               cell.Blog_list_lbl_Banner.tag = indexPath.row
//                cell.Blog_list_lbl_Banner.addGestureRecognizer(tapGesture)
        
         let tapGesture = UITapGestureRecognizer(target: self, action: #selector(gesterHandeler(_:)))
         imgBlur.isUserInteractionEnabled = true
         imgBlur.addGestureRecognizer(tapGesture)
    }
    
     //MARK:- TOUCHES BEGAN
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        hideViewWithAnimation(vw: VwSecondOutlet, img: imgBlur)
//
//    }
    
    //MARK:- GESTURE HANDLER
    @objc func gesterHandeler(_ sender: UITapGestureRecognizer)
    {
         VwSecondOutlet.isHidden = true
         imgBlur.isHidden = true
        //        let row = sender.view!.tag
        //        imgZoomImage.setImage(arrResult[row].getString(key: "PostImage"))
        //        viewZoomPopup.isHidden = false
        
    }
    
     @objc func FilterButton(_ button: UIButton) {
        if button.tag == 1
        {
            imgBlur.isHidden = false
           self.VwSecondOutlet.isHidden = !VwSecondOutlet.isHidden
            
        }
    }

    @objc func SearchButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")

        if button.tag == 1
        {
            navigationItem.rightBarButtonItems = nil

            let searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 200, height: 20))
            searchBar.placeholder = "Search"
            searchBar.delegate = self
            navigationItem.titleView = searchBar
            title = nil

            let menuButton = UIButton(type: .system)
            menuButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
            menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
            menuButton.addTarget(self, action: #selector(searchAction(_:)), for: .touchUpInside)
            
           
        }
    }

    @objc func searchAction(_ button: UIButton) {

        navigationItem.titleView = nil
        if type == 1
        {
            self.title = "Designers/Stylists".capitalized
        }
        else
        {
            self.title = "Trendsetters".capitalized
        }
        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)

        //SideBar
        if revealViewController() != nil
        {
            self.revealViewController().delegate = self
            menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

        let HomeButton = UIButton(type: .system)
        HomeButton.setImage(#imageLiteral(resourceName: "home").withRenderingMode(.alwaysOriginal), for: .normal)
        HomeButton.tag = 1
        HomeButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        HomeButton.addTarget(self, action: #selector(HomeButton(_:)), for: .touchUpInside)
        let barButtonItem = UIBarButtonItem(customView: HomeButton)

        let SearchButton = UIButton(type: .system)
        SearchButton.setImage(#imageLiteral(resourceName: "Search").withRenderingMode(.alwaysOriginal), for: .normal)
        SearchButton.tag = 1
        SearchButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        SearchButton.addTarget(self, action: #selector(SearchButton(_:)), for: .touchUpInside)
        let barButtonItem1 = UIBarButtonItem(customView: SearchButton)
        
        let FilterButton = UIButton(type: .system)
                          FilterButton.setImage(#imageLiteral(resourceName: "filter-icon").withRenderingMode(.alwaysOriginal), for: .normal)
                          FilterButton.tag = 1
                          FilterButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
                          FilterButton.addTarget(self, action: #selector(FilterButton(_:)), for: .touchUpInside)
                          let barButtonItem2 = UIBarButtonItem(customView: FilterButton)

        self.navigationItem.rightBarButtonItems = [barButtonItem2,barButtonItem1, barButtonItem]

        arrResult = arrResults
        tableView.reloadData()
    }

    var arrResult = [[String: Any]]()
    //MARK: - Searchbar Delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        arrResult = arrResults.filter { (dict) -> Bool in

            if "\(dict.getString(key: "FirstName")) \(dict.getString(key: "LastName"))".lowercased().contains(searchText.lowercased())
            {
                return true
            }
            return false
        }
        
        print(arrResult)
        if searchText == ""
        {
            arrResult = arrResults
        }
        tableView.reloadData()
    }

    @IBAction func btnRefressAction(_ sender: UIButton) {
        lblCountry.text = "SELECT COUNTRY"
        lblState.text = "SELECT STATE"
        lblCity.text = "SELECT CITY"
        txtZipView.text = "ZIP CODE"
        
        CountyId = ""
        StateID = ""
        CityID = ""
        
         GetTrendsetrs()
    }
    
    @IBAction func btnRightClicked(_ sender: UIButton) {
        if type == 1
        {
            self.title = "Designers/Stylists".capitalized
        }
        else
        {
            self.title = "Trendsetters".capitalized
        }
        
        
        if CountyId == "" && StateID == "" && CityID == "" {
            GetTrendsetrs()
        }
        else
        {
            filterTrendsetrs()
        }
        
          
        
    }
    
    func getStylist()
    {
        callApi(Path.getStylists, method: .post) { (result) in

            print(result)
            if result.getBool(key: "status")
            {
                self.arrResults = result.getArrayofDictionary(key: "data")
                self.arrResult = self.arrResults
            }
            self.tableView.reloadData()
        }
    }

    func GetTrendsetrs()
    {
        callApi(Path.getTrendsetrs, method: .post) { (result) in

            print(result)
            if result.getBool(key: "status")
            {
                self.arrResults = result.getArrayofDictionary(key: "data")
                self.arrResult = self.arrResults
            }
            self.tableView.reloadData()
            self.VwSecondOutlet.isHidden = true
        }
    }
    
//    let param: [String: Any] = [
//        "CountryID": 231 ,
//        "StateID": 3919 ,
//        "CityID": 42594 ,
//        "ZipCode": 7382882
//    ]
    
    func filterTrendsetrs()
    {
//        if arrResult.count > 0
//        {
//            arrResult.removeAll()
//        }
       
        let param: [String: Any] = [
            "CountryID": CountyId ?? "" ,
            "StateID": StateID ?? "",
            "CityID": CityID ?? "" ,
            "ZipCode": txtZipView.text!
        ]
        
        callApi(Path.getTrendsetrs, method: .post, param: param) { (result) in
            
            print(result)
        
             if result.getBool(key: "status")
             {
                self.arrResults = result.getArrayofDictionary(key: "data")
                self.arrResult = self.arrResults
             }
            
             var message = result.getString(key: "message")
            
              if message == "Trendsetter List Not Found" {
                
                if self.type == 1 {
                    message = "Designer List Not Found"
                }
                
//                let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height))
//                noDataLabel.text = "\(message)."
//                noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
//                noDataLabel.textAlignment = .center
//                self.tableView.backgroundView = noDataLabel
                }
            
            //self.tableView.pullToRefreshView.stopAnimating()
            self.VwSecondOutlet.isHidden = true
            self.tableView.reloadData()
        }
    }
    
    func GetCountry()
    {
        callApi(Path.getCountry, method: .get) { (result) in
            
            print(result)
            if result.getBool(key: "status")
            {
                self.countryResults = result.getArrayofDictionary(key: "data")
                //print("countryResults: \(self.countryResults)")
            }
        }
    }
    
    func GetState()
    {
        let param: [String: Any] = [
            "CountryID": CountyId ?? 0
            ]
        callApi(Path.getStates, method: .post, param: param) { (result) in
            
            print(result)
        
            if result.getBool(key: "status")
            {
                self.stateResults = result.getArrayofDictionary(key: "data")
            }
        }
    }
    
    func GetCity()
    {
        let param: [String: Any] = [
            "StateID": StateID ?? 0
        ]
        callApi(Path.getCity, method: .post, param: param) { (result) in
            
            print(result)
            
            if result.getBool(key: "status")
            {
                self.CityResults = result.getArrayofDictionary(key: "data")
                self.txtZipView.isHidden = true
                self.lblLineZip.isHidden = true
                self.txtZipView.text = ""
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if type == 1
        {
            getStylist()
        }
        else
        {
            GetTrendsetrs()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func HomeButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")

        if button.tag == 1
        {
            let home = TabBarViewController()
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    

    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {

            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            
        } else {
            revealController.frontViewController.view.alpha = 1
            self.view.isUserInteractionEnabled = true
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
             return arrResult.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//          if tableView == tblFilter {
//              let cell = tableView.dequeueReusableCell(withIdentifier: "filterTableCell", for: indexPath) as! filterTableCell
//            // cell.lblName.text = FilterArray[indexPath.row]
//            cell.btnOutlet.setTitle("\(FilterArray[indexPath.row])", for: .normal)
//
//            cell.btnOutlet.tag = indexPath.row
//
//            cell.btnOutlet.addTarget(self, action: #selector(btnAction(_:)), for: .touchUpInside)
//
////            if indexPath.row == 3 {
////                cell.btnOutlet.isHidden = true
////            }
//               return cell
//           }
//
//      else {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DesignerCell
            cell.imgPhoto.roundView()
            
            let record = arrResult[indexPath.row]
            addGesture(cell.imgPhoto, userId: record.getInt(key: "Id"))
            cell.imgPhoto.setImage(record.getString(key: "UserProfileImage"))
            cell.lblName.text = "\(record.getString(key: "FirstName")) \(record.getString(key: "LastName"))"
            addGesture(cell.lblName, userId: record.getInt(key: "Id"))
            cell.lblCompany.text = record.getString(key: "CompanyName")
            cell.lblCountry.text = record.getString(key: "CityName")
            cell.selectionStyle = .none
            return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            let record = arrResult[indexPath.row]
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            vc.userId = "\(record.getInt(key: "Id"))"
            navigationController?.pushViewController(vc, animated: true)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class DesignerCell: UITableViewCell {
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
}

