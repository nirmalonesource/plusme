//
//  EditProfileViewController.swift
//  PlusMe
//
//  Created by rohit on 09/08/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    var userData = [String: Any]()
    var isEdit = false
    var arrCountry = [[String: Any]]()
    var arrStates = [[String: Any]]()
    var arrCity = [[String: Any]]()
    var pickerView: UIPickerView!

    @IBOutlet weak var txtFirstname: UITextField!
    @IBOutlet weak var txtLastname: UITextField!
    @IBOutlet weak var txtCompanyName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtContacts: UITextField!
    @IBOutlet weak var txtWorkHours: UITextField!
    @IBOutlet weak var txtFacebook: UITextField!
    @IBOutlet weak var txtInstagram: UITextField!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblUserType: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtPinCode: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        txtCity.isEnabled = false
        txtState.isEnabled = false
        txtCountry.delegate = self
        txtState.delegate = self
        txtCity.delegate = self

        pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
      //  txtCountry.inputView = pickerView
        txtState.inputView = pickerView
        txtCity.inputView = pickerView
        getCountry()

        // Do any additional setup after loading the view.
        imgProfile.roundView()
        self.navigationController?.navigationBar.isHidden = false;
        self.title = "MY PROFILE"

        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }

        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        menuButton.addTarget(self, action: #selector(AccButton1(_:)), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)

        if isEdit
        {
            let AccButton = UIButton(type: .system)
            AccButton.setTitle("Save", for: .normal)
            AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
            AccButton.addTarget(self, action: #selector(AccButton2(_:)), for: .touchUpInside)
            let barButtonItem = UIBarButtonItem(customView: AccButton)
            self.navigationItem.rightBarButtonItems = [barButtonItem]
        }

        btnCamera.isHidden = !isEdit

        setValue()
        
        txtContacts.delegate = self
        getState()
        getCity()
    }

    func getState()
    {
        let param = [
            "CountryID": txtCountry.tag
        ]
        callApi(Path.getStates, method: .post, param: param) { (result) in

            print(result)
            if result.getBool(key: "status")
            {
                self.arrStates = result.getArrayofDictionary(key: "data")
                self.txtState.isEnabled = true
                self.pickerView.reloadAllComponents()
            }
        }
    }

    func getCity()
    {
        let param = [
            "StateID": txtState.tag
        ]
        callApi(Path.getCity, method: .post, param: param) { (result) in

            print(result)
            if result.getBool(key: "status")
            {
                self.arrCity = result.getArrayofDictionary(key: "data")
                self.txtCity.isEnabled = true
                self.pickerView.reloadAllComponents()
            }
        }
    }

    func getCountry()
    {
        callApi(Path.getCountry, method: .get) { (result) in

            print(result)
            if result.getBool(key: "status")
            {
                self.arrCountry = result.getArrayofDictionary(key: "data")
                self.pickerView.reloadAllComponents()
            }
        }
    }

    func setValue()
    {
        txtFirstname.isEnabled = isEdit
        txtLastname.isEnabled = isEdit
        txtCompanyName.isEnabled = isEdit
        txtEmail.isEnabled = isEdit
        txtContacts.isEnabled = isEdit
        txtWorkHours.isEnabled = isEdit
        txtFacebook.isEnabled = isEdit
        txtInstagram.isEnabled = isEdit
      
        
        lblName.text = "\(userData.getString(key: "FirstName")) \(userData.getString(key: "LastName"))"
        txtFirstname.text = userData.getString(key: "FirstName")
        txtLastname.text = userData.getString(key: "LastName")
        txtCompanyName.text = userData.getString(key: "CompanyName")
        txtEmail.text = userData.getString(key: "Email")
        txtEmail.isEnabled = false
        txtContacts.text =  userData.getString(key: "MobileNo")
        txtWorkHours.text = userData.getString(key: "WorkingHours")
        txtFacebook.text = userData.getString(key: "FacebookLink")
        txtInstagram.text = userData.getString(key: "InstragramLink")
        txtCountry.text = userData.getString(key: "CountryName")
        txtState.text = userData.getString(key: "StateName")
        txtCity.text = userData.getString(key: "CityName")
        txtPinCode.text = userData.getString(key: "InstragramLink")
        txtCity.tag = userData.getInt(key: "CityID")
        txtState.tag = userData.getInt(key: "StateID")
        txtCountry.tag = userData.getInt(key: "CountryID")
        txtPinCode.text = userData.getString(key: "ZipCode")
        imgProfile.setImage(userData.getString(key: "UserProfileImage"))
    }

    @objc func AccButton1(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        navigationController?.popViewController(animated: true)
    }

    @objc func AccButton2(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        let number = "+1" + txtContacts.text!
        let param: [String: Any] = [
            "FirstName":txtFirstname.text!,
            "LastName":txtLastname.text!,
            "CompanyName":txtCompanyName.text!,
            "MobileNo":number,
            "WorkingHours":txtWorkHours.text!,
            "FacebookLink":txtFacebook.text!,
            "InstragramLink": txtInstagram.text!,
            "CountryID": txtCountry.tag,
            "StateID": txtState.tag,
            "CityID": txtCity.tag,
            "ZipCode": txtPinCode.text!,
        ]
        
        callApi(Path.editProfile, method: .post, param: param, data: [UIImagePNGRepresentation(imgProfile.image!)!], dataKey: ["ProfileImage"]) { (result) in

            print(result)
            if result.getBool(key: "success")
            {
                let data = result.getDictionary(key: "data")
                UserDefaults.standard.setValuesForKeys(data)
                
                self.showMessage(message: result.getString(key: "message"), completionHandler: {
                    self.navigationController?.popViewController(animated: true)
                })
            }
            else
            {
                self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
            }
        }
    }

    @IBAction func btnCameraAction(_ sender: UIButton) {

        let imagePicker = UIImagePickerController()

        let actionSheet = UIAlertController(title: "Take image", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in

            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                imagePicker.delegate = self
                imagePicker.isEditing = true
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
            else
            {
                self.showMessage(message: "Camera Not Available", completionHandler: nil)
            }
        }))
        actionSheet.addAction(UIAlertAction(title: "Photos", style: .default, handler: { (action) in

            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.delegate = self
            imagePicker.isEditing = true
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)

        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in


        }))

        present(actionSheet, animated: true, completion: nil)

    }

    // MARK: - ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        let image = info[UIImagePickerControllerEditedImage] as! UIImage
        let fixedImaage = UIImage(cgImage: image.cgImage!, scale: image.scale, orientation: image.imageOrientation)

        imgProfile.image = fixedImaage.resize(scale: 0.25)
        picker.dismiss(animated: true, completion: nil)
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        if txtCountry.isEditing
        {
            return arrCountry.count
        }
        else if txtState.isEditing
        {
            return arrStates.count
        }
        else if txtCity.isEditing
        {
            return arrCity.count
        }
        return 0
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        if txtCountry.isEditing
        {
            return arrCountry[row].getString(key: "Name")
        }
        else if txtState.isEditing
        {
            return arrStates[row].getString(key: "Name")
        }
        else if txtCity.isEditing
        {
            return arrCity[row].getString(key: "Name")
        }
        return ""
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        if txtCountry.isEditing
        {
            txtCountry.text = arrCountry[row].getString(key: "Name")
            txtCountry.tag = arrCountry[row].getInt(key: "Id")
        }
        else if txtState.isEditing
        {
            txtState.text = arrStates[row].getString(key: "Name")
            txtState.tag = arrStates[row].getInt(key: "Id")
        }
        else if txtCity.isEditing
        {
            txtCity.text = arrCity[row].getString(key: "Name")
            txtCity.tag = arrCity[row].getInt(key: "Id")
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {

        if textField == txtCountry
        {
            getState()
        }
        else if textField == txtState
        {
            getCity()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                           replacementString string: String) -> Bool
    {
        let maxLength = 10
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
