//
//  SKGJST
//
//  Created by Priyank Jotangiya✌🏻🤓 on 4/10/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit

class SideTableViewCell: UITableViewCell {

    
    @IBOutlet var Lbl_Name: UILabel!
    @IBOutlet var Img_Name: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
