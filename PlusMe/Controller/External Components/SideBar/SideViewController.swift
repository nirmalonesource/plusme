//
//  SKGJST
//
//  Created by Priyank Jotangiya✌🏻🤓 on 4/10/18.
//  Copyright © 2018 empiereTech. All rights reserved.
//

import UIKit
import SWRevealViewController

class SideViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
//MARK: - Outlets🤔
    
    //View
    @IBOutlet var Tbl_Main: UITableView!
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet var Vw_Hader: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblUserType: UILabel!
    @IBOutlet var lblversion: UILabel!
    
//MARK: - Declare Variables👇🏻😬
    var newFrontController: UIViewController!
    var getint = NSInteger()
    var GetMember = [NSDictionary]()
    @IBOutlet var Img_User: UIImageView!
    
    let arrRes = ["Post new", "Designer/Stylists", "Trendsetter", "Blog Feed", "Vision Board","Settings"]//,"Terms of use","Privacy policy","Logout"]
    let arrIcon = [#imageLiteral(resourceName: "Post new"), #imageLiteral(resourceName: "DesignersStylists"), #imageLiteral(resourceName: "DesignersStylists"), #imageLiteral(resourceName: "Blog Feed"), #imageLiteral(resourceName: "tabBar4_0"), #imageLiteral(resourceName: "Settings")]
    /*
     Post New
     Designers (collection of all user Designers, w/ filter option by
     location/zip code)
     Stylists (collection of all user Designers, w/ filter option by
     location/zip code)
     Blog Feed (collection of blog links)
     Settings (change password, notification options, logout, about,
     terms of use, privacy policy etc)
 */
//MARK: - Happy Coding😊
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        lblversion.text = "App Version: \(appVersion ?? "1.0")"
        
        Vw_Hader.layer.shadowOffset = CGSize.zero
        Vw_Hader.layer.shadowRadius = 1.0
        Vw_Hader.layer.shadowOpacity = 0.5
        
        Img_User.layer.cornerRadius = Img_User.frame.size.height/2.0
        Img_User.layer.masksToBounds = true
        Img_User.layer.borderWidth = 2.0
        Img_User.layer.borderColor = UIColor.init(red:232/255.0, green: 59/255.0, blue: 119/255.0, alpha: 1).cgColor
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(PostNewViewController.someAction(_:)))
        // or for swift 2 +
        _ = UITapGestureRecognizer(target: self, action:  #selector (self.someAction (_:)))
        Vw_Hader.addGestureRecognizer(gesture)

        updateUI()
    }

    func updateUI()
    {
        if imgPhoto != nil
        {
            imgPhoto.setImage(StoredData.shared.userProfile)
            lblName.text = StoredData.shared.name
            lblUserType.text = (StoredData.shared.userType == "0") ? "Trendsetter" : "Stylist/Designer"
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)        
    }
    
    // or for Swift 4
    @objc func someAction(_ sender:UITapGestureRecognizer){
        // do other task
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
        newFrontController = vc
        let navigationController = UINavigationController(rootViewController: newFrontController)
        revealViewController()?.pushFrontViewController(navigationController, animated: true)
    }
    
//MARK: - Delegates✋🏻😇
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return(arrRes.count)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:SideTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SideTableViewCell
        cell.Lbl_Name?.text = arrRes[indexPath.row]
        cell.Img_Name?.image = arrIcon[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // do with place whatever you want
        
        
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        
        let row = indexPath.row
        
        //let revealController: SWRevealViewController = revealViewController()
        
       // Home, About Us, Member Search, Business Search, Matrimonial Search, News, Events, Organization, Rituals and Contact Us.
        newFrontController = nil
        let storyboard = UIStoryboard(name:"Main", bundle:nil)
        
          let nav = self.storyboard?.instantiateViewController(withIdentifier: "VisionBoardViewController") as! VisionBoardViewController
                  nav.resetview()
        switch row {
        case 0:
            let home = storyboard.instantiateViewController(withIdentifier: "PostNewViewController")as! PostNewViewController
            newFrontController = home
            
            let navigationController = UINavigationController(rootViewController: newFrontController)
            revealViewController()?.pushFrontViewController(navigationController, animated: true)
            break

        case 1:
            let home = storyboard.instantiateViewController(withIdentifier: "DesignerViewController")as! DesignerViewController
            newFrontController = home
            home.type = 1

            let navigationController = UINavigationController(rootViewController: newFrontController)
            revealViewController()?.pushFrontViewController(navigationController, animated: true)

            break
        case 2:

            let home = storyboard.instantiateViewController(withIdentifier: "DesignerViewController")as! DesignerViewController
            newFrontController = home
            home.type = 2

            let navigationController = UINavigationController(rootViewController: newFrontController)
            revealViewController()?.pushFrontViewController(navigationController, animated: true)

        case 3:
            let home = storyboard.instantiateViewController(withIdentifier: "HomeViewController")as! HomeViewController
            newFrontController = home
            home.isFromMenu = true
            let navigationController = UINavigationController(rootViewController: newFrontController)
            revealViewController()?.pushFrontViewController(navigationController, animated: true)
            break
        case 4:
            let home = storyboard.instantiateViewController(withIdentifier: "MyVisionBoardViewController") as! MyVisionBoardViewController
            newFrontController = home

            let navigationController = UINavigationController(rootViewController: newFrontController)
            revealViewController()?.pushFrontViewController(navigationController, animated: true)
            break
        case 5:
            let home = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            newFrontController = home

            let navigationController = UINavigationController(rootViewController: newFrontController)
            revealViewController()?.pushFrontViewController(navigationController, animated: true)
            break

        case 6:
            break
        case 7:

            break
        default: break
            
        }
        
    }

    func logout()
    {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
        
        self.newFrontController = nil
        let storyboard = UIStoryboard(name:"Main", bundle:nil)

        let home = storyboard.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
        self.newFrontController = home

        let navigationController = UINavigationController(rootViewController: self.newFrontController)

        self.revealViewController()?.pushFrontViewController(navigationController, animated: true)
    }
    //MARK: - Actions👊🏻😠
    

}
