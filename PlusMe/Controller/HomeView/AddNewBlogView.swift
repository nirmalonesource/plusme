//
//  AddNewBlogView.swift
//  PlusMe
//
//  Created by My Mac on 05/03/19.
//  Copyright © 2019 Priyank Jotangiya. All rights reserved.
//

import UIKit
import SWRevealViewController

class AddNewBlogView: UIViewController , UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, ClosetImages , SWRevealViewControllerDelegate
{

    @IBOutlet weak var btnPostNew: UIButton!
    @IBOutlet var Vw_Cancel: UIView!
    @IBOutlet weak var imgPostImage: UIImageView!
    @IBOutlet weak var txtPostAbout: UITextField!
    @IBOutlet weak var txtTitle: UITextField!
    
    var arrCategoryItem = [String]()
    var arrCategory = [[String: Any]]()
    var selectedCategoryId = 0
    var isSelectedImage = false
    var selectedImageUrl = ""
    var imageSource = 0
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnPostNew.roundCorner()
        //NavigationBar
        //        self.navigationController?.navigationBar.isHidden = true
        
        let gesture = UITapGestureRecognizer(target: self, action: "someAction:")
        // or for swift 2 +
        let gestureSwift2AndHigher = UITapGestureRecognizer(target: self, action:  #selector (self.someAction (_:)))
        Vw_Cancel.addGestureRecognizer(gesture)
        
        self.title = "Post New Blog"
        
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }
        
        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        menuButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        
        //SideBar
//        if revealViewController() != nil
//        {
//            self.revealViewController().delegate = self
//            menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
//            self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
//            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
//        }
        
        let AccButton = UIButton(type: .system)
        AccButton.setImage(#imageLiteral(resourceName: "home").withRenderingMode(.alwaysOriginal), for: .normal)
        AccButton.tag = 1
        AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        AccButton.addTarget(self, action: #selector(AccButton(_:)), for: .touchUpInside)
        let barButtonItem = UIBarButtonItem(customView: AccButton)
        self.navigationItem.rightBarButtonItems = [barButtonItem]
        
    }
    
    //MARK:- Button Action
    
    @objc func AccButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        
        if button.tag == 1
        {
            let home = TabBarViewController()
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    // or for Swift 4
    
    @objc func someAction(_ sender:UITapGestureRecognizer){
        // do other task
        let home = TabBarViewController()
        home.setUI()
        self.navigationController?.pushViewController(home, animated: true)
    }
    
    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {
            
            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            
        } else {
            revealController.frontViewController.view.alpha = 1
            self.view.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func btnAddImageAction(_ sender: UIButton) {
        
        let actionSheet = UIAlertController(title: "Choose Action", message: nil, preferredStyle: .actionSheet)
//        actionSheet.addAction(UIAlertAction(title: "Vision Board", style: .default, handler: { (action) in
//
//            self.imageSource = 1
//            self.performSegue(withIdentifier: "chooseImage", sender: nil)
//        }))
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (action) in
            
            self.imageSource = 0
            self.fromDevice()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
            
        }))
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    func fromDevice()
    {
        let imagePicker = UIImagePickerController()
        
        let actionSheet = UIAlertController(title: "Take image", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                imagePicker.delegate = self
                imagePicker.isEditing = true
                imagePicker.allowsEditing = true
                imagePicker.accessibilityFrame = CGRect(x: 0, y: 0, width: 40, height: 50)
                self.present(imagePicker, animated: true, completion: nil)
            }
            else
            {
                self.showMessage(message: "Camera Not Available", completionHandler: nil)
            }
        }))
        actionSheet.addAction(UIAlertAction(title: "Photos", style: .default, handler: { (action) in
            
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.delegate = self
            imagePicker.isEditing = true
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
        }))
        
        present(actionSheet, animated: true, completion: nil)
        
    }
    
    /*
     BlogTitle
     Description
     ImageType == 1 (image_url)
     ImageType == 0 (PostImage)
   */
    
    @IBAction func btnPostNewAction(_ sender: UIButton) {
        
        if validateData()
        {
            var param: [String: Any] = [
                "BlogTitle": txtTitle.text!,
                "Description": txtPostAbout.text!.encodeEmoji
            ]
            
            var imageData = [Data]()
            var dataKey = [String]()
            
            if imageSource == 0
            {
                param["ImageType"] = 0
                imageData.append(UIImagePNGRepresentation(imgPostImage.image!)!)
                dataKey.append("PostImage")
            }
                
            else
            {
                param["ImageType"] = 1
                param["image_url"] = selectedImageUrl
            }
            
            callApi(Path.addNewBlogFeed, method: .post, param: param, data: imageData, dataKey: dataKey) { (result) in
                
                print(result)
                if result.getBool(key: "success")
                {
                    self.txtPostAbout.text = ""
                    self.selectedCategoryId = 0
                    self.imgPostImage.image = nil
                    self.isSelectedImage = false
                    self.showMessage(message: result.getString(key: "message"), completionHandler: {
                        //self.someAction(UITapGestureRecognizer())
                        self.navigationController?.popViewController(animated: true)
                    })
                }
                    
                else
                {
                    self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                }
            }
        }
    }
    
    func validateData() -> Bool
    {
        if txtTitle.text!.isEmpty()
        {
            showMessage(message: "Please enter title", completionHandler: nil)
            return false
        }
        else if txtPostAbout.text!.isEmpty()
        {
            showMessage(message: "Please mention about new post", completionHandler: nil)
            return false
        }
        else if !isSelectedImage
        {
            showMessage(message: "Please take image for new post", completionHandler: nil)
            return false
        }
        return true
    }
    
    // MARK: - ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image = info[UIImagePickerControllerEditedImage] as! UIImage
        let fixedImaage = UIImage(cgImage: image.cgImage!, scale: image.scale, orientation: image.imageOrientation)
        
        imgPostImage.image = fixedImaage.resize(scale: 0.8)
        isSelectedImage = true
        picker.dismiss(animated: true, completion: nil)
    }
    
    func selectedUrl(url: String, id: Int) {
        
        isSelectedImage = true
        selectedImageUrl = url
        imgPostImage.setImage(url)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "chooseImage"
        {
            let destination = segue.destination as! ChooseFromClosetViewController
            destination.delegate = self
        }
    }
    
    @objc func backAction(_ button: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

/*
 if validateData()
 {
 var param: [String: Any] = [
 "PostTitle": txtTitle.text!,
 "CategoryID": selectedCategoryId,
 "Description": txtPostAbout.text!
 ]
 
 var imageData = [Data]()
 var dataKey = [String]()
 
 if imageSource == 0
 {
 param["ImageType"] = 0
 imageData.append(UIImagePNGRepresentation(imgPostImage.image!)!)
 dataKey.append("PostImage")
 }
 
 else
 {
 param["ImageType"] = 1
 param["image_url"] = selectedImageUrl
 }
 
 callApi(Path.addNewPost, method: .post, param: param, data: imageData, dataKey: dataKey) { (result) in
 
 print(result)
 if result.getBool(key: "success")
 {
 self.txtPostAbout.text = ""
 self.selectedCategoryId = 0
 self.imgPostImage.image = nil
 self.isSelectedImage = false
 self.showMessage(message: result.getString(key: "message"), completionHandler: {
 self.someAction(UITapGestureRecognizer())
 })
 }
 
 else
 {
 self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
 }
 }
 }
 */
