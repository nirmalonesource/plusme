//
//  BlogDetailsVC.swift
//  PlusMe
//
//  Created by rohit on 09/08/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit
import SWRevealViewController

class BlogDetailsVC: UIViewController, SWRevealViewControllerDelegate, UITableViewDelegate, UITableViewDataSource
{

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var impPostImage: UIImageView!
    @IBOutlet weak var btnColset: UIButton!
    @IBOutlet weak var imgUserPhoto: UIImageView!
    @IBOutlet weak var lblUerName: UILabel!
    @IBOutlet weak var lblComments: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblUserType: UILabel!

    @IBOutlet weak var txtComment: UITextField!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet var lbltotallike: UILabel!
    @IBOutlet var tblheight: NSLayoutConstraint!
    
    var blogId = ""
    var TYPE = ""
    var arrComment = [[String: Any]]()
    var categoryName = ""
    var isFromMyBlog = Bool()
    var isfromnotification = false
    //VIEW LIFE CYCLE 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
   

        tableView.delegate = self
        tableView.dataSource = self
        tableView.isHidden = true

        if TYPE == "10" {
            getBlogFeed()
        }
        else
        {
            getBlogDetail()
        }
        

        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        tblheight.constant = tableView.contentSize.height
    }
    override func viewWillAppear(_ animated: Bool) {
             self.title = categoryName
                self.navigationController?.navigationBar.isHidden = false;

                navigationController?.navigationBar.barTintColor = UIColor.white
                navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
                self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }

                let menuButton = UIButton(type: .system)
                menuButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
                menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
                menuButton.addTarget(self, action: #selector(AccButton1(_:)), for: .touchUpInside)
                navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
                
        //        let menuButton = UIButton(type: .system)
        //             menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
        //             menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        //             navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
                
                let HomeButton = UIButton(type: .system)
                HomeButton.setImage(#imageLiteral(resourceName: "home").withRenderingMode(.alwaysOriginal), for: .normal)
                HomeButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
                HomeButton.addTarget(self, action: #selector(HomeButton(_:)), for: .touchUpInside)
                let barHomeButtonItem = UIBarButtonItem(customView: HomeButton)
                     
        //             let AccButton1 = UIButton(type: .system)
        //             AccButton1.setImage(#imageLiteral(resourceName: "Search").withRenderingMode(.alwaysOriginal), for: .normal)
        //             AccButton1.tag = 1
        //             AccButton1.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        //             AccButton1.addTarget(self, action: #selector(AccButton(_:)), for: .touchUpInside)
        //             let barButtonItem = UIBarButtonItem(customView: AccButton1)
                     
                   //  let AccButton = UIButton(type: .system)
                let AccButton = SSBadgeButton()
                         if UserDefaults.standard.object(forKey: "notification") != nil {
                             let noticount = UserDefaults.standard.string(forKey: "notification")
                             AccButton.badge = noticount
                         }
                         else
                         {
                              AccButton.badge = nil
                         }
                     AccButton.setImage(#imageLiteral(resourceName: "Notification").withRenderingMode(.alwaysOriginal), for: .normal)
                     AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
                     AccButton.addTarget(self, action: #selector(AccButton2(_:)), for: .touchUpInside)
                     let barButtonItem1 = UIBarButtonItem(customView: AccButton)
                
                  self.navigationItem.rightBarButtonItems = [ barButtonItem1,barHomeButtonItem]
    }
//    @objc func AccButton1(_ button: UIButton) {
//           print("Button with tag: \(button.tag) clicked!")
//           let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
//           self.navigationController?.pushViewController(vc!, animated: true)
//       }
    
    @objc func AccButton1(_ button: UIButton) {
           print("Button with tag: \(button.tag) clicked!")
        if isfromnotification {
            let home = TabBarViewController()
            self.navigationController?.pushViewController(home, animated: true)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
          
       }
    
    @objc func AccButton2(_ button: UIButton) {
              print("Button with tag: \(button.tag) clicked!")
               let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
                     self.navigationController?.pushViewController(vc!, animated: true)
          }

       @objc func HomeButton(_ button: UIButton) {
           print("Button with tag: \(button.tag) clicked!")
            navigationController?.popViewController(animated: true)
        
         
       }

    override func viewWillDisappear(_ animated: Bool) {

        tabBarController?.tabBar.isHidden = false
    }

    func getBlogDetail()
    {
        
        let param = [
            "BlogId": blogId
        ]
        callApi(Path.getBlogDetail, method: .post, param: param) { (result) in

            print(result)
            if result.getBool(key: "success")
            {
                let data = result.getDictionary(key: "data")
                
                self.impPostImage.setImage(data.getString(key: "PostImage"))
                self.imgUserPhoto.setImage(data.getString(key: "ProfileImage"))
                self.imgUserPhoto.roundView()
                self.lblUerName.text = data.getString(key: "PostedByName")
                self.lblUserType.text = "Trendsetter"
                self.lblDescription.text = data.getString(key: "PostTitle").decodeEmoji
                self.lblUerName.text = data.getString(key: "PostedByName")
                self.arrComment = data.getArrayofDictionary(key: "CommentArray")
                self.lblComments.text = "\(data.getString(key : "TotalComment"))"
                self.lbltotallike.text = "\(data.getString(key : "TotalLike"))"
                self.btnColset.isSelected = data.getBool(key: "IsCloset")
                self.tableView.isHidden = self.arrComment.count == 0
                self.lblTime.text = data.getString(key: "PostTime").toDate().timeAgoSinceDate()
                self.tableView.reloadData()

                self.addGesture(self.imgUserPhoto, userId: data.getInt(key: "PostedByID"))
                self.btnLike.isSelected = data.getBool(key: "isLike")
            }
        }
    }
    
    func getBlogFeed()
    {
        
        let param = [
            "BlogID": blogId
        ]
        callApi(Path.getblogfeed, method: .post, param: param) { (result) in

            print(result)
            if result.getBool(key: "success")
            {
                let data = result.getDictionary(key: "data")
                
                self.impPostImage.setImage(data.getString(key: "PostImage"))
                self.imgUserPhoto.setImage(data.getString(key: "ProfileImage"))
                self.imgUserPhoto.roundView()
                self.lblUerName.text = data.getString(key: "PostedByName")
                self.lblUserType.text = "Trendsetter"
                self.lblDescription.text = data.getString(key: "PostTitle").decodeEmoji
                self.lblUerName.text = data.getString(key: "PostedByName")
                self.arrComment = data.getArrayofDictionary(key: "CommentArray")
                self.lblComments.text = "\(data.getString(key : "TotalComment"))"
                self.lbltotallike.text = "\(data.getString(key : "TotalLike"))"
                self.btnColset.isSelected = data.getBool(key: "IsCloset")
                self.tableView.isHidden = self.arrComment.count == 0
               // self.lblTime.text = data.getString(key: "PostTime").toDate().timeAgoSinceDate()
                self.tableView.reloadData()

                self.addGesture(self.imgUserPhoto, userId: data.getInt(key: "PostedByID"))
                self.btnLike.isSelected = data.getBool(key: "isLike")
            }
        }
    }

   

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrComment.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CommentCell

        let comment = arrComment[indexPath.row]
        cell.imgUserPhoto.setImage(comment.getString(key: "UserProfileImage"))
        cell.imgUserPhoto.backgroundColor = .clear
        cell.imgUserPhoto.roundView()
        cell.lblName.text = comment.getString(key: "CommentedByName")
        cell.lblComment.text = comment.getString(key: "Comment").decodeEmoji
        addGesture(cell.lblName, userId: comment.getInt(key: "UserId"))
        addGesture(cell.imgUserPhoto, userId: comment.getInt(key: "UserId"))
        cell.btnreply.tag = indexPath.row
        cell.btnreply.addTarget(self, action: #selector(connected(sender:)), for: .touchUpInside)
         cell.lbldate.text = convertDateString(dateString: comment.getString(key: "Time"), fromFormat: "dd-MM-yyyy hh:mm:ss", toFormat: "dd-MM-yyyy hh:mm a")
        viewDidLayoutSubviews()
        // comment.getString(key: "Time")
        return cell
    }

    @objc func connected(sender: UIButton){
        let buttonTag = sender.tag
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "FeedCommentReply") as! FeedCommentReply
        nav.dictData = self.arrComment[buttonTag]
        nav.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(nav, animated: true)
       
//        view.endEditing(true)
//        let param = [
//            "CommentID": arrComment[buttonTag]["BlogCommentId"],
//            "id": arrComment[buttonTag]["BlogCommentId"],
//            "FROM": "FEED"
//        ]
//
//        callApi(Path.getComment, method: .post, param: param as [String : Any]) { (result) in
//
//            print(result)
//            let dic:NSDictionary = result as NSDictionary
//            if result.getBool(key: "success")
//            {
//                
//              
//            }
//        }
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let nav = self.storyboard?.instantiateViewController(withIdentifier: "BlogListDetailCommentReplyView") as! BlogListDetailCommentReplyView
//        nav.dictData = arrComment[indexPath.row]
//        navigationController?.pushViewController(nav, animated: true)
//    }
    
    func callcomentreplyapi()  {
        
        
    }

    @IBAction func btnCommentAction(_ sender: UIButton) {
        

        if !txtComment.text!.isEmpty()
        {
            view.endEditing(true)
            let param = [
                "blog_id": blogId,
                "comment": txtComment.text!.encodeEmoji
            ]

            callApi(Path.addComment, method: .post, param: param) { (result) in

                print(result)
                if result.getBool(key: "success")
                {
                    self.txtComment.text = ""
                    self.getBlogDetail()
                }
            }
        }
    }

    @IBAction func btnClosetAction(_ sender: UIButton) {
//        let param = [
//            "BlogId": blogId
//        ]
//        callApi(Path.addToCloset, method: .post, param: param) { (result) in
//            print(result)
//            if result.getBool(key: "success")
//            {
//                sender.isSelected = !sender.isSelected
//            }
//        }
        
         if !sender.isSelected
         {
        
        let param: [String: Any] = [
            "BaseImageID": 0,
            "BlogId" : blogId
            ]
        
            self.callApi(Path.saveToCloset, method: .post, param: param, data: [(UIImagePNGRepresentation((impPostImage.image?.resize(scale: 1) ?? UIImage())) ?? Data())], dataKey: ["PostImage"]) { (result) in
            
            print(result)
            if result.getBool(key: "success")
            {
                sender.isSelected = !sender.isSelected
                self.showMessage(message: result.getString(key: "message"), completionHandler: {
                })
            }
                
            else
            {
                self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
            }
        }
        }
        else
        {
            self.showMessage(message: "Already added to Closet", completionHandler: nil)
        }
    }

    @IBAction func btnLikeClicked(sender:UIButton) {
        let param = [
            "PostId": blogId,
            "isLike": sender.isSelected ? "0" : "1"
        ]
        callApi(Path.likePost, method: .post, param: param) { (result) in
            print(result)
            if result.getBool(key: "success")
            {
                sender.isSelected = !sender.isSelected
            }
            self.getBlogDetail()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


class CommentCell: UITableViewCell {
    @IBOutlet weak var imgUserPhoto: UIImageView!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblName: UILabel!

    @IBOutlet var btnreply: UIButton!
    @IBOutlet var lbldate: UILabel!
    
}
