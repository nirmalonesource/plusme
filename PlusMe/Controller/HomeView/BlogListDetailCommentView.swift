//
//  BlogListDetailCommentView.swift
//  PlusMe
//
//  Created by My Mac on 28/03/19.
//  Copyright © 2019 Priyank Jotangiya. All rights reserved.
//

import UIKit
import SafariServices

class NewBlogListSubCommentCell: UITableViewCell
{
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var btnViewAll: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

class BlogListDetailCommentView: UIViewController , UITableViewDelegate, UITableViewDataSource , UIGestureRecognizerDelegate
{
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    @IBOutlet weak var imgVW: UIImageView!
    @IBOutlet weak var tblVW: UITableView!
    @IBOutlet weak var constImgHeight: NSLayoutConstraint!
    
    @IBOutlet weak var txtComment: UITextField!
    
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var constTblHeight: NSLayoutConstraint!
    var dictData = [String : Any]()
    var arrComment = [[String:Any]]()
    var tempArryComment = [[String:Any]]()
    var postId1 = String()
   //  var blogId = ""
    var isfromnotification = false
    //MARK:- View Life Cycle
    
    
    override func viewWillAppear(_ animated: Bool) {
          self.comentGetFun()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = false;
        
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }
        
        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        menuButton.addTarget(self, action: #selector(AccButton1(_:)), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        tblVW.tableFooterView = UIView()
        
        imgUser.setRadius(radius: imgUser.frame.height/2)
        imgUser.setRadiusBorder(color: UIColor.lightGray)
        
       
        //lblDesc.underline()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        tap.delegate = self // This is not required
        lblDesc.isUserInteractionEnabled = true
        lblDesc.addGestureRecognizer(tap)
      
      
       
        
        //add_blogfeed_comment
        
//        blog_id=1
//        comment="thanx"
//        Auth="
//        aa 3 parameter + api name

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.layoutIfNeeded()
        constTblHeight.constant = tblVW.contentSize.height
    }
    
    //MARK:- Gesture
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        let desc =  dictData["Description"] as? String ?? ""
        
        let arr = desc.components(separatedBy: "\n\n") as NSArray
        var url1 = arr.object(at: 0) as! String
        
//        guard let url = URL(string:url1)
//            else
//        {
//            print("link not open")
//            return }
       // UIApplication.shared.open(url)
        url1 = url1.replacingOccurrences(of: "www.", with: "http://")
        if url1.contains("http://") {
            
        }
        else
        {
            url1 = "http://" + url1
        }
        if let instaurl = URL(string:url1),
            UIApplication.shared.canOpenURL(instaurl) {
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(instaurl)
            } else {
                UIApplication.shared.openURL(instaurl)
            }
        }

    }
    
    
    /*
     
     //MARK:- Gesture
     @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
     // handling code
     guard let url = URL(string: dictSingleListingData[0]["product_website"] as? String ?? "") else {
     return //be safe
     }
     
     if ["http", "https"].contains(url.scheme?.lowercased() ?? "") {
     // Can open with SFSafariViewController
     let safariViewController = SFSafariViewController(url: url)
     self.present(safariViewController, animated: true, completion: nil)
     } else {
     UIApplication.shared.open(url, options: [:], completionHandler: nil)
     }
  */
    
    //MARK:- Tableview methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrComment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:NewBlogListSubCommentCell = tableView.dequeueReusableCell(withIdentifier: "NewBlogListSubCommentCell") as! NewBlogListSubCommentCell
        cell.layoutIfNeeded()
        
        cell.imgUser.setRadius(radius: cell.imgUser.frame.height/2)
        cell.imgUser.setRadiusBorder(color: UIColor.lightGray)
       
        cell.lblTitle.text = arrComment[indexPath.row]["PostedByName"] as? String
        cell.lblSubtitle.text = (arrComment[indexPath.row]["Comment"] as? String)?.decodeEmoji
        
        let imgURL = arrComment[indexPath.row]["ProfileImage"] as? String
        cell.imgUser.setImage(imgURL ?? "")
        
        cell.layoutIfNeeded()
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "BlogListDetailCommentReplyView") as! BlogListDetailCommentReplyView
        nav.dictData = arrComment[indexPath.row]
        navigationController?.pushViewController(nav, animated: true)
    }
    
    //MARK:- Button Action
    
    @objc func AccButton1(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        if isfromnotification {
            let home = TabBarViewController()
            self.navigationController?.pushViewController(home, animated: true)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func setdata()  {
        
         lblTitle.text = dictData["BlogTitle"] as? String
                      lblSubtitle.text = dictData["RegisterUserTypeByName"] as? String
        lblDesc.text = (dictData["Description"] as? String)?.decodeEmoji
               
               lblTime.text = dictData.getString(key: "CreateDate") != "" ? dictData.getString(key: "CreateDate").toDate().timeAgoSinceDate() : ""
               
               let imgURL = dictData["ProfileImage"] as? String
               imgUser.setImage(imgURL ?? "")
               
               let imgURLPost = dictData["PostImage"] as? String
               imgVW.setImage(imgURLPost ?? "")
               
               arrComment = dictData["comments"] as? [[String:Any]] ?? []
               tblVW.reloadData()
               self.view.layoutIfNeeded()
               constTblHeight.constant = tblVW.contentSize.height
        self.viewDidLayoutSubviews()
               lblComment.text = "\(arrComment.count)"
               let catPictureURL = URL(string: imgURLPost ?? "")
               let session = URLSession(configuration: .default)
               
               /*
                if let image = response.result.value {
                let size = image.size
                print("image size is \(size)")
                }
        */
               if catPictureURL != nil {
                    DispatchQueue.main.async {
                              
                              let downloadPicTask = session.dataTask(with: catPictureURL ??  URL(string:"")!) { (data, response, error) in
                                  if let e = error {
                                      print("Error downloading cat picture: \(e)")
                                  } else {
                                      if let res = response as? HTTPURLResponse {
                                          print("Downloaded cat picture with response code \(res.statusCode)")
                                          if let imageData = data {
                                              let image = UIImage(data: imageData)
                                              print("IMAGESIZE",image?.size.height ?? 0)
                                             // self.constImgHeight.constant = 350 //image?.size.height ?? 0
                                          }
                                          else {
                                              print("Couldn't get image: Image is nil")
                                          }
                                      } else {
                                          print("Couldn't get response code for some reason")
                                      }
                                  }
                              }
                              downloadPicTask.resume()
                          }
               }
    }
    
    func comentGetFun() {
        var param = [:] as! [String:Any]
        if (dictData["NotificationId"] != nil) {
            param = [
                           "BlogId": dictData["TypeID"] as? String ?? "0" ,
                          // "BlogId": blogId ,
                           "PostedByID": postId1
                       ]
        }
        else if isfromnotification
        {
            param = [
                "BlogId": postId1 ,
               // "BlogId": blogId ,
                "PostedByID": StoredData.shared.userId
            ]
        }
        else
        {
            param = [
                "BlogId": dictData["BlogId"] as? String ?? "0" ,
               // "BlogId": blogId ,
                "PostedByID": postId1
            ]
        }
             
        
        callApi(Path.getBlogListDetail, method: .post, param: param) { (result) in
            
            print(result)
            if result.getBool(key: "success")
            {
                self.tempArryComment = result.getArrayofDictionary(key: "data")
                //
                
                
                //
                self.arrComment.removeAll()
                
                for i in self.tempArryComment {
                    self.dictData = i
                   
                   self.arrComment = i["comments"] as? [[String:Any]] ?? []
                }
                 self.setdata()
                 self.tblVW.reloadData()
                 self.constTblHeight.constant = self.tblVW.contentSize.height
                 self.viewDidLayoutSubviews()
                 self.lblComment.text = "\(self.arrComment.count)"
            }
        }
    }

    @IBAction func btnSendCommentAction(_ sender: UIButton) {
        if !txtComment.text!.isEmpty()
        {
            view.endEditing(true)
            var param = [:] as! [String:Any]
                   if (dictData["NotificationId"] != nil) {
                        let param = [
                                      "blog_id": dictData["TypeID"] as? String ?? "0" ,
                                       //"blog_id": blogId ,
                                      "comment": txtComment.text!.encodeEmoji,
                                      "FROM": "BLOG"
                                  ]
                   }
                   else
                   {
                         param = [
                                      "blog_id": dictData["BlogId"] as? String ?? "0" ,
                                       //"blog_id": blogId ,
                                      "comment": txtComment.text!.encodeEmoji,
                                      "FROM": "BLOG"
                                  ]
                   }
           
            
            callApi(Path.addBlogfeedComment,
                    method: .post,
                    param: param) { (result) in
                print(result)
                if result.getBool(key: "success")
                {
                    self.txtComment.text = ""
//                    self.showMessage(message: result.getString(key: "message"), completionHandler: {
//                        for controller in self.navigationController!.viewControllers as Array {
//                            if controller.isKind(of: BlogViewAllView.self) {
//                                _ =  self.navigationController!.popToViewController(controller, animated: true)
//                                break
//                            }
//                        }
//
//                    })
                    
                    self.comentGetFun()
                }
                    
                else
                {
                    self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                }
            }
        }
    }

}

extension UILabel {
    func underline() {
        if let textString = self.text {
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle,
                                          value: NSUnderlineStyle.styleSingle.rawValue,
                                          range: NSRange(location: 0, length: textString.count))
            self.attributedText = attributedString
        }
    }
}
