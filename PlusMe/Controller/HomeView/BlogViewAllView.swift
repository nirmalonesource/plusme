//
//  BlogViewAllView.swift
//  PlusMe
//
//  Created by My Mac on 28/03/19.
//  Copyright © 2019 Priyank Jotangiya. All rights reserved.
//

import UIKit
import DropDown

class BlogViewAllView: UIViewController , UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblVW: UITableView!
    var arrResult = [[String: Any]]()
    var postId = String()
    let ReportDropDown = DropDown()
    //MARK:- Web service
    
    func getBlogs()
    {
        let param = [
            "PostedByID": postId
        ]
        
        callApi(Path.getBlogListDetail, method: .post, param: param)
        { (result) in
            print(result)
            if result.getBool(key: "success")
            {
                self.arrResult = result.getArrayofDictionary(key: "data")
            }
            else
            {
                self.tblVW.showEmptyMessage(message: result.getString(key: "message"))
            }
            
            self.tblVW.reloadData()
        }
    }
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = false;
        
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }
        
        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        menuButton.addTarget(self, action: #selector(AccButton1(_:)), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        tblVW.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
           getBlogs()
    }
    
    //MARK:- Tableview methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:NewBlogListSubCell = tableView.dequeueReusableCell(withIdentifier: "NewBlogListSubCell") as! NewBlogListSubCell
        let record = arrResult[indexPath.row]
        cell.lblTitle.text = arrResult[indexPath.row]["BlogTitle"] as? String
        cell.lblSubTitle.text = (arrResult[indexPath.row]["Description"] as? String)?.decodeEmoji
        cell.lblTime.text = record.getString(key: "CreateDate") != "" ? record.getString(key: "CreateDate").toDate().timeAgoSinceDate() : ""
        cell.selectionStyle = .none
        
        cell.btnmenu.tag = indexPath.row
        cell.btnmenu.addTarget(self, action: #selector(btnReportClicked(sender:)), for: UIControlEvents.touchUpInside)
        
        if StoredData.shared.userId == postId {
            cell.btnmenu.isHidden = false
        }
        else
        {
            cell.btnmenu.isHidden = true
        }
        
        return cell
    }
    
    //MARK: - Actions👊🏻😠
    
    @objc func btnReportClicked(sender:UIButton) {
        
        let post = arrResult[sender.tag]
                
        ReportDropDown.anchorView = sender
              ReportDropDown.dataSource = ["Delete"]
        ReportDropDown.selectionAction = { [weak self] (index, item) in
            print(sender.tag)
            self?.DeletePost(BlogID: post.getString(key: "BlogId"))
        }
        ReportDropDown.show()
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "BlogListDetailCommentView") as! BlogListDetailCommentView
        nav.dictData = arrResult[indexPath.row]
        nav.postId1 = postId
        navigationController?.pushViewController(nav, animated: true)
    }
    
    @objc func AccButton1(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        navigationController?.popViewController(animated: true)
    }
    
    func DeletePost(BlogID:String)
       {
           arrResult.removeAll()
           callApi(Path.RemovePost, method: .post, param: ["BlogID": BlogID]) { (result) in

               print(result)
               if result.getBool(key: "success")
               {
                self.getBlogs()
                  // self.arrMyPost = result.getArrayofDictionary(key: "data")
               }
//               if self.arrMyPost.count == 0
//               {
//                   self.lblMessage.text = "No post found"
//                   self.lblMessage.isHidden = false
//               }
            //   self.tableView.reloadData()
           }
       }

}
