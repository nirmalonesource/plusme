//
//  FeedCommentReply.swift
//  PlusMe
//
//  Created by My Mac on 19/10/19.
//  Copyright © 2019 Priyank Jotangiya. All rights reserved.
//

import UIKit

class FeedCommentReply:  UIViewController , UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet var lbldate: UILabel!
    
    @IBOutlet weak var txtComment: UITextField!
    @IBOutlet weak var tblVW: UITableView!
    var dictData = [String : Any]()
    var arrReply = [[String:Any]]()
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = false;
        
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }
        
        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        menuButton.addTarget(self, action: #selector(AccButton1(_:)), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        
        imgUser.setRadius(radius: imgUser.frame.height/2)
        imgUser.setRadiusBorder(color: UIColor.lightGray)
        
        print("DICTDATA:",dictData)
        
        lblTitle.text = dictData["CommentedByName"] as? String
        lblSubTitle.text = (dictData["Comment"] as? String)?.decodeEmoji
        
        lbldate.text = convertDateString(dateString: dictData.getString(key: "Time"), fromFormat: "dd-MM-yyyy hh:mm:ss", toFormat: "dd-MM-yyyy hh:mm a")
        
        let imgURL = dictData["UserProfileImage"] as? String
        imgUser.setImage(imgURL ?? "")
       
        tblVW.tableFooterView = UIView()
        callcomentreplyapi()
        tabBarController?.tabBar.isHidden = true
    }
    
    //MARK:- Tableview methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReply.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:BlogListDetailCommentReplyCell = tableView.dequeueReusableCell(withIdentifier: "BlogListDetailCommentReplyCell") as! BlogListDetailCommentReplyCell
        cell.layoutIfNeeded()
       
        cell.imgUser.setRadius(radius: cell.imgUser.frame.height/2)
        cell.imgUser.setRadiusBorder(color: UIColor.lightGray)
        
        cell.lblTitle.text = arrReply[indexPath.row]["PostedByName"] as? String
        cell.lblSubtitle.text = (arrReply[indexPath.row]["Comment"] as? String)?.decodeEmoji
        
        cell.lbldate.text = convertDateString(dateString: arrReply[indexPath.row].getString(key: "Time"), fromFormat: "dd-MM-yyyy hh:mm:ss", toFormat: "dd-MM-yyyy hh:mm a")
        
        let imgURL = arrReply[indexPath.row]["ProfileImage"] as? String
        cell.imgUser.setImage(imgURL ?? "")
        
        cell.layoutIfNeeded()
        
        cell.selectionStyle = .none
        return cell
    }
    
    @objc func AccButton1(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        navigationController?.popViewController(animated: true)
    }
    
    func callcomentreplyapi()  {
           
                view.endEditing(true)
                let param = [
                    "CommentID": dictData["BlogCommentId"] as! String,
                    //"id": dictData["BlogCommentId"] as! String,
                    "FROM": "FEED"
                ]
        
                callApi(Path.getComment, method: .post, param: param as [String : Any]) { (result) in
        
                    print(result)
                    let dic:NSArray = result["data"] as! NSArray
                    if result.getBool(key: "success")
                    {
                        self.arrReply = ((dic.object(at: 0)) as AnyObject).value(forKey: "reply") as? [[String:Any]] ?? []
                        self.tblVW.reloadData()
        
                    }
                }
           
       }
    
    
    @IBAction func btnSendCommentAction(_ sender: UIButton) {
        if !txtComment.text!.isEmpty()
        {
            view.endEditing(true)
            let param = [
                "blog_id": dictData["BlogId"] as? String ?? "0" ,
                "ParentCommentID" : dictData["BlogCommentId"] as? String ?? "0" ,
                "comment": txtComment.text!.encodeEmoji
            ]
            
            callApi(Path.addBlogfeedComment, method: .post, param: param) { (result) in
                
                print(result)
                if result.getBool(key: "success")
                {
                    self.txtComment.text = ""
                    self.showMessage(message: result.getString(key: "message"), completionHandler: {
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: BlogViewAllView.self) {
                                _ =  self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                        
                    })
                    self.callcomentreplyapi()
                }
                else
                {
                    self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                }
            }
        }
    }
    
}
