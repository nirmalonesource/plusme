//
//  HomeViewController.swift
//  PlusMe
//
//  Created by Priyank Jotangiya on 23/07/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit
import SWRevealViewController

//MARK:- Table view cell class

protocol CellNavigationDelegate: NSObjectProtocol {
    func didPressCell(sender: [String : Any])
}

class NewBlogListCell: UITableViewCell , UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var btnViewAll: UIButton!
    @IBOutlet weak var tblVW: UITableView!
    @IBOutlet weak var constTblHeight: NSLayoutConstraint!
    var arrBlog = [[String:Any]]()
    var delegate:CellNavigationDelegate!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        tblVW.delegate = self
        tblVW.dataSource = self
        
        tblVW.reloadData()
        self.setNeedsLayout()
        self.layoutIfNeeded()
      //  constTblHeight.constant = tblVW.contentSize.height
        self.layoutIfNeeded()
        tblVW.tableFooterView = UIView()
    }
    
     func viewDidLayoutSubviews() {
       // constTblHeight.constant = tblVW.contentSize.height
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        self.layoutIfNeeded()
        super.layoutSubviews()
       // constTblHeight.constant = tblVW.contentSize.height
        self.layoutIfNeeded()
    }
    
    //MARK:- Tableview methods
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
           return 1000
       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrBlog.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:NewBlogListSubCell = tableView.dequeueReusableCell(withIdentifier: "NewBlogListSubCell") as! NewBlogListSubCell
        let record = arrBlog[indexPath.row]

        cell.lblTitle.text = arrBlog[indexPath.row]["BlogTitle"] as? String
        cell.lblSubTitle.text = (arrBlog[indexPath.row]["Description"] as? String)?.decodeEmoji
        cell.lblTime.text = record.getString(key: "CreateDate").toDate().timeAgoSinceDate()
        cell.selectionStyle = .none
         print("INNERCELLHEIGHT",cell.contentView.frame.size.height)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.didPressCell(sender: arrBlog[indexPath.row] as [String : Any])
    }
}

class NewBlogListSubCell: UITableViewCell
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnmenu: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

class HomeViewController: UIViewController, SWRevealViewControllerDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate , CellNavigationDelegate
{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tblVW: UITableView!
    var menuView: BTNavigationDropdownMenu!
    @IBOutlet weak var btnAddBlog: UIButton!
    var arrBlogs = [[String: Any]]()
    var arrCategoryItem = [String]()
    var arrCategory = [[String: Any]]()
    var categoryName = ""
    var isFromMenu = false
    var selectedCatId = 0
    var arrResult = [[String: Any]]()

    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //NavigationBar
        self.navigationController?.navigationBar.isHidden = false;
        
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }

        btnAddBlog.setRadius(radius: btnAddBlog.frame.height/2)
        let menuButton = UIButton(type: .system)
        
        tblVW.estimatedRowHeight = 400
        tblVW.rowHeight = UITableViewAutomaticDimension
        
        if !isFromMenu
        {
            self.title = categoryName
            btnAddBlog.isHidden = true
            let menuButton = UIButton(type: .system)
            menuButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
            menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
            menuButton.addTarget(self, action: #selector(AccButton1(_:)), for: .touchUpInside)
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        }
            
        else
        {
            self.title = "Blog Feed"
//            let menuButton = UIButton(type: .system)
            menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
            menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        }
        
        //SideBar
        if revealViewController() != nil && isFromMenu
        {
            self.revealViewController().delegate = self
            menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

            let AccButton3 = UIButton(type: .system)
            AccButton3.setImage(#imageLiteral(resourceName: "home").withRenderingMode(.alwaysOriginal), for: .normal)
            AccButton3.tag = 1
            AccButton3.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
            AccButton3.addTarget(self, action: #selector(AccButton3(_:)), for: .touchUpInside)
            let barButtonItem3 = UIBarButtonItem(customView: AccButton3)
            self.navigationItem.rightBarButtonItems = [barButtonItem3]
        }

//        if isFromMenu
//        {
//            StoredData.shared.getCategory { (result) in
//                self.arrCategoryItem.removeAll()
//                self.arrCategory = result
//                for item in result
//                {
//                    self.arrCategoryItem.append(item.getString(key: "CategoryName"))
//                }
//                self.performSelector(onMainThread: #selector(self.createDropdown(loadData:)), with: true, waitUntilDone: true)
//            }
//        }
       
        tblVW.tableFooterView = UIView()
        self.view.layoutIfNeeded()
        
    }

    @objc func AccButton1(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        navigationController?.popViewController(animated: true)
    }

    @objc func AccButton3(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")

        if button.tag == 1
        {
            let home = TabBarViewController()
            self.navigationController?.pushViewController(home, animated: true)
        }
    }

    @objc func createDropdown(loadData: Bool = false)
    {
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [kCTForegroundColorAttributeName: UIColor.white] as [NSAttributedStringKey : Any]

        menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: BTTitle.index(0), items: arrCategoryItem)

        categoryName = self.arrCategory[0].getString(key: "CategoryName")
        menuView.cellHeight = 50
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor.lightGray//(red: 0.0/255.0, green:160.0/255.0, blue:195.0/255.0, alpha: 1.0)
        menuView.shouldKeepSelectedCellColor = true
        menuView.cellTextLabelColor = UIColor.black
        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
        menuView.cellTextLabelAlignment = .left // .Center // .Right // .Left
        menuView.arrowPadding = 15
        menuView.animationDuration = 0.5
        menuView.maskBackgroundColor = UIColor.black
        menuView.maskBackgroundOpacity = 0.3
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> Void in
            print("Did select item at index: \(indexPath)")
            StoredData.shared.selectedCateogryId = self.arrCategory[indexPath].getInt(key: "CategoryID")
            self.selectedCatId = StoredData.shared.selectedCateogryId
            self.getBlogs(StoredData.shared.selectedCateogryId)
            self.self.categoryName = self.arrCategory[indexPath].getString(key: "CategoryName")
        }

        self.navigationItem.titleView = menuView
        if !loadData
        {
            getBlogs(arrCategory[0].getInt(key: "CategoryID"))
        }
    }

    override func viewWillAppear(_ animated: Bool) {

//        if arrCategory.count > 0
//        {
            self.getBlogs(StoredData.shared.selectedCateogryId)
//        }
        if menuView != nil
        {
            createDropdown(loadData: true)
            menuView.setSelected(index: StoredData.shared.selectedCateogryIndex)
            menuView.setMenuTitle(arrCategory[0].getString(key: "CategoryName"))
        }
    
    }

    //MARK:- WEB SERVICE
    func getBlogs(_ id: Int)
    {
        if arrBlogs.count > 0
        {
            arrBlogs.removeAll()
            arrResult.removeAll()
        }
        
//        let param = [
//            "CategoryID": id
//        ]

        let param = [
            "CategoryID": id
        ]
        
 //getBlogList param: [:]) getBlog
        selectedCatId = id
        callApi(Path.getBlogList, method: .post, param: param)
        { (result) in
            print(result)
            if result.getBool(key: "success")
            {
                for view in self.tableView.subviews
                {
                    view.removeFromSuperview()
                }
                self.arrBlogs = result.getArrayofDictionary(key: "data")
                self.arrResult = self.arrBlogs
            }
            else
            {
                self.tableView.showEmptyMessage(message: result.getString(key: "message"))
            }
            
            self.tblVW.reloadData()
        }
    }

    //MARK: - Searchbar Delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        arrResult = arrBlogs.filter { (dict) -> Bool in

            if dict.getString(key: "PostedByName").lowercased().contains(searchText.lowercased())
            {
                return true
            }
            return false
        }
        
        print(arrResult)
        
        if searchText == ""
        {
            arrResult = arrBlogs
        }
           tableView.reloadData()
    }
    
//MARK: - Methods(Functions)☝🏻😳
    
    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {

            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            
            revealController.frontViewController.view.alpha = 1
            self.view.isUserInteractionEnabled = true
        }
    }
    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
//    {
//        return arrResult.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
//    {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MenuTableViewCell
//
//        let record = arrResult[indexPath.row]
//
//        cell.Blog_list_VW.layer.borderWidth = 1.0
//        cell.Blog_list_VW.layer.borderColor = UIColor.lightGray.cgColor
//        cell.Blog_list_VW.layer.cornerRadius = 5.0
//
//        cell.Btn_Closet.tag = indexPath.row
//        cell.Btn_Closet.addTarget(self, action: #selector(buttonClicked(sender:)), for: UIControlEvents.touchUpInside)
//        cell.Btn_Closet.isSelected = record.getBool(key: "IsCloset")
//
//        cell.btnLike.tag = indexPath.row
//        cell.btnLike.addTarget(self, action: #selector(btnLikeClicked(sender:)), for: UIControlEvents.touchUpInside)
//        cell.btnLike.isSelected = record.getBool(key: "isLike")
//
//        cell.Blog_list_img.layer.cornerRadius = cell.Blog_list_img.frame.height/2
//        cell.Blog_list_img.clipsToBounds = true
//        cell.Blog_list_img.backgroundColor = .lightGray
//        cell.Blog_list_img.setImage(record.getString(key: "ProfileImage"))
//        addGesture(cell.Blog_list_img, userId: record.getInt(key: "PostedByID"))
//        addGesture(cell.Blog_list_lbl_Title, userId: record.getInt(key: "PostedByID"))
//
//
//        cell.Blog_list_lbl_Banner.setImage(record.getString(key: "PostImage"))
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(gesterHandeler(_:)))
//        cell.Blog_list_lbl_Banner.isUserInteractionEnabled = true
//        cell.Blog_list_lbl_Banner.tag = indexPath.row
//        cell.Blog_list_lbl_Banner.addGestureRecognizer(tapGesture)
//        cell.Blog_list_lbl_Banner.contentMode = .scaleAspectFit
//
//        cell.Blog_list_lbl_Title.text = record.getString(key: "PostedByName")
//        cell.Blog_list_lbl_Owner.text = record.getString(key: "RegisterUserTypeByName")
//        cell.Blog_list_lbl_Time.text = record.getString(key: "PostTime").toDate().timeAgoSinceDate()
//        cell.Blog_list_lbl_Detail.text = record.getString(key: "PostTitle")
//        cell.lblComments.text = "\(record.getString(key: "TotalComment")) Comments"
//
//        cell.selectionStyle = .none
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
//    {
//        let nav = self.storyboard?.instantiateViewController(withIdentifier: "BlogDetailsVC") as! BlogDetailsVC
//        nav.blogId = arrResult[indexPath.row].getString(key: "BlogId")
//        nav.categoryName = categoryName
//        navigationController?.pushViewController(nav, animated: true)
//    }
    
    //MARK:- Tableview methods
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
           return 1000
       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:NewBlogListCell = tableView.dequeueReusableCell(withIdentifier: "NewBlogListCell") as! NewBlogListCell
        cell.layoutIfNeeded()

        cell.vwBG.setRadius(radius: 5)
        cell.vwBG.setRadiusBorder(color: UIColor.lightGray)

        cell.imgUser.setRadius(radius: cell.imgUser.frame.height/2)
        cell.imgUser.setRadiusBorder(color: UIColor.lightGray)
        cell.delegate = self

        cell.btnViewAll.setRadius(radius: 5)
        cell.btnViewAll.setRadiusBorder(color: UIColor.lightGray)
        
        cell.lblTitle.text = arrResult[indexPath.row]["CompanyName"] as? String
        cell.lblSubtitle.text = arrResult[indexPath.row]["PostedByName"] as? String

        let imgURL = arrResult[indexPath.row]["UserProfileImage"] as? String
        
        cell.imgUser?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        cell.imgUser?.translatesAutoresizingMaskIntoConstraints = true
        //imgUser?.contentMode = .ScaleAspectFit
        cell.imgUser.setImage(imgURL ?? "")
      
        
        cell.arrBlog.removeAll()
        cell.arrBlog = arrResult[indexPath.row]["Blogs"] as? [[String : Any]] ?? []
        cell.tblVW.reloadData()
        
//        if cell.arrBlog.count == 0
//        {
//            cell.constTblHeight.constant = 1
//        }
//        else
//        {
//            cell.constTblHeight.constant = cell.tblVW.contentSize.height
//        }
        
        cell.viewDidLayoutSubviews()
        cell.layoutSubviews()
        cell.btnViewAll.tag = indexPath.row
        cell.btnViewAll.addTarget(self, action: #selector(self.btnViewAllAction(_:)), for: .touchUpInside)
        
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        
        cell.selectionStyle = .none
        print("OUTERCELLHEIGHT",cell.contentView.frame.size.height)
        return cell
        
    }
    
    
    //MARK:- Actions👊🏻😠
    
    @objc func buttonClicked(sender:UIButton) {
        let param = [
            "BlogId": arrResult[sender.tag].getString(key: "BlogId")
        ]
        callApi(Path.addToCloset, method: .post, param: param) { (result) in
            print(result)
            if result.getBool(key: "success")
            {
                sender.isSelected = !sender.isSelected
            }
        }
    }

    @objc func btnLikeClicked(sender:UIButton) {
        let param = [
            "PostId": arrResult[sender.tag].getString(key: "BlogId"),
            "isLike": sender.isSelected ? "0" : "1"
        ]
        callApi(Path.likePost, method: .post, param: param) { (result) in
            print(result)
            if result.getBool(key: "success")
            {
                sender.isSelected = !sender.isSelected
            }
        }
    }
    
    @objc func AccButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")

        if button.tag == 1
        {
            let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    
    @objc func SearchButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        navigationItem.rightBarButtonItems = nil

        let searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 200, height: 20))
        searchBar.placeholder = "Search"
        searchBar.delegate = self
        navigationItem.titleView = searchBar
        title = nil

        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        menuButton.addTarget(self, action: #selector(searchAction(_:)), for: .touchUpInside)
    }

    @objc func searchAction(_ button: UIButton) {

        navigationItem.titleView = nil
        title = "MY BLOGS"

        let menuButton = UIButton(type: .system)

        if !isFromMenu
        {
            let menuButton = UIButton(type: .system)
            menuButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
            menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
            menuButton.addTarget(self, action: #selector(AccButton1(_:)), for: .touchUpInside)
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        }
        else
        {
            //            let menuButton = UIButton(type: .system)
            menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
            menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        }


       // let AccButton = UIButton(type: .system)
        let AccButton = SSBadgeButton()
          if UserDefaults.standard.object(forKey: "notification") != nil {
              let noticount = UserDefaults.standard.string(forKey: "notification")
              AccButton.badge = noticount
          }
          else
          {
               AccButton.badge = nil
          }
        AccButton.setImage(#imageLiteral(resourceName: "Notification").withRenderingMode(.alwaysOriginal), for: .normal)
        AccButton.tag = 1
        AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        AccButton.addTarget(self, action: #selector(AccButton(_:)), for: .touchUpInside)
        let barButtonItem = UIBarButtonItem(customView: AccButton)

        let SearchButton = UIButton(type: .system)
        SearchButton.tag = 2
        SearchButton.setImage(#imageLiteral(resourceName: "Search").withRenderingMode(.alwaysOriginal), for: .normal)
        SearchButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        SearchButton.addTarget(self, action: #selector(SearchButton(_:)), for: .touchUpInside)
        let barButtonItem2 = UIBarButtonItem(customView: SearchButton)

        self.navigationItem.rightBarButtonItems = [barButtonItem, barButtonItem2]

        //SideBar
        if revealViewController() != nil && isFromMenu
        {
            self.revealViewController().delegate = self
            menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

            let AccButton3 = UIButton(type: .system)
            AccButton3.setImage(#imageLiteral(resourceName: "home").withRenderingMode(.alwaysOriginal), for: .normal)
            AccButton3.tag = 1
            AccButton3.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
            AccButton3.addTarget(self, action: #selector(AccButton3(_:)), for: .touchUpInside)
            let barButtonItem3 = UIBarButtonItem(customView: AccButton3)
            self.navigationItem.rightBarButtonItems = [barButtonItem, barButtonItem2, barButtonItem3]
        }

        if isFromMenu
        {
            StoredData.shared.getCategory { (result) in
                self.arrCategoryItem.removeAll()
                self.arrCategory = result
                for item in result
                {
                    self.arrCategoryItem.append(item.getString(key: "CategoryName"))
                }
                self.performSelector(onMainThread: #selector(self.createDropdown(loadData:)), with: true, waitUntilDone: true)
            }
        }

        arrResult = arrBlogs
        tableView.reloadData()
    }

    @IBOutlet weak var viewZoomPopup: UIView!
    @IBOutlet weak var imgZoomImage: UIImageView!

    @IBAction func btnHideZoomPopupAction(_ sender: UIButton) {

        viewZoomPopup.isHidden = true
    }
    
    @objc func gesterHandeler(_ sender: UITapGestureRecognizer)
    {
        let row = sender.view!.tag
        imgZoomImage.setImage(arrResult[row].getString(key: "PostImage"))
        viewZoomPopup.isHidden = false
    }
    
    @IBAction func btnAddBlogAction(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddNewBlogView") as? AddNewBlogView
        vc?.selectedCategoryId = selectedCatId
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func btnViewAllAction(_ sender : UIButton)
    {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "BlogViewAllView") as! BlogViewAllView
        nextViewController.postId = arrResult[sender.tag]["Id"] as? String ?? "0"
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func didPressCell(sender: [String : Any]) {
//        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "BlogViewAllView") as! BlogViewAllView
//        nextViewController.postId = sender["PostedByID"] as? String ?? "0"
//        self.navigationController?.pushViewController(nextViewController, animated: true)
        
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "BlogListDetailCommentView") as! BlogListDetailCommentView
        nav.dictData = sender
        nav.postId1 = sender["PostedByID"] as? String ?? "0"
        navigationController?.pushViewController(nav, animated: true)
    }
    
}

extension String {
    
    func URLQueryAllowedString() -> String? {
        let escapedString = self.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        return escapedString
    }
}
