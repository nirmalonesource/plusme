
import UIKit

class MenuTableViewCell: UITableViewCell
{
    //table main menu
    @IBOutlet var imgprivacy: UIImageView!
    @IBOutlet var imglogout: UIImageView!
    @IBOutlet var lbllogout: UILabel!
    @IBOutlet var lblprivacy: UILabel!
    @IBOutlet var imgterm: UIImageView!
    @IBOutlet var lblterm: UILabel!
    @IBOutlet var imgpass: UIImageView!
    @IBOutlet var lblpass: UILabel!
    @IBOutlet var lblabout: UILabel!
    @IBOutlet var imgabout: UIImageView!
    @IBOutlet var lblfeed: UILabel!
    @IBOutlet var imgfeed: UIImageView!
    @IBOutlet var imgpost: UIImageView!
    @IBOutlet var lblpost: UILabel!
    
    //blog listing page
    @IBOutlet weak var Blog_list_VW: UIView!
    @IBOutlet weak var Blog_list_img: UIImageView!
    @IBOutlet weak var Blog_list_lbl_Owner: UILabel!
    @IBOutlet weak var Blog_list_lbl_Title: UILabel!
    @IBOutlet weak var Blog_list_lbl_Time: UILabel!
    @IBOutlet weak var Blog_list_lbl_Banner: UIImageView!
    @IBOutlet weak var Blog_list_lbl_Detail: UILabel!
    @IBOutlet weak var Btn_Closet: UIButton!
    @IBOutlet weak var btnLike: UIButton!

    @IBOutlet var btnreport: UIButton!
    @IBOutlet var lbltotallike: UILabel!
    @IBOutlet weak var lblComments: UILabel!
    
    @IBOutlet weak var feedImageHeightConstraint: NSLayoutConstraint!
    
    //my feeds page
    @IBOutlet weak var Feed_VW: UIView!
    @IBOutlet weak var Feed_img: UIImageView!
    @IBOutlet weak var Feed_lbl_Title: UILabel!
    @IBOutlet weak var Feed_lbl_Time: UILabel!
    @IBOutlet weak var Feed_lbl_Owner: UILabel!
    @IBOutlet weak var Feed_lbl_Detail: UILabel!
    @IBOutlet weak var Feed_Banner: UIImageView!
    
    //stores page
    @IBOutlet weak var Stores_View: UIView!
    @IBOutlet weak var Stores_img: UIImageView!
    @IBOutlet weak var Stores_lbl_Details: UILabel!
    @IBOutlet weak var Stores_lbl_Address: UILabel!
    @IBOutlet weak var Stores_lbl_Name: UILabel!
    
    
    //notification page
    @IBOutlet weak var Notification_date: UILabel!
    @IBOutlet weak var Notification_lbl: UILabel!
    @IBOutlet weak var Notification_View: UIView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        
        
    }
    
    internal var aspectConstraint : NSLayoutConstraint? {
           didSet {
               if oldValue != nil {
                   Blog_list_lbl_Banner.removeConstraint(oldValue!)
               }
               if aspectConstraint != nil {
                   Blog_list_lbl_Banner.addConstraint(aspectConstraint!)
               }
           }
       }
    
    func setCustomImage(image : UIImage) {
           
           let aspect = image.size.width / image.size.height
           
           let constraint = NSLayoutConstraint(item: Blog_list_lbl_Banner, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: Blog_list_lbl_Banner, attribute: NSLayoutConstraint.Attribute.height, multiplier: aspect, constant: 0.0)
           constraint.priority = UILayoutPriority(rawValue: 999)
           
           aspectConstraint = constraint
           
           Blog_list_lbl_Banner.image = image
        
        setNeedsLayout()
       }
       
       override func prepareForReuse() {
           super.prepareForReuse()
           aspectConstraint = nil
       }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        
    }

}
