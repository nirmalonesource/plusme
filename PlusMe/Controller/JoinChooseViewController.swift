//
//  JoinChooseViewController.swift
//  PlusMe
//
//  Created by Priyank Jotangiya on 23/07/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit

class JoinChooseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true

    }

    
    @IBAction func TrendsetterButtonPressed(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignupViewController") as? SignupViewController
        vc?.userType = 0
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func StylistButtonPressed(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignupViewController") as? SignupViewController
        vc?.userType = 1
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
