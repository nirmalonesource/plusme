//
//  JoinViewController.swift
//  PlusMe
//
//  Created by Priyank Jotangiya on 20/07/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit

class JoinViewController: UIViewController {

//MARK: - Outlets🤔
    @IBOutlet var Btn_Join: UIButton!
    @IBOutlet var Btn_Signin: UIButton!
    
    var additionalData = [String:Any]()
    var isfrompush = false
    
//MARK: - Declare Variables👇🏻😬

    
//MARK: - Happy Coding😊
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        //Buttons
        Btn_Join.layer.cornerRadius = 20.0
        Btn_Signin.layer.cornerRadius = 20.0

        if StoredData.shared.isLogedin
        {
            let home = TabBarViewController()
            self.navigationController?.pushViewController(home, animated: false)
//            StoredData.shared.rearViewController.updateUI()
        }
    }
    
    @IBAction func JoinNowButtonPressed(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "JoinChooseViewController") as? JoinChooseViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    @IBAction func SignInButtonPressed(_ sender: Any) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
