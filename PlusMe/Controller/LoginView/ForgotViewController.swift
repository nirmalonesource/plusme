//
//  ForgotViewController.swift
//  PlusMe
//
//  Created by Priyank Jotangiya on 23/07/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//


import UIKit

class ForgotViewController: UIViewController {

    @IBOutlet var Vw_Cancel: UIView!
    @IBOutlet weak var Btn_send: UIButton!
    @IBOutlet weak var txtEmail: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.

        txtEmail.ChangePlaceholderColor(placeholder: "Enter Email");
        self.navigationController?.navigationBar.isHidden = true
        
        let gesture = UITapGestureRecognizer(target: self, action: "someAction:")
        // or for swift 2 +
        let gestureSwift2AndHigher = UITapGestureRecognizer(target: self, action:  #selector (self.someAction (_:)))
        Vw_Cancel.addGestureRecognizer(gesture)
        
        Btn_send.layer.cornerRadius = Btn_send.frame.size.height / 2.0
        Btn_send.clipsToBounds = true
    }

    fileprivate func isValidateData() -> Bool
    {
        if txtEmail.text!.isEmpty()
        {
            showMessage(message: "Please enter email.", completionHandler: nil)
            return false
        }
        else if !txtEmail.text!.isValidEmail()
        {
            showMessage(message: "Please enter valid email.", completionHandler: nil)
            return false
        }
        return true
    }

    @IBAction func LoginButtonPressed(_ sender: Any) {
        if isValidateData()
        {
            let param: [String: Any] = [
                "Email": txtEmail.text!,
            ]

            callApi(Path.forgotPassword, method: .post, param: param) { (result) in

                print(result)
                if result.getBool(key: "success")
                {
                    self.txtEmail.text = ""
                    self.showMessage(message: result.getString(key: "message"), completionHandler: {
                        self.navigationController?.popViewController(animated: true)
                    })

                }
                else
                {
                    self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                }
            }

        }
    }
    
    // or for Swift 4
    @objc func someAction(_ sender:UITapGestureRecognizer){
        // do other task
        self.navigationController?.popViewController(animated: true)
    }

}
