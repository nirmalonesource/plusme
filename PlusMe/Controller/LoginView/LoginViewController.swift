//
//  LoginViewController.swift
//  PlusMe
//
//  Created by Priyank Jotangiya on 23/07/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet var Lbl_Signup: UILabel!
    @IBOutlet weak var Btn_Login: UIButton!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    let kUserDefault = UserDefaults.standard
    var playerId: String?
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        playerId = kUserDefault.string(forKey: "playerId")
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
        
        let CreateAcc = NSMutableAttributedString(string: Lbl_Signup.text!, attributes: nil)
        //NSRange linkRange2 = NSMakeRange(24, 15); // for the word "link" in the string above
        let AccString = "Don't have an account? Signup"
        let linkRange2: NSRange = (AccString as NSString).range(of: "Signup")
        let linkAttributes2 = [NSAttributedStringKey.foregroundColor: UIColor.init(red:232/255.0, green: 59/255.0, blue: 119/255.0, alpha: 1)]
        CreateAcc.setAttributes(linkAttributes2, range: linkRange2)
        // Assign attributedText to UILabel
        Lbl_Signup.attributedText = CreateAcc
        Lbl_Signup.isUserInteractionEnabled = true
        Lbl_Signup.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleCreateAcc)))
        
        Btn_Login.layer.cornerRadius = Btn_Login.frame.size.height / 2.0
        Btn_Login.clipsToBounds = true

        txtUsername.ChangePlaceholderColor(placeholder: "Username/Email")
        txtPassword.ChangePlaceholderColor(placeholder: "Password")
    }
    
    // or for Swift 4
    @objc func handleCreateAcc(_ sender:UITapGestureRecognizer){
        // do other task
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "JoinChooseViewController") as? JoinChooseViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }

    fileprivate func isValidateData() -> Bool
    {
        if txtUsername.text!.isEmpty()
        {
            showMessage(message: "Please enter username.", completionHandler: nil)
            return false
        }
            
        else if txtPassword.text!.isEmpty()
        {
            showMessage(message: "Please enter password.", completionHandler: nil)
            return false
        }
        return true
    }
    
    @IBAction func LoginButtonPressed(_ sender: Any) {
      
        if isValidateData()
        {
            let param: [String: Any] = [
                "Email": txtUsername.text!,
                "Password": txtPassword.text!,
                "DeviceId": 1,
                "Devicetoken": StoredData.shared.playerId!,
                "IMEI": StoredData.shared.udid,
                "DeviceName": UIDevice.modelName,
                "OSVersion": "\(UIDevice.current.systemName) \(UIDevice.current.systemVersion)"
            ]

            callApi(Path.login, method: .post, param: param) { (result) in

                print(result)
                if result.getBool(key: "status")
                {
                    self.txtUsername.text = ""
                    self.txtPassword.text = ""
                    let data = result.getDictionary(key: "user_data")
                   // self.kUserDefault.removeObject(forKey: "playerId")
                    UserDefaults.standard.setValuesForKeys(data)
                    let home = TabBarViewController()
                    self.navigationController?.pushViewController(home, animated: true)
                    if StoredData.shared.rearViewController != nil
                    {
                        StoredData.shared.rearViewController.updateUI()
                    }
                }
                else
                {
                    self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                }
            }
        }
    }
    
    @IBAction func ForgotButtonPressed(_ sender: Any) {
        let home = self.storyboard?.instantiateViewController(withIdentifier: "ForgotViewController") as! ForgotViewController
        self.navigationController?.pushViewController(home, animated: true)
    }
}
