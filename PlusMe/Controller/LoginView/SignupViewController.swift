//
//  SignupViewController.swift
//  PlusMe
//
//  Created by Priyank Jotangiya on 23/07/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet var Lbl_Signin: UILabel!
    @IBOutlet weak var Btn_Register: UIButton!
    
    @IBOutlet weak var txt_firstname: UITextField!
    @IBOutlet weak var txt_lastname: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_mobile: UITextField!
    @IBOutlet weak var txt_confirmpass: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtPinCode: UITextField!
    @IBOutlet var vwcity: UIView!
     @IBOutlet weak var imgProfile: UIImageView!
        
    @IBOutlet weak var imgcheck: UIImageView!
    
    var userType = 0
    var arrCountry = [[String: Any]]()
    var arrStates = [[String: Any]]()
    var arrCity = [[String: Any]]()
    var pickerView: UIPickerView!
    var isterms = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtCity.isEnabled = false
        txtState.isEnabled = false
        txtCountry.delegate = self
        txtState.delegate = self
        txtCity.delegate = self
        txt_mobile.delegate = self

        pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        txtCountry.inputView = pickerView
        txtState.inputView = pickerView
        txtCity.inputView = pickerView
        getCountry()

        let CreateAcc = NSMutableAttributedString(string: Lbl_Signin.text!, attributes: nil)
        //NSRange linkRange2 = NSMakeRange(24, 15); // for the word "link" in the string above
        let AccString = "Already have an account? Sign In"
        let linkRange2: NSRange = (AccString as NSString).range(of: "Sign In")
        let linkAttributes2 = [NSAttributedStringKey.foregroundColor: UIColor.init(red:232/255.0, green: 59/255.0, blue: 119/255.0, alpha: 1)]
        CreateAcc.setAttributes(linkAttributes2, range: linkRange2)
        // Assign attributedText to UILabel
        Lbl_Signin.attributedText = CreateAcc
        Lbl_Signin.isUserInteractionEnabled = true
        Lbl_Signin.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleCreateAcc)))
        
        txt_firstname.ChangePlaceholderColor(placeholder: "First Name")
        txt_lastname.ChangePlaceholderColor(placeholder: "Last Name")
        txt_email.ChangePlaceholderColor(placeholder: "Email Id")
        txt_mobile.ChangePlaceholderColor(placeholder: "Mobile No")
        txt_password.ChangePlaceholderColor(placeholder: "Password")
        txt_confirmpass.ChangePlaceholderColor(placeholder: "Confirm Password")
        txtState.ChangePlaceholderColor(placeholder: "State")
        txtCountry.ChangePlaceholderColor(placeholder: "Country")
        txtCity.ChangePlaceholderColor(placeholder: "City")
        txtPinCode.ChangePlaceholderColor(placeholder: "Pin Code")
        
        Btn_Register.layer.cornerRadius = Btn_Register.frame.size.height/2.0
        Btn_Register.clipsToBounds = true
         imgProfile.roundView()
        
        imgcheck.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleTandC)))
        
        getState()
    }

    func getState()
    {
        let param = [
           // "CountryID": txtCountry.tag
            "CountryID": 231
        ]
        callApi(Path.getStates, method: .post, param: param) { (result) in

            print(result)
            if result.getBool(key: "status")
            {
                self.arrStates = result.getArrayofDictionary(key: "data")
                self.txtState.isEnabled = true
                self.pickerView.reloadAllComponents()
            }
        }
    }

    func getCity()
    {
        let param = [
            "StateID": txtState.tag
        ]
        callApi(Path.getCity, method: .post, param: param) { (result) in

            print(result)
            if result.getBool(key: "status")
            {
                self.arrCity = result.getArrayofDictionary(key: "data")
                self.txtCity.isEnabled = true
                if self.arrCity.count > 0 {
                      self.vwcity.isHidden = false
                }
                else
                {
                     self.vwcity.isHidden = true
                }
                self.pickerView.reloadAllComponents()
            }
        }
    }

    func getCountry()
    {
        callApi(Path.getCountry, method: .get) { (result) in

            print(result)
            if result.getBool(key: "status")
            {
                self.arrCountry = result.getArrayofDictionary(key: "data")
                self.pickerView.reloadAllComponents()
            }
        }
    }
    
    @objc func handleTandC(_ sender:UITapGestureRecognizer){
        // do other task
        if isterms {
            isterms = false
            imgcheck.image = UIImage(named: "blank-check-box")
        }
        else
        {
            isterms = true
            imgcheck.image = UIImage(named: "checkbox")
        }

    }
    
    // or for Swift 4
    @objc func handleCreateAcc(_ sender:UITapGestureRecognizer){
        // do other task
        let vc = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
        self.navigationController?.pushViewController(vc!, animated: true)

    }
    fileprivate func validateData() -> Bool
    {
        if txt_firstname.text!.isEmpty()
        {
            showMessage(message: "Please enter firstname.", completionHandler: nil)
            return false
        }
        else if txt_lastname.text!.isEmpty()
        {
            showMessage(message: "Please enter lastname.", completionHandler: nil)
            return false
        }
        else if txt_email.text!.isEmpty()
        {
            showMessage(message: "Please enter email.", completionHandler: nil)
            return false
        }
        else if !txt_email.text!.isValidEmail()
        {
            showMessage(message: "Please enter valid email.", completionHandler: nil)
            return false
        }
        else if txt_mobile.text!.isEmpty()
        {
            showMessage(message: "Please enter mobile number.", completionHandler: nil)
            return false
        }
        else if !txt_mobile.text!.validPhoneNo()
        {
            showMessage(message: "Please enter valid mobile number.", completionHandler: nil)
            return false
        }
        else if txt_password.text!.isEmpty()
        {
            showMessage(message: "Please enter password.", completionHandler: nil)
            return false
        }
        else if txt_confirmpass.text!.isEmpty()
        {
            showMessage(message: "Please enter confirm password.", completionHandler: nil)
            return false
        }
        else if txt_password.text! != txt_confirmpass.text!
        {
            showMessage(message: "Confirem password does not match with new password.", completionHandler: nil)
            return false
        }
        else if txtCountry.text!.isEmpty()
        {
            showMessage(message: "Please select country.", completionHandler: nil)
            return false
        }
        else if txtState.text!.isEmpty()
        {
            showMessage(message: "Please select state.", completionHandler: nil)
            return false
        }
        else if self.arrCity.count > 0
        {
            if txtCity.text!.isEmpty()
            {
                showMessage(message: "Please select city.", completionHandler: nil)
                return false
            }
        }
        else if txtPinCode.text!.isEmpty()
        {
            showMessage(message: "Please enter pin code.", completionHandler: nil)
            return false
        }
        else if !isterms
        {
            showMessage(message: "Please agree terms and conditions", completionHandler: nil)
            return false
        }
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")

        if (isBackSpace == -92) {
            return true
        }

        if txt_mobile == textField && txt_mobile.text!.count >= 10
        {
            return false
        }
        return true
    }
    @IBAction func Terms_click(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "termConditionViewController") as! termConditionViewController
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.pushViewController(vc, animated: true)
    //    self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func RegiButtonPressed(_ sender: Any) {
        if validateData()
        {
            view.endEditing(true)
            let mobile = "+1" + txt_mobile.text!
            let param = [
                "FirstName": txt_firstname.text!,
                "LastName": txt_lastname.text!,
                "Email": txt_email.text!,
                "MobileNo": mobile,
                "Password": txt_password.text!,
                "RegisterUserType": userType,
                "IMEI": StoredData.shared.udid,
                "DeviceName": UIDevice.modelName,
                "OSVersion": "\(UIDevice.current.systemName) \(UIDevice.current.systemVersion)",
                "CountryID": 231,
                "StateID": txtState.tag,
                "CityID": txtCity.tag,
                "ZipCode": txtPinCode.text!,
                "DeviceToken": StoredData.shared.deviceToken
                ] as [String : Any]
          //  callApi(Path.register, method: .post, param: param) { (result) in
            callApi(Path.register, method: .post, param: param, data: [UIImagePNGRepresentation(imgProfile.image!)!], dataKey: ["ProfileImage"]) { (result) in
                print(result)
                if result.getBool(key: "success")
                {
                    self.resetForm()
                    let data = result.getDictionary(key: "data")
                    self.showMessage(message: result.getString(key: "message"), completionHandler: {

                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
                        self.navigationController?.pushViewController(vc!, animated: true)                    })
                }
                else
                {
                    self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                }
            }
        }
    }
    
    func resetForm()
    {
        txt_firstname.text = ""
        txt_lastname.text = ""
        txt_email.text = ""
        txt_mobile.text = ""
        txt_confirmpass.text = ""
        txt_password.text = ""
        txtState.text = ""
        txtCity.text = ""
       // txtCountry.text = ""
        txtPinCode.text = ""
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        if txtCountry.isEditing
        {
            return arrCountry.count
        }
        else if txtState.isEditing
        {
            return arrStates.count
        }
        else if txtCity.isEditing
        {
            return arrCity.count
        }
        return 0
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

        if txtCountry.isEditing
        {
            return arrCountry[row].getString(key: "Name")
        }
        else if txtState.isEditing
        {
            return arrStates[row].getString(key: "Name")
        }
        else if txtCity.isEditing
        {
            return arrCity[row].getString(key: "Name")
        }
        return ""
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {

        if txtCountry.isEditing
        {
            txtCountry.text = arrCountry[0].getString(key: "Name")
            txtCountry.tag = arrCountry[0].getInt(key: "Id")
            txtState.text = ""
            txtState.tag = 0
            txtCity.text = ""
            txtCity.tag = 0
        }
        else if txtState.isEditing && arrStates.count > 0
        {
            txtState.text = arrStates[0].getString(key: "Name")
            txtState.tag = arrStates[0].getInt(key: "Id")
            txtCity.text = ""
            txtCity.tag = 0
        }
        else if txtCity.isEditing && arrCity.count > 0
        {
            txtCity.text = arrCity[0].getString(key: "Name")
            txtCity.tag = arrCity[0].getInt(key: "Id")
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        if txtCountry.isEditing
        {
            txtCountry.text = arrCountry[row].getString(key: "Name")
            txtCountry.tag = arrCountry[row].getInt(key: "Id")
        }
        else if txtState.isEditing
        {
            txtState.text = arrStates[row].getString(key: "Name")
            txtState.tag = arrStates[row].getInt(key: "Id")
        }
        else if txtCity.isEditing
        {
            txtCity.text = arrCity[row].getString(key: "Name")
            txtCity.tag = arrCity[row].getInt(key: "Id")
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {

        if textField == txtCountry
        {
            getState()
        }
        else if textField == txtState
        {
            getCity()
        }
    }
    @IBAction func btnCameraAction(_ sender: UIButton) {

           let imagePicker = UIImagePickerController()

           let actionSheet = UIAlertController(title: "Take image", message: nil, preferredStyle: .actionSheet)
           actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in

               if UIImagePickerController.isSourceTypeAvailable(.camera)
               {
                   imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                   imagePicker.delegate = self
                   imagePicker.isEditing = true
                   imagePicker.allowsEditing = true
                   self.present(imagePicker, animated: true, completion: nil)
               }
               else
               {
                   self.showMessage(message: "Camera Not Available", completionHandler: nil)
               }
           }))
           actionSheet.addAction(UIAlertAction(title: "Photos", style: .default, handler: { (action) in

               imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
               imagePicker.delegate = self
               imagePicker.isEditing = true
               imagePicker.allowsEditing = true
               self.present(imagePicker, animated: true, completion: nil)

           }))
           actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in


           }))

           present(actionSheet, animated: true, completion: nil)

       }
    // MARK: - ImagePicker delegate
       func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

           let image = info[UIImagePickerControllerEditedImage] as! UIImage
           let fixedImaage = UIImage(cgImage: image.cgImage!, scale: image.scale, orientation: image.imageOrientation)

           imgProfile.image = fixedImaage.resize(scale: 0.25)
           picker.dismiss(animated: true, completion: nil)
       }
}
