//
//  ClosetImagesCollectionView.swift
//  PlusMe
//
//  Created by Priyank Jotangiya on 23/07/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit

class ClosetImagesCollectionView: UICollectionViewCell
{
    
    //closet page
    @IBOutlet weak var ClosetImg: UIImageView!
    @IBOutlet var Lbl_ClosetTitle: UILabel!
    
}
