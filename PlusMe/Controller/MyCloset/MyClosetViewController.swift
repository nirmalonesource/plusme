//
//  MyClosetViewController.swift
//  PlusMe
//
//  Created by Priyank Jotangiya on 23/07/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit
import SWRevealViewController

class MyClosetViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,SWRevealViewControllerDelegate
{
    @IBOutlet weak var vwNoData: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewZoomImage: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnRemove: UIButton!
    
    var arrClosets = [[String: Any]]()
    var a : Int?
    var menuView: BTNavigationDropdownMenu!
    var arrCategory = [[String: Any]]()
    var arrCategoryItem = [String]()
    var categoryName = ""
    var ClosetID = Int()
    
    //MARK:- View Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()

        btnEdit.roundCorner()
        btnRemove.roundCorner()
        
     
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "VisionBoardViewController") as! VisionBoardViewController
        nav.resetview()
        
        StoredData.shared.isfrommycloset = false
           //NavigationBar
                self.navigationController?.navigationBar.isHidden = false;
                self.title = "My Closet"
                
                navigationController?.navigationBar.barTintColor = UIColor.white
                navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
                self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }
                
                let menuButton = UIButton(type: .system)
                menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
                menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
                navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
                
               // let AccButton = UIButton(type: .system)
                let AccButton = SSBadgeButton()
                         if UserDefaults.standard.object(forKey: "notification") != nil {
                             let noticount = UserDefaults.standard.string(forKey: "notification")
                             AccButton.badge = noticount
                         }
                         else
                         {
                              AccButton.badge = nil
                         }
                AccButton.setImage(#imageLiteral(resourceName: "Notification").withRenderingMode(.alwaysOriginal), for: .normal)
                AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
                AccButton.addTarget(self, action: #selector(AccButton(_:)), for: .touchUpInside)
                let barButtonItem = UIBarButtonItem(customView: AccButton)
                
        //        let AccButton2 = UIButton(type: .system)
        //        AccButton2.setImage(#imageLiteral(resourceName: "Search").withRenderingMode(.alwaysOriginal), for: .normal)
        //        AccButton2.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        //        AccButton2.addTarget(self, action: #selector(AccButton2(_:)), for: .touchUpInside)
        //        let barButtonItem2 = UIBarButtonItem(customView: AccButton2)

                self.navigationItem.rightBarButtonItems = [barButtonItem]
                
                //SideBar
                if revealViewController() != nil
                {
                    self.revealViewController().delegate = self
                    menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
                    self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                    view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                }

//        if arrCategory.count > 0
//        {
//            createDropdown()
//        }
//        else {
            self.loadMyClosets(CategoryID: StoredData.shared.selectedCateogryId)
       // }

//     else{
//
//            StoredData.shared.getCategory { (result) in
//                self.arrCategoryItem.removeAll()
//                self.arrCategory = result
//                for item in result
//                {
//                    self.arrCategoryItem.append(item.getString(key: "CategoryName"))
//                }
//                self.performSelector(onMainThread: #selector(self.createDropdown(loadData:)), with: true, waitUntilDone: true)
//            }
//        }
    }
 
    @objc func createDropdown(loadData: Bool = false)
    {
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [kCTForegroundColorAttributeName: UIColor.white] as [NSAttributedStringKey : Any]
        
        menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: BTTitle.index(0), items: arrCategoryItem)

        categoryName = self.arrCategory[0].getString(key: "CategoryName")
        menuView.cellHeight = 50
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor.lightGray//(red: 0.0/255.0, green:160.0/255.0, blue:195.0/255.0, alpha: 1.0)
        menuView.shouldKeepSelectedCellColor = true
        menuView.cellTextLabelColor = UIColor.black
        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
        menuView.cellTextLabelAlignment = .left // .Center // .Right // .Left
        menuView.arrowPadding = 15
        menuView.animationDuration = 0.5
        menuView.maskBackgroundColor = UIColor.black
        menuView.maskBackgroundOpacity = 0.3
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> Void in
            print("Did select item at index: \(indexPath)")
            StoredData.shared.selectedCateogryId = self.arrCategory[indexPath].getInt(key: "CategoryID")
            self.loadMyClosets(CategoryID: StoredData.shared.selectedCateogryId)
            self.self.categoryName = self.arrCategory[indexPath].getString(key: "CategoryName")
        }

        self.navigationItem.titleView = menuView
        if !loadData
        {
            loadMyClosets(CategoryID: arrCategory[0].getInt(key: "CategoryID"))
        }
    }

    func loadMyClosets(CategoryID: Int)
    {
        arrClosets.removeAll()
        let param = [
            "CategoryID": CategoryID
        ]
        callApi(Path.myClosetList, method: .post, param: param) { (result) in

            print(result)
            if result.getBool(key: "success")
            {
                for view in self.collectionView.subviews
                {
                    view.removeFromSuperview()
                }
                self.arrClosets = result.getArrayofDictionary(key: "data")
                self.vwNoData.isHidden = true
            }
            else
            {
                self.vwNoData.isHidden = false
//                self.collectionView.showEmptyMessage(message: result.getString(key: "message"))
            }
            self.collectionView.reloadData()
        }
    }
    
    func removeMyClosets(ClosetID: Int)
    {
        let param = [
            "ClosetID": ClosetID
        ]
        callApi(Path.removeCloset, method: .post, param: param) { (result) in
            
            print(result)
            if result.getBool(key: "success")
            {
                self.viewZoomImage.isHidden = true
               // self.createDropdown()
                 self.loadMyClosets(CategoryID: StoredData.shared.selectedCateogryId)
            }
            else
            {
                self.viewZoomImage.isHidden = false
            }
            self.collectionView.reloadData()
        }
    }

//MARK: - Actions👊🏻😠
    
    @IBAction func btnEditAction(_ sender: UIButton) {
        
//        let vc = storyboard?.instantiateViewController(withIdentifier: "VisionBoardViewController") as? VisionBoardViewController
        StoredData.shared.ClosetID = String(ClosetID)
        StoredData.shared.isfrommycloset = true
        StoredData.shared.isEditClosetImage = true
        StoredData.shared.closetImage = imageView.image!
        //vc?.hidesBottomBarWhenPushed = true
        tabBarController?.selectedIndex = 4
        //self.navigationController?.pushViewController(vc!, animated: true)
        
    }

    @IBAction func btnClosetAction(_ sender: UIButton) {
        removeMyClosets(ClosetID: ClosetID)
    }
    
    @objc func AccButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func AccButton2(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
    }
    
    //MARK: - Methods(Functions)☝🏻😳
    
    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {
            
            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            
        } else {
            revealController.frontViewController.view.alpha = 1
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrClosets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        // get a reference to our storyboard cell
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: "item", for: indexPath as IndexPath) as! ClosetImagesCollectionView
        
        item.layer.masksToBounds = true
        item.layer.cornerRadius = 5
        item.layer.borderWidth = 1
        item.layer.shadowOffset = CGSize(width: -1, height: 1)
        item.layer.borderColor = UIColor.lightGray.cgColor
        
        let flow = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        let itemSpacing: CGFloat = 3
        let itemsInOneLine: CGFloat = 3
        flow.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        let width = UIScreen.main.bounds.size.width - itemSpacing * CGFloat(itemsInOneLine - 1)
        flow.itemSize = CGSize(width: floor(width/itemsInOneLine), height: width/itemsInOneLine)
        flow.minimumInteritemSpacing = 3
        flow.minimumLineSpacing = 3

        let record = arrClosets[indexPath.row]

        item.ClosetImg.setImage(record.getString(key: "ClosetImage"))
        return item
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let record = arrClosets[indexPath.row]
        ClosetID = record.getInt(key: "ClosetID")
        viewZoomImage.isHidden = false
        let cell = collectionView.cellForItem(at: indexPath) as! ClosetImagesCollectionView
        imageView.image = cell.ClosetImg.image
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureHandeler(_:)))
        viewZoomImage.addGestureRecognizer(tapGesture)
    }

    @objc func tapGestureHandeler(_ gesture: UITapGestureRecognizer)
    {
        viewZoomImage.isHidden = true
    }
    
    //collectionViewLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = UIScreen.main.bounds.width
        return CGSize(width: (width - 10)/3, height: (width - 10)/3) // width & height are the same to make a square cell
    }
}
