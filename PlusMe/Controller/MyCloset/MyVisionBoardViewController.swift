//
//  MyClosetViewController.swift
//  PlusMe
//
//  Created by Priyank Jotangiya on 23/07/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit
import SWRevealViewController
import Alamofire

protocol VisionBoardImages {
    func visionBoardSelectedUrl(url: String, id: Int)
}

class MyVisionBoardViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, SWRevealViewControllerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, ClosetImages, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate
{
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewZoomImage: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var viewSavePopup: UIView!
    @IBOutlet weak var imageViewPopup: UIImageView!
    @IBOutlet weak var txtCategory: UITextField!
    @IBOutlet weak var btnMoveToCloset: UIButton!
    
    @IBOutlet weak var imgBlur: UIImageView!
    @IBOutlet weak var vwPostNew: UIView!
    @IBOutlet weak var txtNewPost: UITextField!
    
    var arrClosets = [[String: Any]]()
    var a : Int?
    var menuView: BTNavigationDropdownMenu!
    var arrCategory = [[String: Any]]()
    var arrCategoryItem = [String]()
    var categoryName = ""
    var closetImageId = 0
    var selectedCategoryId = Int("")
      var AddVisionBoardID = 0
    var picker: UIPickerView!
    var selectedImgURL = String()
    var isfromlook = Bool()
    var test: Bool = false
    var isfrompostnew: Bool = false
    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var vwNodata: UIView!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnPost: UIButton!
    @IBOutlet var btndelet: UIButton!
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAddPost: UIButton!
    var delegate: VisionBoardImages?
    
    @IBOutlet weak var vwAddImage: UIView!
    
    
    //MARK:- View Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()

        txtCategory.layer.borderColor = #colorLiteral(red: 0.9294117647, green: 0.2196078431, blue: 0.462745098, alpha: 1)
        txtCategory.layer.borderWidth = 1
        picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        txtCategory.inputView = picker
        txtCategory.delegate = self

        btnMoveToCloset.roundCorner()

//        StoredData.shared.getCategory { (result) in
//            self.arrCategoryItem.removeAll()
//            self.arrCategory = result
//
////            - key : "CategoryID"
////              - value : 9
////            ▿ 1 : 2 elements
////              - key : "CategoryImageUrl"
////              - value : https://plusme.blenzabi.com/admin/uploads/Category/1537265884.jpg
////            ▿ 2 : 2 elements
////              - key : "CategoryName"
////              - value : Party Style
//            for item in self.arrCategory
//            {
//                self.arrCategoryItem.append(item.getString(key: "CategoryName"))
//            }
//            self.picker.reloadAllComponents()
//        }

        vwPostNew.setRadius(radius: 10)
        btnPost.setRadius(radius: 10)
         btndelet.setRadius(radius: 10)
        btnShare.setRadius(radius: 10)
        btnCancel.setRadius(radius: 10)
        btnAddPost.setRadius(radius: 10)

       
        
    }

    @objc func HomeButtonAction(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")

        if button.tag == 1
        {
            if isfromlook {
              //   let nav = self.storyboard?.instantiateViewController(withIdentifier: "MyFeedViewController") as! MyFeedViewController
                       self.tabBarController?.selectedIndex = 2
                 //  self.navigationController?.pushViewController(nav, animated: true)
            }
            else
            {
                let home = TabBarViewController()
                self.tabBarController?.selectedIndex = 2
                self.navigationController?.pushViewController(home, animated: true)
            }
            
            
           
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        
        //NavigationBar
               self.navigationController?.navigationBar.isHidden = false;
               self.title = "VISION BOARD"
               
               navigationController?.navigationBar.barTintColor = UIColor.white
               navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
               self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }
               
               let menuButton = UIButton(type: .system)
               menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
               menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
               navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
               
               let AccButton = UIButton(type: .system)
               AccButton.setImage(#imageLiteral(resourceName: "addnew").withRenderingMode(.alwaysOriginal), for: .normal)
               AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
               AccButton.addTarget(self, action: #selector(AccButton(_:)), for: .touchUpInside)
               let barButtonItem = UIBarButtonItem(customView: AccButton)
               
               let HomeButton = UIButton(type: .system)
               HomeButton.setImage(#imageLiteral(resourceName: "home").withRenderingMode(.alwaysOriginal), for: .normal)
               HomeButton.tag = 1
               HomeButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
               HomeButton.addTarget(self, action: #selector(HomeButtonAction(_:)), for: .touchUpInside)
               let barButtonItem1 = UIBarButtonItem(customView: HomeButton)
               
              // let NotiButton = UIButton(type: .system)
               let NotiButton = SSBadgeButton()
                        if UserDefaults.standard.object(forKey: "notification") != nil {
                            let noticount = UserDefaults.standard.string(forKey: "notification")
                            NotiButton.badge = noticount
                        }
                        else
                        {
                             NotiButton.badge = nil
                        }
               NotiButton.setImage(#imageLiteral(resourceName: "Notification").withRenderingMode(.alwaysOriginal), for: .normal)
               NotiButton.tag = 1
               NotiButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
               NotiButton.addTarget(self, action: #selector(NotificationAction(_:)), for: .touchUpInside)
               let barButtonItem2 = UIBarButtonItem(customView: NotiButton)

               self.navigationItem.rightBarButtonItems = [barButtonItem2,barButtonItem, barButtonItem1]
               
               //SideBar
               if revealViewController() != nil
               {
                   self.revealViewController().delegate = self
                   menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
                   self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                   view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
               }

        if arrCategory.count > 0
        {
            createDropdown()
        }
        else
        {
            StoredData.shared.getCategory { (result) in
                self.arrCategoryItem.removeAll()
                self.arrCategory = result
                let firstdict = NSMutableDictionary()
                firstdict["CategoryID"] = ""
                 firstdict["CategoryImageUrl"] = ""
                 firstdict["CategoryName"] = "ALL"
                self.arrCategory.insert(firstdict as! [String : Any], at: 0)
                for item in self.arrCategory
                {
                    self.arrCategoryItem.append(item.getString(key: "CategoryName"))
                }
                self.performSelector(onMainThread: #selector(self.createDropdown(loadData:)), with: true, waitUntilDone: true)
            }
        }
    }

    @objc func createDropdown(loadData: Bool = false)
    {
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [kCTForegroundColorAttributeName: UIColor.white] as [NSAttributedStringKey : Any]
 //coment here

        menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: BTTitle.index(0), items: arrCategoryItem)

        categoryName = self.arrCategory[0].getString(key: "CategoryName")
        selectedCategoryId = self.arrCategory[0].getInt(key: "CategoryID")
       // selectedCategoryId = 0
        
        menuView.cellHeight = 50
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor.lightGray//(red: 0.0/255.0, green:160.0/255.0, blue:195.0/255.0, alpha: 1.0)
        menuView.shouldKeepSelectedCellColor = true
        menuView.cellTextLabelColor = UIColor.black
        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
        menuView.cellTextLabelAlignment = .left // .Center // .Right // .Left
        menuView.arrowPadding = 15
        menuView.animationDuration = 0.5
        menuView.maskBackgroundColor = UIColor.black
        menuView.maskBackgroundOpacity = 0.3
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> Void in
            print("Did select item at index: \(indexPath)")
            StoredData.shared.selectedCateogryId = self.arrCategory[indexPath].getInt(key: "CategoryID")
            self.loadMyClosets(CategoryID: String(StoredData.shared.selectedCateogryId))
            self.self.categoryName = self.arrCategory[indexPath].getString(key: "CategoryName")
            self.selectedCategoryId = self.arrCategory[indexPath].getInt(key: "CategoryID")
        }

        self.navigationItem.titleView = menuView
 
        //coment above
        
        if !loadData
        {
            loadMyClosets(CategoryID: arrCategory[0].getString(key: "CategoryID"))
          //  loadMyClosets(CategoryID: 0)
        }
    }

    func loadMyClosets(CategoryID: String)
    {
        var param = [String:Any]()
        
        if CategoryID == "0" {
             param = [
                       "CategoryID": ""
                   ]
        }
        else
        {
            param = [
                       "CategoryID": CategoryID
                   ]
        }
        arrClosets.removeAll()
        callApi(Path.myVisionBoardList, method: .post, param: param) { (result) in

            print(result)
            if result.getBool(key: "success")
            {
                self.arrClosets = result.getArrayofDictionary(key: "data")
                self.vwNodata.isHidden = true
            }
            else
            {
                if self.test {
                    self.vwNodata.isHidden = true
                }
                else {
                    self.vwNodata.isHidden = false
                }
               //self.collectionView.showEmptyMessage(message: result.getString(key: "message"))
            }
            self.collectionView.reloadData()
        }
    }

 //MARK:- Photos Actions👊🏻😠
    
    @objc func AccButton(_ button: UIButton) {
//        print("Button with tag: \(button.tag) clicked!")
//        let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
//        self.navigationController?.pushViewController(vc!, animated: true)

        let imagePicker = UIImagePickerController()
        let actionSheet = UIAlertController(title: "Choose action", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in

            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                imagePicker.delegate = self
                imagePicker.isEditing = true
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
            else
            {
                self.showMessage(message: "Camera Not Available", completionHandler: nil)
            }
        }))
        actionSheet.addAction(UIAlertAction(title: "Photos", style: .default, handler: { (action) in

            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.delegate = self
            imagePicker.isEditing = true
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)

        }))
        
        actionSheet.addAction(UIAlertAction(title: "My Closet", style: .default, handler: { (action) in

            self.performSegue(withIdentifier: "chooseImage", sender: nil)
            
        }))

        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in

        }))

        present(actionSheet, animated: true, completion: nil)
    }
    
    //MARK:- Actions👊🏻😠
    @objc func NotificationAction(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        
        if button.tag == 1
        {
            let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    
    @IBAction func btnEditAction(_ sender: UIButton) {
        
        StoredData.shared.isEditClosetImage = true
        StoredData.shared.closetImage = imageView.image!
        
        tabBarController?.selectedIndex = 4
        let vc = storyboard?.instantiateViewController(withIdentifier: "VisionBoardViewController") as? VisionBoardViewController
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    @IBAction func btnShareAction(_ sender: UIButton) {
        let text = txtNewPost.text!
       // let shareLink = NSURL(string:selectedImgURL)
        self.showProgressHud(message: "")
        
        let url = URL(string: selectedImgURL)

        KingfisherManager.shared.retrieveImage(with: url!, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
            print(image!)
            self.hideProgressHud()
            
            let shareAll = [text , image!] as [Any]
            let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = sender
            self.present(activityViewController, animated: true, completion: nil)
            
        })
        
       
    }
    
    @IBAction func btnPostAction(_ sender: UIButton) {
        self.viewZoomImage.isHidden = true
        showViewWithAnimation(vw: vwPostNew, img: imgBlur)
    }
    
    @IBAction func btnDeletAction(_ sender: UIButton) {
        
        let param: [String: Any] = [
                       "VisionBoardID": AddVisionBoardID,
                       ]

                   callApi(Path.RemoveVisionBoard, method: .post, param: param) { (result) in

                       print(result)
                       if result.getBool(key: "success")
                       {
//                           self.txtCategory.text = ""
//                           self.selectedCategoryId = 0
                          self.showMessage(message: result.getString(key: "message"), completionHandler: {
                            self.viewZoomImage.isHidden = true
                            self.viewWillAppear(true)
                           })
                       
                       }
                       else
                       {
                           self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                       }
                   }
    }
    
    
    @IBAction func btnCancelPostAction(_ sender: UIButton) {
        hideViewWithAnimation(vw: vwPostNew, img: imgBlur)
    }
    
    @IBAction func btnNewPostAction(_ sender: UIButton) {
        if txtNewPost.text!.isEmpty()
        {
            showMessage(message: "Please mention about new post", completionHandler: nil)
        }
//        else if selectedCategoryId == 0
//        {
//            showMessage(message: "Please select category", completionHandler: nil)
//        }
        else
        {
            addPost()
        }
    }
    
    func addPost()
    {
         var param = [String:Any]()
        
        if selectedCategoryId == Int("0") {
                   param = [
                             "CategoryID": "",
                    "PostTitle": txtNewPost.text!.encodeEmoji,
                            "Description":""
                         ]
              }
              else
              {
                  param = [
                    "CategoryID": selectedCategoryId!,
                    "PostTitle": txtNewPost.text!.encodeEmoji,
                            "Description":""
                         ]
              }
        
//        param = [
//        "CategoryID": selectedCategoryId,
//            "PostTitle": txtNewPost.text!,
//            "Description":""
//        ]
        
        var imageData = [Data]()
        var dataKey = [String]()
        param["ImageType"] = 0
        imageData.append(UIImagePNGRepresentation(imageView.image!)!)
        dataKey.append("PostImage")
        
        callApi(Path.addNewPost, method: .post, param: param, data: imageData, dataKey: dataKey) { (result) in
            
            print(result)
            if result.getBool(key: "success")
            {
                self.txtNewPost.text = ""
                self.txtCategory.text = ""
                self.selectedCategoryId = 0
                self.imageView.image = nil
                self.showMessage(message: result.getString(key: "message"), completionHandler: {
                    self.someAction(UITapGestureRecognizer())
                })
                self.viewZoomImage.isHidden = true
                self.hideViewWithAnimation(vw: self.vwPostNew, img: self.imgBlur)
            }
            else
            {
                self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
            }
        }
        
    }
    
    @objc func someAction(_ sender:UITapGestureRecognizer){
        // do other task
        
        let home = TabBarViewController()
        home.setUI()
        self.navigationController?.pushViewController(home, animated: true)
    }
    
    // MARK: - ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        test = true
        let image = info[UIImagePickerControllerEditedImage] as! UIImage
        let fixedImaage = UIImage(cgImage: image.cgImage!, scale: image.scale, orientation: image.imageOrientation)

        imageViewPopup.image = fixedImaage
      
       
        viewSavePopup.isHidden = false
        vwAddImage.isHidden = false
        vwNodata.isHidden = true
         picker.dismiss(animated: true, completion: nil)
    }

//MARK: - Methods(Functions)☝🏻😳
    
//    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
//        if case .right = position {
//
//            revealController.frontViewController.view.alpha = 0.5
//            self.view.isUserInteractionEnabled = false
//            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
//            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
//
//        } else {
//
//            revealController.frontViewController.view.alpha = 1
//            self.view.isUserInteractionEnabled = true
//        }
//    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrClosets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        // get a reference to our storyboard cell
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: "item", for: indexPath as IndexPath) as! ClosetImagesCollectionView
        
        let flow = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        
        let itemSpacing: CGFloat = 3
        let itemsInOneLine: CGFloat = 3
        flow.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        let width = UIScreen.main.bounds.size.width - itemSpacing * CGFloat(itemsInOneLine - 1)
        flow.itemSize = CGSize(width: floor(width/itemsInOneLine), height: width/itemsInOneLine)
        flow.minimumInteritemSpacing = 3
        flow.minimumLineSpacing = 3

        let record = arrClosets[indexPath.row]

        item.ClosetImg.setImage(record.getString(key: "PostImage"))
        
        return item
    }
    
    //BlogId ImageType
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let record = arrClosets[indexPath.row]
                          selectedImgURL = record.getString(key: "PostImage")
        if isfrompostnew {
                   
                   delegate?.visionBoardSelectedUrl(url: arrClosets[indexPath.row].getString(key: "PostImage"), id: arrClosets[indexPath.row].getInt(key: "CategoryID"))
                   
                   navigationController?.popViewController(animated: true)
        }
        else
        {
            viewZoomImage.isHidden = false
                   let cell = collectionView.cellForItem(at: indexPath) as! ClosetImagesCollectionView
                   imageView.image = cell.ClosetImg.image

                   let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureHandeler(_:)))
                   viewZoomImage.addGestureRecognizer(tapGesture)
                   selectedCategoryId = record.getInt(key: "CategoryID")
                   AddVisionBoardID = record.getInt(key: "AddVisionBoardID")
        }
       
        
       
    }

    @objc func tapGestureHandeler(_ gesture: UITapGestureRecognizer)
    {
        viewZoomImage.isHidden = true
    }
    
    //collectionViewLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = UIScreen.main.bounds.width
        return CGSize(width: (width - 10)/3, height: (width - 10)/3) // width & height are the same to make a square cell
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrCategoryItem.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrCategoryItem[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        txtCategory.text = arrCategoryItem[row]
        selectedCategoryId = arrCategory[row].getInt(key: "CategoryID")
    }

    @IBAction func btnHideAction(_ sender: UIButton) {

        viewSavePopup.isHidden = true
    }
    
    @IBAction func btnMoveToClosetAction(_ sender: UIButton) {

        if txtCategory.text!.isEmpty()
        {
            showMessage(message: "Please select cetegory.", completionHandler: nil)
        }
            
        else
        {
            let param: [String: Any] = [
                "CategoryID": selectedCategoryId!,
                 "Type" : "IMAGE"
                ]

            callApi(Path.MoveToVisionBoard, method: .post, param: param, data: [UIImagePNGRepresentation((imageViewPopup.image?.resize(scale: 0.7))!)!], dataKey: ["PostImage"]) { (result) in

                print(result)
                if result.getBool(key: "success")
                {
                    self.txtCategory.text = ""
                    self.selectedCategoryId = 0
                    self.showMessage(message: result.getString(key: "message"), completionHandler: {
                        self.viewSavePopup.isHidden = true
                        self.loadMyClosets(CategoryID: "")
                    })
                }
                else
                {
                    self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                }
            }
        }
    }

    func selectedUrl(url: String, id: Int) {
        imageViewPopup.setImage(url)
        viewSavePopup.isHidden = false
        closetImageId = id
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "chooseImage"
        {
            let destination = segue.destination as! ChooseFromClosetViewController
            destination.delegate = self
        }
    }
}

extension UIViewController
{
    func showViewWithAnimation(vw : UIView , img : UIImageView) {
        vw.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.3) {
            vw.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            vw.isHidden = false
            img.isHidden = false
        }
    }
    
    func hideViewWithAnimation(vw : UIView , img : UIImageView) {
        UIView.animate(withDuration: 0.3, animations: {
            vw.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        }, completion: {
            (value: Bool) in
            vw.isHidden = true
            img.isHidden = true
        })
    }
}

extension UIView
{
    func setRadius(radius: CGFloat) {
        self.layoutIfNeeded()
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func setRadiusBorder(color: UIColor) {
        self.layoutIfNeeded()
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = 2
        self.layer.masksToBounds = true
    }
    
    func setDarkRadiusBorder(color: UIColor , weight : CGFloat) {
        self.layoutIfNeeded()
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = weight
        self.layer.masksToBounds = true
    }
}
