//
//  MyFeedViewController.swift
//  PlusMe
//
//  Created by rohit on 20/08/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit
import SWRevealViewController
import SDWebImage
import DropDown

class SSBadgeButton: UIButton {
    
    var badgeLabel = UILabel()
    
    var badge: String? {
        didSet {
            addBadgeToButon(badge: badge)
        }
    }

    public var badgeBackgroundColor = UIColor.red {
        didSet {
            badgeLabel.backgroundColor = badgeBackgroundColor
        }
    }
    
    public var badgeTextColor = UIColor.white {
        didSet {
            badgeLabel.textColor = badgeTextColor
        }
    }
    
    public var badgeFont = UIFont.systemFont(ofSize: 12.0) {
        didSet {
            badgeLabel.font = badgeFont
        }
    }
    
    public var badgeEdgeInsets: UIEdgeInsets? {
        didSet {
            addBadgeToButon(badge: badge)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addBadgeToButon(badge: nil)
    }
    
    func addBadgeToButon(badge: String?) {
        badgeLabel.text = badge
        badgeLabel.textColor = badgeTextColor
        badgeLabel.backgroundColor = badgeBackgroundColor
        badgeLabel.font = badgeFont
        badgeLabel.sizeToFit()
        badgeLabel.textAlignment = .center
        let badgeSize = badgeLabel.frame.size
        
        let height = max(18, Double(badgeSize.height) + 5.0)
        let width = max(height, Double(badgeSize.width) + 10.0)
        
        var vertical: Double?, horizontal: Double?
        if let badgeInset = self.badgeEdgeInsets {
            vertical = Double(badgeInset.top) - Double(badgeInset.bottom)
            horizontal = Double(badgeInset.left) - Double(badgeInset.right)
            
            let x = (Double(bounds.size.width) - 10 + horizontal!)
            let y = -(Double(badgeSize.height) / 2) - 10 + vertical!
            badgeLabel.frame = CGRect(x: x, y: y+5, width: width, height: height)
        } else {
            let x = self.frame.width - CGFloat((width / 2.0))
            let y = CGFloat(-(height / 2.0))
            badgeLabel.frame = CGRect(x: x, y: y+5, width: CGFloat(width), height: CGFloat(height))
        }
        
        badgeLabel.layer.cornerRadius = badgeLabel.frame.height/2
        badgeLabel.layer.masksToBounds = true
        addSubview(badgeLabel)
        badgeLabel.isHidden = badge != nil ? false : true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.addBadgeToButon(badge: nil)
        fatalError("init(coder:) has not been implemented")
    }
}

class MyFeedViewController: UIViewController, SWRevealViewControllerDelegate, UITableViewDelegate,UITableViewDataSource, UISearchBarDelegate  {

    @IBOutlet weak var tableView: UITableView!
    var menuView: BTNavigationDropdownMenu!
    var arrBlogs = [[String: Any]]()

    @IBOutlet weak var vwFirstAddPostView: UIView!
    
    @IBOutlet weak var lblPost: UILabel!
    @IBOutlet weak var imgAddOutlet: UIImageView!
    @IBOutlet weak var btnAddPostOutlet: UIButton!
    @IBOutlet weak var btnCreatePostOutlet: UIButton!
    
    var categoryName = ""
    var CateogryId = Int()
    var arrCategoryName = [String]()
    
    var arrResult = [[String: Any]]()
    
     let ReportDropDown = DropDown()
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
         
        
      
    
        //SideBar
      

        tableView.estimatedRowHeight = 1000
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
//        let items = ["TRENDY", "FORMAL", "BUSSINESS", "CASUAL"]
//        //        self.selectedCellLabel.text = items.first
//        self.navigationController?.navigationBar.isTranslucent = false
//        self.navigationController?.navigationBar.barTintColor = .white
//        //            UIColor(red: 0.0/255.0, green:180/255.0, blue:220/255.0, alpha: 1.0)
//        self.navigationController?.navigationBar.titleTextAttributes = [kCTForegroundColorAttributeName: UIColor.white] as [NSAttributedStringKey : Any]
//
//        // "Old" version
//        // menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: "Dropdown Menu", items: items)
//
//        menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: BTTitle.index(2), items: items)
//
//        // Another way to initialize:
//        // menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: BTTitle.title("Dropdown Menu"), items: items)
//
//        menuView.cellHeight = 50
//        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
//        menuView.cellSelectionColor = UIColor.lightGray//(red: 0.0/255.0, green:160.0/255.0, blue:195.0/255.0, alpha: 1.0)
//        menuView.shouldKeepSelectedCellColor = true
//        menuView.cellTextLabelColor = UIColor.black
//        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
//        menuView.cellTextLabelAlignment = .left // .Center // .Right // .Left
//        menuView.arrowPadding = 15
//        menuView.animationDuration = 0.5
//        menuView.maskBackgroundColor = UIColor.black
//        menuView.maskBackgroundOpacity = 0.3
//        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> Void in
//            print("Did select item at index: \(indexPath)")
//            //            self.selectedCellLabel.text = items[indexPath]
//        }
//
//        self.navigationItem.titleView = menuView
    }
    
    func getAspectRatioAccordingToiPhones(cellImageFrame:CGSize,downloadedImage: UIImage)->CGFloat {
        let widthOffset = downloadedImage.size.width - cellImageFrame.width
        let widthOffsetPercentage = (widthOffset*100)/downloadedImage.size.width
        let heightOffset = (widthOffsetPercentage * downloadedImage.size.height)/100
        let effectiveHeight = downloadedImage.size.height - heightOffset
        return(effectiveHeight)
    }
    
    // MARK: Optional function for resize of image
    func resizeHighImage(image:UIImage)->UIImage {
        let size = image.size.applying(CGAffineTransform(scaleX: 0.5, y: 0.5))
        let hasAlpha = false
        let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        image.draw(in: CGRect(origin: .zero, size: size))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImage!
        
    }
    
    
    @objc func AccButton1(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }

    @objc func HomeButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "CategoriesViewController") as? CategoriesViewController
        vc?.tabBarController?.hidesBottomBarWhenPushed = false
        tabBarController?.selectedIndex = 2
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @objc func RefreshButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        
        getBlogs()
        tableView.setContentOffset(.zero, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "VisionBoardViewController") as! VisionBoardViewController
        nav.resetview()
        
        getBlogs()
        //NavigationBar
          
              self.navigationController?.navigationBar.isHidden = false;
              
              navigationController?.navigationBar.barTintColor = UIColor.white
              navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
              self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }
       
              
              let menuButton = UIButton(type: .system)
              menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
              menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
              navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
              
              let AccButton1 = UIButton(type: .system)
              AccButton1.setImage(#imageLiteral(resourceName: "Search").withRenderingMode(.alwaysOriginal), for: .normal)
              AccButton1.tag = 1
              AccButton1.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
              AccButton1.addTarget(self, action: #selector(AccButton(_:)), for: .touchUpInside)
              let barButtonItem = UIBarButtonItem(customView: AccButton1)
              
             // let AccButton = UIButton(type: .system)
               let AccButton = SSBadgeButton()
              if UserDefaults.standard.object(forKey: "notification") != nil {
                  let noticount = UserDefaults.standard.string(forKey: "notification")
                  AccButton.badge = noticount
              }
              else
              {
                   AccButton.badge = nil
              }
              
              AccButton.setImage(#imageLiteral(resourceName: "Notification").withRenderingMode(.alwaysOriginal), for: .normal)
              AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
              AccButton.addTarget(self, action: #selector(AccButton1(_:)), for: .touchUpInside)
             
              let barButtonItem1 = UIBarButtonItem(customView: AccButton)
              
              let HomeButton = UIButton(type: .system)
              HomeButton.setImage(#imageLiteral(resourceName: "home").withRenderingMode(.alwaysOriginal), for: .normal)
              HomeButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
              HomeButton.addTarget(self, action: #selector(HomeButton(_:)), for: .touchUpInside)
              let barHomeButtonItem = UIBarButtonItem(customView: HomeButton)
        
        let RefreshButton = UIButton(type: .system)
                     RefreshButton.setImage( #imageLiteral(resourceName: "refresh-icon").withRenderingMode(.alwaysTemplate), for: .normal)
                     RefreshButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        RefreshButton.tintColor = UIColor.black
                     RefreshButton.addTarget(self, action: #selector(RefreshButton(_:)), for: .touchUpInside)
                     let barHomeButtonItemrefresh = UIBarButtonItem(customView: RefreshButton)
              
          
             // self.navigationItem.rightBarButtonItems = [barButtonItem1, barButtonItem]
              
              if categoryName != "" {
                   self.title = categoryName
                   self.navigationItem.rightBarButtonItems = [barButtonItem1, barHomeButtonItem, barButtonItem]
              }
                  
              else {
                   self.title = "Home"
                   self.navigationItem.rightBarButtonItems = [barButtonItem1, barButtonItem,barHomeButtonItemrefresh]
              }
        if revealViewController() != nil
              {
                  self.revealViewController().delegate = self
                  menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
                  self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                  view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
              }
        
    //    weak var weakSelf: MyFeedViewController? = self
//         setup pull-to-refresh
//        tableView.addPullToRefresh(actionHandler: {() -> Void in
//            weakSelf?.insertRowAtTop()
//
//        })
        
//        tableView.addInfiniteScrolling(actionHandler: {() -> Void in
//            weakSelf?.insertRowAtTop()
//        })
      
    }
    
    func insertRowAtTop()
    {
        getBlogs()
    }
    
    //MARK:- API
    func getBlogs()
    {
        if arrBlogs.count > 0
        {
            arrBlogs.removeAll()
            arrResult.removeAll()
        }
  //      let param = [:] as [String:Any]
        
        let param = [
            "CategoryID": CateogryId
        ]
        
        callApi(Path.getMyFeed, method: .get, param: param) { (result) in

            print(result)
            if result.getBool(key: "success")
            {
              //  self.tableView.pullToRefreshView.stopAnimating()
                //self.arrBlogs = result.getArrayofDictionary(key: "data")
                let tempResult = result.getArrayofDictionary(key: "data")
               
//                for i in tempResult {
//                  let name = i.getString(key: "PostedByName")
//                    if StoredData.shared.name != name
//                    {
//                       self.arrResult.append(i)
//                       self.arrBlogs.append(i)
//                    }
//                }
                
                 self.arrResult = tempResult
                self.arrBlogs = tempResult
            }
               
            else
            {
                self.navigationItem.titleView = nil
                self.title = "My feeds"
              //  self.tableView.pullToRefreshView.stopAnimating()
//                self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                //self.tableView.showEmptyMessage(message: result.getString(key: "message"))
                
                
                self.vwFirstAddPostView.isHidden = false
                
//                self.btnAddPostOutlet.imageView?.changeImageViewImageColor(color: UIColor.init(red: 68/255, green: 198/255, blue: 82/255, alpha: 1))
                self.btnAddPostOutlet.roundView(UIColor.init(red: 68/255, green: 198/255, blue: 82/255, alpha: 1), borderWidth: 2)
                self.btnAddPostOutlet.setTitleColor(UIColor.init(red: 68/255, green: 198/255, blue: 82/255, alpha: 1), for: .normal)
                self.btnCreatePostOutlet.roundCorner1()
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    // your code here
                    self.tableView.reloadData()
                }
                
            }
            
        }
    }
    
    //MARK: - Methods(Functions)☝🏻😳

    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {

            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            revealController.frontViewController.view.alpha = 1
            self.view.isUserInteractionEnabled = true
        }
    }
    
    //MARK:- TABLE VIEW METHODS
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 1000
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrResult.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MenuTableViewCell
        if arrResult.count > 0 {
            
        }
        else
        {
            return cell
        }
        
        let record = arrResult[indexPath.row]
        
        cell.Blog_list_VW.layer.borderWidth = 1.0
        cell.Blog_list_VW.layer.borderColor = UIColor.lightGray.cgColor
        cell.Blog_list_VW.layer.cornerRadius = 5.0

        cell.Btn_Closet.tag = indexPath.row
        cell.Btn_Closet.addTarget(self, action: #selector(buttonClicked(sender:)), for: UIControlEvents.touchUpInside)
        cell.Btn_Closet.isSelected = record.getBool(key: "IsCloset")

        cell.btnLike.tag = indexPath.row
        cell.btnLike.addTarget(self, action: #selector(btnLikeClicked(sender:)), for: UIControlEvents.touchUpInside)
        cell.btnLike.isSelected = record.getBool(key: "isLike")
        
        cell.btnreport.tag = indexPath.row
        cell.btnreport.addTarget(self, action: #selector(btnReportClicked(sender:)), for: UIControlEvents.touchUpInside)
        
        cell.Blog_list_img.layer.cornerRadius = cell.Blog_list_img.frame.height/2
        cell.Blog_list_img.clipsToBounds = true
        cell.Blog_list_img.backgroundColor = .lightGray
        cell.Blog_list_img.setImage(record.getString(key: "ProfileImage"))
        addGesture(cell.Blog_list_img, userId: record.getInt(key: "PostedByID"))
        addGesture(cell.Blog_list_lbl_Title, userId: record.getInt(key: "PostedByID"))

        //let image = UIImage(named:"IconPhone")
    
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(gesterHandeler(_:)))
        cell.Blog_list_lbl_Banner.isUserInteractionEnabled = true
        cell.Blog_list_lbl_Banner.tag = indexPath.row
        cell.Blog_list_lbl_Banner.addGestureRecognizer(tapGesture)
        
       // cell.Blog_list_lbl_Banner.setImage(record.getString(key: "PostImage"))
        
        var cellFrame = cell.frame.size
        cellFrame.height =  cellFrame.height - 15
        cellFrame.width =  cellFrame.width - 15
        // let url = URL(string: arrResult[indexPath.row].imageModel[0].standardResolutionUrl!)
        
        let url = URL(string: arrResult[indexPath.row]["PostImage"] as? String ?? "")
             //let url = URL(string: record.getString(key: "PostImage"))
        
           
                
                 DispatchQueue.main.async {
                    
                     cell.Blog_list_lbl_Banner.sd_setImage(with: url, placeholderImage: nil, options: .progressiveLoad, completed: { (theImage, error, cache, url) in
                   
              //   cell.feedImageHeightConstraint.constant = self.getAspectRatioAccordingToiPhones(cellImageFrame: cellFrame,downloadedImage: theImage!)
                    cell.setNeedsLayout() //invalidate current layout
                    cell.layoutIfNeeded()
                    cell.Blog_list_lbl_Banner.setNeedsLayout()
                    cell.Blog_list_lbl_Banner.layoutIfNeeded()
                     
 })
                   }
              
        
       DispatchQueue.main.async {
             
        //   cell.feedImageHeightConstraint.constant = self.getAspectRatioAccordingToiPhones(cellImageFrame: cellFrame,downloadedImage: theImage!)
              cell.setNeedsLayout() //invalidate current layout
              cell.layoutIfNeeded()
              cell.Blog_list_lbl_Banner.setNeedsLayout()
              cell.Blog_list_lbl_Banner.layoutIfNeeded()

             }


        
        cell.Blog_list_lbl_Title.text = record.getString(key: "PostedByName")
        cell.Blog_list_lbl_Owner.text = record.getString(key: "RegisterUserTypeByName")
        cell.Blog_list_lbl_Time.text = record.getString(key: "PostTime").toDate().timeAgoSinceDate()
        cell.Blog_list_lbl_Detail.text = record.getString(key: "PostTitle").decodeEmoji
        cell.lblComments.text = "\(record.getString(key: "TotalComment"))"
         cell.lbltotallike.text = "\(record.getString(key: "TotalLike"))"
       
         return cell
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MenuTableViewCell
//
//        cell.Blog_list_VW.layer.borderWidth = 1.0
//        cell.Blog_list_VW.layer.borderColor = UIColor.lightGray.cgColor
//        cell.Blog_list_VW.layer.cornerRadius = 5.0
//
//        cell.Btn_Closet.tag = indexPath.row
//        cell.Btn_Closet.addTarget(self, action: #selector(buttonClicked(sender:)), for: UIControlEvents.touchUpInside)
//
//        cell.Blog_list_img.layer.cornerRadius = cell.Blog_list_img.frame.height/2
//        cell.Blog_list_img.clipsToBounds = true
//        cell.Blog_list_img.image = #imageLiteral(resourceName: "IconFacebook")
//
//
//        cell.Blog_list_lbl_Banner.image = #imageLiteral(resourceName: "Model")
//
//        cell.Blog_list_lbl_Title.text = "Fashion Hub"
//        cell.Blog_list_lbl_Owner.text = "Adam Clark"
//        cell.Blog_list_lbl_Time.text = "1 hour ago"
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "BlogDetailsVC") as! BlogDetailsVC
        nav.blogId = arrResult[indexPath.row].getString(key: "BlogId")
        navigationController?.pushViewController(nav, animated: true)
        
    }

    //MARK: - Actions👊🏻😠
    
    @objc func btnReportClicked(sender:UIButton) {
        
        ReportDropDown.anchorView = sender
              ReportDropDown.dataSource = ["Report An Issue"]
        ReportDropDown.selectionAction = { [weak self] (index, item) in
            print(sender.tag)
            self?.showMessage(message: "Don't show me this post again", completionHandler: {
                let nav = self?.storyboard?.instantiateViewController(withIdentifier: "ReportIssueViewController") as! ReportIssueViewController
                nav.dicdata = self?.arrResult[sender.tag] ?? [:]
                self?.navigationController?.pushViewController(nav, animated: true)
          })
           
                      }
        ReportDropDown.show()
    }
    
    @objc func btnLikeClicked(sender:UIButton) {
        let param = [
            "PostId": arrResult[sender.tag].getString(key: "BlogId"),
            "isLike": sender.isSelected ? "0" : "1"
        ]
        callApi(Path.likePost, method: .post, param: param) { (result) in
            print(result)
            if result.getBool(key: "success")
            {
                sender.isSelected = !sender.isSelected
            }
            self.getBlogs()
          
        }
    }
    @IBAction func refresh_click(_ sender: UIButton) {
        getBlogs()
        tableView.setContentOffset(.zero, animated: true)
    }
    
    @IBAction func BtnFirstPostAction(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PostNewViewController") as? PostNewViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func btnAddAction(_ sender: UIButton) {
        
    }
    
    @objc func buttonClicked(sender:UIButton) {
        
//        let param = [
//            "BlogId": arrResult[sender.tag].getString(key: "BlogId")
//        ]
//        callApi(Path.addToCloset, method: .post, param: param) { (result) in
//            print(result)
//            if result.getBool(key: "success")
//            {
//                sender.isSelected = !sender.isSelected
//            }
//        }
        
        if !sender.isSelected
        {
            let indexPath = IndexPath(row: sender.tag, section: 0)
            let cell : MenuTableViewCell = (tableView.cellForRow(at: indexPath) as? MenuTableViewCell)!
            
            let param: [String: Any] = [
                "BaseImageID": 0,
                "BlogId" : arrResult[sender.tag].getString(key: "BlogId")
                ]
            
            self.callApi(Path.saveToCloset, method: .post, param: param, data: [UIImagePNGRepresentation((cell.Blog_list_lbl_Banner.image?.resize(scale: 1))!)!], dataKey: ["PostImage"]) { (result) in
                
                print(result)
                if result.getBool(key: "success")
                {
                    sender.isSelected = !sender.isSelected
                    self.showMessage(message: result.getString(key: "message"), completionHandler: {
                    })
                }
                    
                else
                {
                    self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                }
            }
        }
            
        else
        {
            self.showMessage(message: "Already added to Closet", completionHandler: nil)
        }
    }

    @objc func AccButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")

        if button.tag == 1
        {
            navigationItem.rightBarButtonItems = nil

            let searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 200, height: 20))
            searchBar.placeholder = "Search"
            searchBar.delegate = self
            navigationItem.titleView = searchBar
            title = nil

            let menuButton = UIButton(type: .system)
            menuButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
            menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
            navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
            menuButton.addTarget(self, action: #selector(searchAction(_:)), for: .touchUpInside)
        }
    }

    @objc func searchAction(_ button: UIButton) {
        navigationItem.titleView = nil
        title = "Home"
        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)

        //SideBar
        if revealViewController() != nil
        {
            self.revealViewController().delegate = self
            menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

        let AccButton1 = UIButton(type: .system)
        AccButton1.setImage(#imageLiteral(resourceName: "Search").withRenderingMode(.alwaysOriginal), for: .normal)
        AccButton1.tag = 1
        AccButton1.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        AccButton1.addTarget(self, action: #selector(AccButton(_:)), for: .touchUpInside)
        let barButtonItem = UIBarButtonItem(customView: AccButton1)


        let AccButton = UIButton(type: .system)
        AccButton.setImage(#imageLiteral(resourceName: "Notification").withRenderingMode(.alwaysOriginal), for: .normal)
        AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        AccButton.addTarget(self, action: #selector(AccButton1(_:)), for: .touchUpInside)
        let barButtonItem1 = UIBarButtonItem(customView: AccButton)
        
        let RefreshButton = UIButton(type: .system)
                          RefreshButton.setImage( #imageLiteral(resourceName: "refresh-icon").withRenderingMode(.alwaysTemplate), for: .normal)
         RefreshButton.tintColor = UIColor.black
                          RefreshButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
                          RefreshButton.addTarget(self, action: #selector(RefreshButton(_:)), for: .touchUpInside)
                          let barHomeButtonItemrefresh = UIBarButtonItem(customView: RefreshButton)

        self.navigationItem.rightBarButtonItems = [barButtonItem1, barButtonItem,barHomeButtonItemrefresh]

        arrResult = arrBlogs
        
         DispatchQueue.main.async {
            self.tableView.reloadData()
         }
       
    }

    @objc func AccButton2(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
    }

    
    //MARK: - Searchbar Delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

         arrResult = arrBlogs.filter { (dict) -> Bool in

            if dict.getString(key: "PostedByName").lowercased().contains(searchText.lowercased())
            {
                return true
            }
            return false
        }
        
        print(arrResult)
        
        if searchText == ""
        {
            arrResult = arrBlogs
        }
        
        tableView.reloadData()
    }

    @IBOutlet weak var viewZoomPopup: UIView!
    @IBOutlet weak var imgZoomImage: UIImageView!

    @IBAction func btnHideZoomPopupAction(_ sender: UIButton) {
        viewZoomPopup.isHidden = true
    }
    
    @objc func gesterHandeler(_ sender: UITapGestureRecognizer)
    {
        let row = sender.view!.tag
        imgZoomImage.setImage(arrResult[row].getString(key: "PostImage"))
        viewZoomPopup.isHidden = false
    }
}


class ScaledHeightImageView: UIImageView {

    override var intrinsicContentSize: CGSize {

        if let myImage = self.image {
            let myImageWidth = myImage.size.width
            let myImageHeight = myImage.size.height
            let myViewWidth = self.frame.size.width

            let ratio = myViewWidth/myImageWidth
            let scaledHeight = myImageHeight * ratio

            return CGSize(width: myViewWidth, height: scaledHeight)
        }

        return CGSize(width: -1.0, height: -1.0)
    }

}
