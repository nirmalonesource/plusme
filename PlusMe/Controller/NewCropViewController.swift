//
//  NewCropViewController.swift
//  PlusMe
//
//  Created by Emp-Mac-1 on 03/12/20.
//  Copyright © 2020 Priyank Jotangiya. All rights reserved.
//

import UIKit
import CropPickerView

class NewCropViewController: UIViewController {
    @IBOutlet weak var cropvw: CropPickerView!
    
    var shapeLayer1 = CAShapeLayer()
    var GetCropImage = UIImageView()
    var isCrop = false
    var crImage = UIImage()
    var croppedImage = UIImage()
    var delegate: CropImageDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        

//        let cropPickerView = CropPickerView()
//        self.view.addSubview(cropPickerView)
        cropvw.scrollMinimumZoomScale = 1
        cropvw.scrollMaximumZoomScale = 2
        cropvw.cropMinSize = 10
        StoredData.shared.closetImage = UIImage()
        //btnCrop.setTitleColor(UIColor.gray, for: .disabled)
        if isCrop {
            cropvw.image = crImage
           // cropvw.image(crImage, isMin: true)
        }
        else {
            cropvw.image = GetCropImage.image
        }
    }
    

    @IBAction func crop_click(_ sender: UIButton) {
        
        self.cropvw.crop { (crop) in
            if let error = (crop.error as NSError?) {
                let alertController = UIAlertController(title: "Error", message: error.domain, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
                return
            }
//            self.imageView.image = crop.image
//            self.descriptionLabel.text = ""
            if let value = crop.cropFrame {
                print("\n\n-crop frame\n\(value)")
            }
            if let value = crop.realCropFrame {
                print("\n\n-real crop frame\n\(value)")
            }
            if let value = crop.imageSize {
                print("\n\n-before image size\n\(value)")
            }
            print(self.cropvw.image ?? UIImage().resize(targetSize: crop.realCropFrame!.size))
            let cropimg = crop.image
            let img =  cropimg!.resize(targetSize: crop.realCropFrame!.size)
                  self.delegate?.cropImage(cropImage:img, ShapLayer: self.shapeLayer1)
               self.navigationController?.popViewController(animated: true)
        }
        
       
    }
    @IBAction func delet_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
extension UIImage {

    func resize(targetSize: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size:targetSize).image { _ in
            self.draw(in: CGRect(origin: .zero, size: targetSize))
        }
    }

}
