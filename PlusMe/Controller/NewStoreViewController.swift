//
//  NewStoreViewController.swift
//  PlusMe
//
//  Created by rohit on 23/10/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit
import MapKit

class NewStoreViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate , UITextFieldDelegate {

    @IBOutlet weak var txtCompanyName: UITextField!
    @IBOutlet weak var txtWebURL: UITextField!
    @IBOutlet weak var txtPhoneNo: UITextField!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var txtAbout: UITextField!
    @IBOutlet weak var imgCompany: UIImageView!
    @IBOutlet weak var btnSendRequest: UIButton!
    @IBOutlet weak var txtemail: UITextField!
    var getAnnotation = MKPointAnnotation()
    var selectedPin:MKPlacemark? = nil
    var isSelectedImage = false
    var address = ""
    var latitude = ""
    var logitude = ""
    
    //MARK:- VIEW LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Add New Store"
        self.navigationController?.navigationBar.isHidden = false;

        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }

        let btnBack = UIButton(type: .system)
        btnBack.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
        btnBack.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        btnBack.addTarget(self, action: #selector(btnBackAction(_:)), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btnBack)

        txtCompanyName.ChangePlaceholderColor(placeholder: "Company name")
        txtWebURL.ChangePlaceholderColor(placeholder: "Website URL")
        txtemail.ChangePlaceholderColor(placeholder: "Email")
        txtPhoneNo.ChangePlaceholderColor(placeholder: "Phone No.")
        txtLocation.ChangePlaceholderColor(placeholder: "Location / address")
        btnSendRequest.roundCorner()
         txtPhoneNo.delegate = self
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")

        if (isBackSpace == -92) {
            return true
        }

        if txtPhoneNo == textField && txtPhoneNo.text!.count >= 10
        {
            return false
        }
        return true
    }

    @objc func btnBackAction(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        navigationController?.popViewController(animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
        
        if selectedPin != nil {
           // getAnnotation.coordinate = (selectedPin?.coordinate)!
            
            latitude = "\(getAnnotation.coordinate.latitude)"
            logitude = "\(getAnnotation.coordinate.longitude)"
        
            // longitude.text = "\(coord.longitude)"
            // let name = selectedPin?.name
            //txtLocation.text = name
            //  print(selectedPin?.addressDictionary!)
            
            //txtLocation.text = selectedPin?.addressDictionary
            
//            if let city = selectedPin?.locality,
//                let state = selectedPin!.administrativeArea {
//                getAnnotation.subtitle = "\(city) \(state)"
//                print("\(city) \(state)")
//                // txtLocation.text = name! + city + state
//                txtLocation.isUserInteractionEnabled = false
//            }
            
            if let locationName = selectedPin?.addressDictionary!["Name"] as? String {
                address += locationName + ", "
            }
            
            // Street address
            if let street = selectedPin?.addressDictionary!["Thoroughfare"] as? String {
                address += street + ", "
                
            }
            
            // City
            if let city = selectedPin?.addressDictionary!["City"] as? String {
                address += city + ", "
               
            }
            
            // Zip code
            if let zip = selectedPin?.addressDictionary!["ZIP"] as? String {
                address += zip + ", "
               
            }
            
            // Country
            if let country = selectedPin?.addressDictionary!["Country"] as? String {
                address += country + ", "
              
              }
            
            txtLocation.text = address
            
            //print("address: \(address)")
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
    }

    func validateData() -> Bool
    {
        if txtCompanyName.text!.isEmpty()
        {
            showMessage(message: "Please enter company name", completionHandler: nil)
            return false
        }
        else if !isSelectedImage
        {
            showMessage(message: "Please select company logo.", completionHandler: nil)
            return false
        }
        else if txtWebURL.text!.isEmpty()
        {
            showMessage(message: "Please enter website URL.", completionHandler: nil)
            return false
        }
        else if !isValidEmail(txtemail.text ?? "")
        {
            showMessage(message: "Please enter email id.", completionHandler: nil)
            return false
        }
        else if txtPhoneNo.text!.isEmpty()
        {
            showMessage(message: "Please enter phone no.", completionHandler: nil)
            return false
        }
        else if txtLocation.text!.isEmpty()
        {
            showMessage(message: "Please enter location.", completionHandler: nil)
            return false
        }
        else if txtAbout.text!.isEmpty()
        {
            showMessage(message: "Please mention about new company.", completionHandler: nil)
            return false
        }
        return true
    }

    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    @IBAction func btnCancelAction(_ sender: UIButton) {

        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSendRequestAction(_ sender: UIButton) {

        if validateData()
        {
            let phoneno = "+1" + txtPhoneNo.text!
            let param = [
                "CompanyName": txtCompanyName.text!,
                "WesiteUrl": txtWebURL.text!,
                "PhoneNo": phoneno,
                "LocationAdress": txtLocation.text!,
                "AboutCompany": txtAbout.text! ,
                "StoreLatitude" : latitude ,
                "StoreLongiture" : logitude,
                "Email" : txtemail.text!
            ]
            
            callApi(Path.addStore, method: .post, param: param, data: [UIImagePNGRepresentation(imgCompany.image!.resize(scale: 0.25))!], dataKey: ["CompanyLogo"], completionHandler: { (result) in

                print(result)
                if result.getBool(key: "success")
                {
                    self.showMessage(message: result.getString(key: "message"), completionHandler: {
                        self.txtCompanyName.text = ""
                        self.txtWebURL.text = ""
                        self.txtPhoneNo.text = ""
                        self.txtLocation.text = ""
                        self.txtAbout.text = ""
                        self.imgCompany.image = nil
                        self.btnBackAction(UIButton())
                    })
                }
                    
                else
                {
                    self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                }
            })
        }
    }
    
    @IBAction func btnimagePickAction(_ sender: UIButton) {

        let imagePicker = UIImagePickerController()

        let actionSheet = UIAlertController(title: "Take image", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in

            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                imagePicker.delegate = self
                imagePicker.isEditing = true
                imagePicker.allowsEditing = true
                imagePicker.accessibilityFrame = CGRect(x: 0, y: 0, width: 40, height: 50)
                self.present(imagePicker, animated: true, completion: nil)
            }
                
            else
            {
                self.showMessage(message: "Camera Not Available", completionHandler: nil)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photos", style: .default, handler: { (action) in

            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.delegate = self
            imagePicker.isEditing = true
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)

        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))

        present(actionSheet, animated: true, completion: nil)
    }

    // MARK: - ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        let image = info[UIImagePickerControllerEditedImage] as! UIImage
        let fixedImaage = UIImage(cgImage: image.cgImage!, scale: image.scale, orientation: image.imageOrientation)

        imgCompany.image = fixedImaage.resize(scale: 0.25)
        isSelectedImage = true
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- TEXTFIELD METHODS
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtLocation {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "StoreMapView") as! StoreMapView
            self.navigationController?.pushViewController(vc, animated: true)
            self.view.endEditing(true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
