//
//  NotificationViewController.swift
//  PlusMe
//
//  Created by rohit on 09/08/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit
import SWRevealViewController

struct NotificationClass {
    var Message: String?
    var CreatedDate: String?
    var UserId: Int?
    var TypeID: Int?

    init(Message: String, CreatedDate: String, UserId: Int, TypeID: Int) {
        self.Message = Message
        self.CreatedDate = CreatedDate
        self.UserId = UserId
        self.TypeID = TypeID
    }
}

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
}

class NotificationViewController: UIViewController, SWRevealViewControllerDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var arrNotification = [[String: Any]]()
    
    var data = [NotificationClass]()
    
    /*
     if arrNotification[indexPath.row].getInt(key: "Type") != 7 {
     cell.lblMessage.text = arrNotification[indexPath.row].getString(key: "Message")
     cell.lblDate.text = arrNotification[indexPath.row].getString(key: "CreatedDate").toDate().timeAgoSinceDate2()
     }
 */
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
         UserDefaults.standard.removeObject(forKey: "notification")
        self.callApi(Path.getNotificationList, method: .get, completionHandler: { (result) in
            
            print("Hey Result: \(result)")
            if result.getBool(key: "status")
            {
                self.arrNotification = result.getArrayofDictionary(key: "data")
                
//                for i in 0..<self.arrNotification.count {
//                    if self.arrNotification[i].getInt(key: "Type") != 7 {
//                        let Message = self.arrNotification[i].getString(key: "Message")
//
//                        let CreatedDate = self.arrNotification[i].getString(key: "CreatedDate").toDate().timeAgoSinceDate2()
//                        let UserId = self.arrNotification[i].getInt(key: "UserId")
//                        let TypeID = self.arrNotification[i].getInt(key: "TypeID")
//                        self.data.append(Notification(Message: Message, CreatedDate: CreatedDate, UserId: UserId, TypeID: TypeID))
//                    }
//                }
                
                self.tableView.reloadData()
            }
           
        })

        // Do any additional setup after loading the view.
        self.title = "NOTIFICATIONS"

        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }

        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)

        let AccButton = UIButton(type: .system)
        AccButton.setImage(#imageLiteral(resourceName: "home").withRenderingMode(.alwaysOriginal), for: .normal)
        AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        AccButton.addTarget(self, action: #selector(AccButton(_:)), for: .touchUpInside)
        let barButtonItem = UIBarButtonItem(customView: AccButton)

        self.navigationItem.rightBarButtonItems = [barButtonItem]

        //SideBar
        if revealViewController() != nil
        {
            self.revealViewController().delegate = self
            menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

        tableView.delegate = self
        tableView.dataSource = self
        let footerView = UIView()
        footerView.backgroundColor = .clear
        tableView.tableFooterView = footerView
    }

    @objc func AccButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")

        navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {

            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {

            revealController.frontViewController.view.alpha = 1
            self.view.isUserInteractionEnabled = true
        }
    }

    // MARK: - Tableview datasource & delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotification.count
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NotificationCell

            cell.lblMessage.text = arrNotification[indexPath.row].getString(key: "Message")
            cell.lblDate.text = arrNotification[indexPath.row].getString(key: "CreatedDate").toDate().timeAgoSinceDate2()
        
        return cell
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrNotification[indexPath.row].getInt(key: "Type") == 6
        {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            vc.userId = "\(arrNotification[indexPath.row].getInt(key: "UserId"))"
            navigationController?.pushViewController(vc, animated: true)
        }
        
        else if arrNotification[indexPath.row].getInt(key: "Type") == 7 {
            tableView.deselectRow(at: indexPath, animated: true)
        }
            else if (arrNotification[indexPath.row].getInt(key: "Type") == 8) || (arrNotification[indexPath.row].getInt(key: "Type") == 9) || (arrNotification[indexPath.row].getInt(key: "Type") == 12) {
                      let nav = self.storyboard?.instantiateViewController(withIdentifier: "BlogDetailsVC") as! BlogDetailsVC
                                nav.blogId = "\(arrNotification[indexPath.row].getInt(key: "TypeID"))"
                                // nav.TYPE = "\(arrNotification[indexPath.row].getInt(key: "Type"))"
                                navigationController?.pushViewController(nav, animated: true)
                   }
        else if arrNotification[indexPath.row].getInt(key: "Type") == 10 {
                       let nav = self.storyboard?.instantiateViewController(withIdentifier: "BlogListDetailCommentView") as! BlogListDetailCommentView
                       nav.postId1 = "\(arrNotification[indexPath.row].getInt(key: "ReceiverId"))"
                        nav.dictData = arrNotification[indexPath.row]
                       navigationController?.pushViewController(nav, animated: true)
        }
            else if  (arrNotification[indexPath.row].getInt(key: "Type") == 13){
                               let nav = self.storyboard?.instantiateViewController(withIdentifier: "BlogListDetailCommentView") as! BlogListDetailCommentView
                               nav.postId1 = "\(arrNotification[indexPath.row].getInt(key: "UserId"))"
                                nav.dictData = arrNotification[indexPath.row]
                               navigationController?.pushViewController(nav, animated: true)
                }
            
        else
        {
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "BlogDetailsVC") as! BlogDetailsVC
            nav.blogId = "\(arrNotification[indexPath.row].getInt(key: "TypeID"))"
             nav.TYPE = "\(arrNotification[indexPath.row].getInt(key: "Type"))"
            navigationController?.pushViewController(nav, animated: true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
      cell.Blog_list_lbl_Time.text = record.getString(key: "PostTime").toDate().timeAgoSinceDate()
    */

}
