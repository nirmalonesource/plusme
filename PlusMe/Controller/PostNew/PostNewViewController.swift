//
//  PostNewViewController.swift
//  PlusMe
//
//  Created by Priyank Jotangiya on 23/07/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit
import SWRevealViewController

class PostNewViewController: UIViewController,SWRevealViewControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, ClosetImages, VisionBoardImages {
    
    @IBOutlet weak var btnPostNew: UIButton!
    @IBOutlet weak var txtCategory: UITextField!
    @IBOutlet var Vw_Cancel: UIView!
    @IBOutlet weak var imgPostImage: UIImageView!
    @IBOutlet weak var txtPostAbout: UITextField!

    var picker: UIPickerView!
    var arrCategoryItem = [String]()
    var arrCategory = [[String: Any]]()
    var selectedCategoryId = 0
    var isSelectedImage = false
    var selectedImageUrl = ""
    var imageSource = 0
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()

       txtCategory.ChangePlaceholderColor(placeholder: "Select Category")
        btnPostNew.roundCorner()
        //NavigationBar
//        self.navigationController?.navigationBar.isHidden = true

        picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        txtCategory.inputView = picker
        txtCategory.delegate = self
        
        let gesture = UITapGestureRecognizer(target: self, action: "someAction:")
        // or for swift 2 +
        let gestureSwift2AndHigher = UITapGestureRecognizer(target: self, action:  #selector (self.someAction (_:)))
        Vw_Cancel.addGestureRecognizer(gesture)


        StoredData.shared.getCategory { (result) in
            self.arrCategoryItem.removeAll()
            self.arrCategory = result
            for item in result
            {
                self.arrCategoryItem.append(item.getString(key: "CategoryName"))
            }
            self.picker.reloadAllComponents()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
                self.title = "Post New"

         navigationController?.navigationBar.barTintColor = UIColor.white
         navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
         self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }

         let menuButton = UIButton(type: .system)
         menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
         menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
         navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)

         //SideBar
         if revealViewController() != nil
         {
             self.revealViewController().delegate = self
             menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
             self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
             view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
         }
         
         let AccButton = UIButton(type: .system)
         AccButton.setImage(#imageLiteral(resourceName: "home").withRenderingMode(.alwaysOriginal), for: .normal)
         AccButton.tag = 1
         AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
         AccButton.addTarget(self, action: #selector(AccButton(_:)), for: .touchUpInside)
         let barButtonItem = UIBarButtonItem(customView: AccButton)
         
        // let NotiButton = UIButton(type: .system)
         let NotiButton = SSBadgeButton()
                         if UserDefaults.standard.object(forKey: "notification") != nil {
                             let noticount = UserDefaults.standard.string(forKey: "notification")
                             NotiButton.badge = noticount
                         }
                         else
                         {
                              NotiButton.badge = nil
                         }
         NotiButton.setImage(#imageLiteral(resourceName: "Notification").withRenderingMode(.alwaysOriginal), for: .normal)
         NotiButton.tag = 1
         NotiButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
         NotiButton.addTarget(self, action: #selector(NotiButtonItemButton(_:)), for: .touchUpInside)
         let NotiButtonItem = UIBarButtonItem(customView: NotiButton)
         
         self.navigationItem.rightBarButtonItems = [NotiButtonItem,barButtonItem]

    }
    
    @objc func AccButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")

        if button.tag == 1
        {
            let home = TabBarViewController()
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    @objc func NotiButtonItemButton(_ button: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }

    // or for Swift 4
    @objc func someAction(_ sender:UITapGestureRecognizer) {
        // do other task
        
        let home = TabBarViewController()
        home.setUI()
        self.navigationController?.pushViewController(home, animated: true)
    }

    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {

            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            revealController.frontViewController.view.alpha = 1
            self.view.isUserInteractionEnabled = true
        }
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtCategory
        {
            txtCategory.text = arrCategoryItem[0]
            selectedCategoryId = arrCategory[0].getInt(key: "CategoryID")
        }
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        return arrCategoryItem.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrCategoryItem[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        txtCategory.text = arrCategoryItem[row]
        selectedCategoryId = arrCategory[row].getInt(key: "CategoryID")
    }

    @IBAction func btnAddImageAction(_ sender: UIButton) {

        let actionSheet = UIAlertController(title: "Choose Action", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Vision Board", style: .default, handler: { (action) in

            self.imageSource = 1
            //self.performSegue(withIdentifier: "chooseImage", sender: nil)
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyVisionBoardViewController") as? MyVisionBoardViewController
            vc?.delegate = self
            vc?.isfrompostnew = true
            self.navigationController?.pushViewController(vc!, animated: true)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (action) in

            self.imageSource = 0
            self.fromDevice()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))

        present(actionSheet, animated: true, completion: nil)
    }

    func fromDevice()
    {
        let imagePicker = UIImagePickerController()

        let actionSheet = UIAlertController(title: "Take image", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in

            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                imagePicker.delegate = self
                imagePicker.isEditing = true
                imagePicker.allowsEditing = true
                imagePicker.accessibilityFrame = CGRect(x: 0, y: 0, width: 40, height: 50)
                self.present(imagePicker, animated: true, completion: nil)
            }
            else
            {
                self.showMessage(message: "Camera Not Available", completionHandler: nil)
            }
        }))
        actionSheet.addAction(UIAlertAction(title: "Photos", style: .default, handler: { (action) in

            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.delegate = self
            imagePicker.isEditing = true
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)

        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in


        }))

        present(actionSheet, animated: true, completion: nil)

    }

    @IBAction func btnPostNewAction(_ sender: UIButton) {

        if validateData()
        {
            var param: [String: Any] = [
                "PostTitle": txtPostAbout.text!.encodeEmoji,
                "CategoryID": selectedCategoryId,
                "Description":""
            ]

            var imageData = [Data]()
            imageData.append(UIImagePNGRepresentation(imgPostImage.image!)!)
            var dataKey = [String]()
            if imageSource == 0
            {
                param["ImageType"] = 0
                dataKey.append("PostImage")
            }
            else
            {
                param["ImageType"] = 1
                param["image_url"] = selectedImageUrl
            }

            callApi(Path.addNewPost, method: .post, param: param, data: imageData, dataKey: dataKey) { (result) in

                print(result)
                if result.getBool(key: "success")
                {
                    self.txtPostAbout.text = ""
                    self.txtCategory.text = ""
                    self.selectedCategoryId = 0
                    self.imgPostImage.image = nil
                    self.isSelectedImage = false
                    self.showMessage(message: result.getString(key: "message"), completionHandler: {
                        self.someAction(UITapGestureRecognizer())
                    })
                }
                    
                else
                {
                    self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                }
            }
        }
    }

    func validateData() -> Bool
    {
        if txtCategory.text!.isEmpty()
        {
            showMessage(message: "Please select category", completionHandler: nil)
            return false
        }
        else if txtPostAbout.text!.isEmpty()
        {
            showMessage(message: "Please mention about new post", completionHandler: nil)
            return false
        }
        else if !isSelectedImage
        {
            showMessage(message: "Please take image for new post", completionHandler: nil)
            return false
        }
        return true
    }

    // MARK: - ImagePicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerEditedImage] as! UIImage
        let fixedImaage = UIImage(cgImage: image.cgImage!, scale: image.scale, orientation: image.imageOrientation)

        imgPostImage.image = fixedImaage.resize(scale: 0.80)
        isSelectedImage = true
        picker.dismiss(animated: true, completion: nil)
    }

    func selectedUrl(url: String, id: Int) {
        isSelectedImage = true
        selectedImageUrl = url
        imgPostImage.setImage(url)
    }
    
    func visionBoardSelectedUrl(url: String, id: Int) {
         isSelectedImage = true
         selectedImageUrl = url
         imgPostImage.setImage(url)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "chooseImage"
        {
            let destination = segue.destination as! ChooseFromClosetViewController
            destination.delegate = self
        }
    }

}
