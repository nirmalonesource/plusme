//
//  ProfileViewController.swift
//  PlusMe
//
//  Created by Priyank Jotangiya on 23/07/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit
import SWRevealViewController
import DropDown

class ProfileViewController: UIViewController,SWRevealViewControllerDelegate, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var btnTab: [UIButton]!

    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var btnInfo: UIButton!
    @IBOutlet weak var lblFollow: UILabel!
    @IBOutlet weak var lblUserType: UILabel!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMessage: UILabel!

    var userData = [String: Any]()
    var arrMyPost = [[String: Any]]()
    var arrFollowers = [[String: Any]]()
    var arrFollowings = [[String: Any]]()
    var userId = StoredData.shared.userId
    var barButtonItem: UIBarButtonItem!
    var barButtonItem2: UIBarButtonItem!
    var barButtonMenuBack: UIBarButtonItem!
    var menuButton = UIButton()
    var isfromnotification = false
    
    let ReportDropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imgProfile.roundView()
        //NavigationBar
        self.navigationController?.navigationBar.isHidden = false;
        self.title = "MY PROFILE"
        
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }
        
        menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        barButtonMenuBack = UIBarButtonItem(customView: menuButton)
        
        let AccButton = UIButton(type: .system)
        AccButton.setImage(#imageLiteral(resourceName: "EDIT02").withRenderingMode(.alwaysOriginal), for: .normal)
        AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        AccButton.addTarget(self, action: #selector(AccButton(_:)), for: .touchUpInside)
         barButtonItem = UIBarButtonItem(customView: AccButton)

        let AccButton2 = UIButton(type: .system)
        AccButton2.tag = 2
        AccButton2.setImage(#imageLiteral(resourceName: "home").withRenderingMode(.alwaysOriginal), for: .normal)
        AccButton2.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        AccButton2.addTarget(self, action: #selector(AccButton2(_:)), for: .touchUpInside)
         barButtonItem2 = UIBarButtonItem(customView: AccButton2)
        tableView.delegate = self
        tableView.dataSource = self
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isHidden = true
    }

    override func viewWillAppear(_ animated: Bool) {
        self.lblName.text = ""
        self.lblUserType.text = ""
        loadData()
        getMyPost()
        getFollowers()
        getFollowings()
        StoredData.shared.rearViewController.updateUI()
    }

    func getMyPost()
    {
        arrMyPost.removeAll()
        callApi(Path.myPost, method: .post, param: ["UserProfileId": userId]) { (result) in

            print(result)
            if result.getBool(key: "success")
            {
                self.arrMyPost = result.getArrayofDictionary(key: "data")
            }
            if self.arrMyPost.count == 0
            {
                self.lblMessage.text = "No post found"
                self.lblMessage.isHidden = false
            }
            self.tableView.reloadData()
        }
    }

    func loadData()
    {
        callApi(Path.getUserProfile, method: .post, param: ["UserProfileID": userId]) { (result) in

            print(result)
            if result.getBool(key: "success")
            {
                let data = result.getDictionary(key: "user_data")
                self.userData = data
                self.lblName.text = data.getString(key: "FirstName") + " " + data.getString(key: "LastName")
                self.lblUserType.text = data.getInt(key: "RegisterUserType") == 0 ? "Trendsetter" : "Stylist/Designer"
                self.imgProfile.setImage(data.getString(key: "UserProfileImage"))
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.gesterHandeler1(_:)))
                self.imgProfile.isUserInteractionEnabled = true
                self.imgProfile.addGestureRecognizer(tapGesture)

                self.btnFollow.isSelected = data.getInt(key: "isFollowing") != 0
                self.lblFollow.text = data.getInt(key: "isFollowing") == 0 ? "Follow" : "Following"

                self.btnFollow.isHidden = data.getString(key: "Id") == StoredData.shared.userId
                self.lblFollow.isHidden = data.getString(key: "Id") == StoredData.shared.userId
                if data.getString(key: "Id") == StoredData.shared.userId
                {
                    self.title = "MY PROFILE"
                    self.navigationItem.rightBarButtonItems = [self.barButtonItem, self.barButtonItem2]
                    self.navigationItem.leftBarButtonItems = [self.barButtonMenuBack]
                    
                    //SideBar
                    if self.revealViewController() != nil
                    {
                        self.revealViewController().delegate = self
                        self.menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
                        self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                    }

                }
                    
                else
                {
                    self.title = "PROFILE"
                    self.menuButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
                    self.menuButton.addTarget(self, action: #selector(self.backAction(_:)), for: .touchUpInside)
                    self.navigationItem.leftBarButtonItems = [self.barButtonMenuBack]
                    self.navigationItem.rightBarButtonItems = [self.barButtonItem2]
                }
            }
        }
    }

    func getFollowers()
    {
        arrFollowers.removeAll()
        let param = [
            "UserProfileId":userId
        ]

        callApi(Path.getFollowers, method: .post, param: param) { (result) in
            print(result)
            if result.getBool(key: "success")
            {
                self.arrFollowers = result.getArrayofDictionary(key: "data")
            }
            self.collectionView.reloadData()
        }
    }

    func getFollowings()
    {
        arrFollowings.removeAll()
        let param = [
            "UserProfileId":userId
        ]

        callApi(Path.getFollowing, method: .post, param: param) { (result) in
            print(result)
            if result.getBool(key: "success")
            {
                self.arrFollowings = result.getArrayofDictionary(key: "data")
            }
            self.collectionView.reloadData()
        }
    }
    
    func DeletePost(BlogID:String)
       {
           arrMyPost.removeAll()
           callApi(Path.RemoveBlog, method: .post, param: ["BlogID": BlogID]) { (result) in

               print(result)
               if result.getBool(key: "success")
               {
                self.getMyPost()
                  // self.arrMyPost = result.getArrayofDictionary(key: "data")
               }
//               if self.arrMyPost.count == 0
//               {
//                   self.lblMessage.text = "No post found"
//                   self.lblMessage.isHidden = false
//               }
            //   self.tableView.reloadData()
           }
       }

    //MARK: - Actions👊🏻😠
    
    @objc func AccButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")

        let nav = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        nav.userData = self.userData
        nav.isEdit = userData.getString(key: "Id") == StoredData.shared.userId
        navigationController?.pushViewController(nav, animated: true)
    }
    
    @objc func backAction(_ button: UIButton) {
        if isfromnotification {
            let home = TabBarViewController()
            self.navigationController?.pushViewController(home, animated: true)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
       
    }
    
    @objc func AccButton2(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        let home = TabBarViewController()
        home.setUI()
        self.navigationController?.pushViewController(home, animated: true)
        
    }
    @objc func buttonClicked(sender:UIButton) {

        sender.isSelected = !sender.isSelected
    }

    @IBAction func btnInfoAction(_ sender: UIButton) {

        performSegue(withIdentifier: "userInfo", sender: nil)
//        let nav = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
//        nav.userData = self.userData
//        nav.isEdit = userData.getString(key: "Id") == StoredData.shared.userId
//        navigationController?.pushViewController(nav, animated: true)
    }

    @IBAction func btnFollowAction(_ sender: UIButton) {

        let param: [String: Any] = [
            "FollowUserID": userId,
            "isFollow": sender.isSelected ? 0 : 1,
        ]

        callApi(Path.userFollow, method: .post, param: param) { (result) in

            print(result)
            if result.getBool(key: "success")
            {
                sender.isSelected = !sender.isSelected
                self.getFollowings()
            }
        }
    }

    @IBAction func btnTabAction(_ sender: UIButton) {

        for btn in btnTab
        {
            btn.isSelected = false
        }
        sender.isSelected = true

        switch sender.tag {
        case 1:
            tableView.isHidden = false
            collectionView.isHidden = true
            collectionView.reloadData()
            if self.arrMyPost.count == 0
            {
                self.lblMessage.text = "No post found"
                self.lblMessage.isHidden = false
            }
            else
            {
                lblMessage.isHidden = true
            }
        case 2:
            tableView.isHidden = true
            collectionView.isHidden = false
            collectionView.reloadData()
            if self.arrFollowers.count == 0
            {
                self.lblMessage.text = "No followers found"
                self.lblMessage.isHidden = false
            }
            else
            {
                lblMessage.isHidden = true
            }
        case 3:
            tableView.isHidden = true
            collectionView.isHidden = false
            collectionView.reloadData()
            if self.arrFollowings.count == 0
            {
                self.lblMessage.text = "No followings found"
                self.lblMessage.isHidden = false
            }
            else
            {
                lblMessage.isHidden = true
            }
        default:
            break
        }
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrMyPost.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MenuTableViewCell

        let post = arrMyPost[indexPath.row]

        cell.Blog_list_VW.layer.borderWidth = 1.0
        cell.Blog_list_VW.layer.borderColor = UIColor.lightGray.cgColor
        cell.Blog_list_VW.layer.cornerRadius = 5.0

//        cell.Btn_Closet.tag = indexPath.row
//        cell.Btn_Closet.addTarget(self, action: #selector(buttonClicked(sender:)), for: UIControlEvents.touchUpInside)

        cell.Blog_list_img.layer.cornerRadius = cell.Blog_list_img.frame.height/2
        cell.Blog_list_img.clipsToBounds = true
        cell.Blog_list_img.setImage(post.getString(key: "ProfileImage"))
        
        if StoredData.shared.userId == post["PostedByID"] as! String {
            cell.btnreport.isHidden = false
        }
        else
        {
            cell.btnreport.isHidden = true
        }

        cell.btnreport.tag = indexPath.row
        cell.btnreport.addTarget(self, action: #selector(btnReportClicked(sender:)), for: UIControlEvents.touchUpInside)
        
        cell.Blog_list_lbl_Banner.setImage(post.getString(key: "PostImage"))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(gesterHandeler(_:)))
        cell.Blog_list_lbl_Banner.isUserInteractionEnabled = true
        cell.Blog_list_lbl_Banner.tag = indexPath.row
        cell.Blog_list_lbl_Banner.addGestureRecognizer(tapGesture)

        cell.Blog_list_lbl_Title.text = post.getString(key: "PostedByName")
        cell.Blog_list_lbl_Owner.text = userData.getInt(key: "RegisterUserType") == 0 ? "Trendsetter" : "Stylist/Designer"
        cell.Blog_list_lbl_Time.text = post.getString(key: "PostTime").toDate().timeAgoSinceDate()
        cell.Blog_list_lbl_Detail.text = post.getString(key: "PostTitle").decodeEmoji

        return cell
    }
    
    //MARK: - Actions👊🏻😠
    
    @objc func btnReportClicked(sender:UIButton) {
        
        let post = arrMyPost[sender.tag]
        
        ReportDropDown.anchorView = sender
              ReportDropDown.dataSource = ["Delete"]
        ReportDropDown.selectionAction = { [weak self] (index, item) in
            print(sender.tag)
            self?.DeletePost(BlogID: post.getString(key: "BlogId"))

                      }
        ReportDropDown.show()
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "BlogDetailsVC") as! BlogDetailsVC
        nav.blogId = arrMyPost[indexPath.row].getString(key: "BlogId")
        navigationController?.pushViewController(nav, animated: true)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if btnTab[2].isSelected
        {
            return arrFollowings.count
        }
        else if btnTab[1].isSelected
        {
            return arrFollowers.count
        }
        return 0
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var record = [String: Any]()
        if btnTab[2].isSelected
        {
            record = arrFollowings[indexPath.row]
        }
        else if btnTab[1].isSelected
        {
            record = arrFollowers[indexPath.row]
        }

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FollowCell

        cell.imgProfile.roundView()
        cell.imgProfile.setImage(record.getString(key: "ProfileImage"))
        cell.lblName.text = "\(record.getString(key: "FirstName")) \(record.getString(key: "LastName"))"
        cell.lblUserType.text = record.getString(key: "RegisterUserTypeByName")
        addGesture(cell.imgProfile, userId: record.getInt(key: "Id"))
        addGesture(cell.lblName, userId: record.getInt(key: "Id"))

        cell.setBorder()
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width / 2.0 - 2
        let yourHeight = CGFloat(80.0)

        return CGSize(width: yourWidth, height: yourHeight)
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "userInfo"
        {
            let destination = segue.destination as! UserInfoViewController
            destination.userInfo = userData
        }
    }
    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {

            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {

            revealController.frontViewController.view.alpha = 1
            self.view.isUserInteractionEnabled = true
        }
    }
    @IBOutlet weak var viewZoomPopup: UIView!
    @IBOutlet weak var imgZoomImage: UIImageView!

    @IBAction func btnHideZoomPopupAction(_ sender: UIButton) {

        viewZoomPopup.isHidden = true
    }
    @objc func gesterHandeler(_ sender: UITapGestureRecognizer)
    {
        let row = sender.view!.tag
        imgZoomImage.setImage(arrMyPost[row].getString(key: "PostImage"))
        if ((imgZoomImage?.image) != nil) {
             viewZoomPopup.isHidden = false
        }
       
    }
    @objc func gesterHandeler1(_ sender: UITapGestureRecognizer)
    {
        imgZoomImage.setImage(userData.getString(key: "UserProfileImage"))
        viewZoomPopup.isHidden = false
    }
}


class FollowCell: UICollectionViewCell
{
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUserType: UILabel!
    @IBOutlet weak var lblName: UILabel!

}
