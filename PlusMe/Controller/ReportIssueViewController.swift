//
//  ReportIssueViewController.swift
//  PlusMe
//
//  Created by My Mac on 27/03/20.
//  Copyright © 2020 Priyank Jotangiya. All rights reserved.
//

import UIKit

class ReportIssueViewController: UIViewController {
    
    @IBOutlet var vwmiddle: UIView!
    @IBOutlet var imgprofile: UIImageView!
    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var lblsubtitle: UILabel!
    @IBOutlet var lbldate: UILabel!
    @IBOutlet var lbldescr: UILabel!
    @IBOutlet var imgpost: UIImageView!
    
    @IBOutlet var imgpostheight: NSLayoutConstraint!
    
    @IBOutlet var vwbottom: UIView!
    @IBOutlet var textview_statement: UITextView!
    
    var dicdata = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setdata()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setdata()  {
        
               vwmiddle.layer.borderWidth = 1.0
               vwmiddle.layer.borderColor = UIColor.lightGray.cgColor
               vwmiddle.layer.cornerRadius = 5.0
        
        vwbottom.layer.borderWidth = 1.0
        vwbottom.layer.borderColor = UIColor.lightGray.cgColor
        vwbottom.layer.cornerRadius = 5.0
        
            textview_statement.layer.borderWidth = 1.0
              textview_statement.layer.borderColor = UIColor.lightGray.cgColor
              textview_statement.layer.cornerRadius = 5.0

               
               imgprofile.layer.cornerRadius = imgprofile.frame.height/2
               imgprofile.clipsToBounds = true
               imgprofile.backgroundColor = .lightGray
               imgprofile.setImage(dicdata.getString(key: "ProfileImage"))
            
               
               let url = URL(string: dicdata["PostImage"] as? String ?? "")
                    //let url = URL(string: record.getString(key: "PostImage"))
               
                        DispatchQueue.main.async {
                           
                            self.imgpost.sd_setImage(with: url, placeholderImage: nil, options: .progressiveLoad, completed: { (theImage, error, cache, url) in
                          
                     //   imgpostheight.constant = self.getAspectRatioAccordingToiPhones(cellImageFrame: cellFrame,downloadedImage: theImage!)
                                self.imgpost.setNeedsLayout() //invalidate current layout
                                self.imgpost.layoutIfNeeded()
                          
                            
        })
                          }
                     

               
               lbltitle.text = dicdata.getString(key: "PostedByName")
               lblsubtitle.text = dicdata.getString(key: "RegisterUserTypeByName")
               lbldate.text = dicdata.getString(key: "PostTime").toDate().timeAgoSinceDate()
               lbldescr.text = dicdata.getString(key: "PostTitle")
             
        
    }

    @IBAction func back_click(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func submit_click(_ sender: UIButton) {
        
        if textview_statement.text != "" {
             let param = [
                       "BlogID": dicdata.getString(key: "BlogId"),
                       "Message": textview_statement.text
                ] as [String : Any]
                   callApi(Path.BlogReport, method: .post, param: param) { (result) in
                       print(result)
                       if result.getBool(key: "success")
                       {
                          self.showMessage(message: result.getString(key: "message"), completionHandler: {
                                self.navigationController?.popViewController(animated: true)
                        })
                       }
                   }
        }
        else
        {
             self.showMessage(message: "Please Enter Issue", completionHandler: nil)
            }

        }
    }
    


