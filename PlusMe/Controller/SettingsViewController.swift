//
//  SettingsViewController.swift
//  PlusMe
//
//  Created by rohit on 13/09/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit
import SWRevealViewController

class SettingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SWRevealViewControllerDelegate
{

    @IBOutlet weak var tableView: UITableView!

   // let arrOptions = ["Change Password", "Allow Notification", "Privacy", "About Plusme", "Terms & Condition ", "Logout"]
    let arrOptions = ["Change Password", "Allow Notification", "About Plusme", "Terms & Condition","Invite Friends", "Logout"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        self.title = "SETTINGS"

        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }

        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)

        //SideBar
        if revealViewController() != nil
        {
            self.revealViewController().delegate = self
            menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

        let AccButton = UIButton(type: .system)
        AccButton.setImage(#imageLiteral(resourceName: "home").withRenderingMode(.alwaysOriginal), for: .normal)
        AccButton.tag = 1
        AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        AccButton.addTarget(self, action: #selector(AccButton(_:)), for: .touchUpInside)
        let barButtonItem = UIBarButtonItem(customView: AccButton)
        self.navigationItem.rightBarButtonItems = [barButtonItem]
    }

    @objc func AccButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")

        if button.tag == 1
        {
            let home = TabBarViewController()
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    //MARK:- TABLEVIEW METHODS
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOptions.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let lblOption = cell.viewWithTag(10) as! UILabel
        lblOption.text = arrOptions[indexPath.row]

        let sw = cell.viewWithTag(20) as! UISwitch

        sw.isHidden = indexPath.row != 1
//        sw.setOn(StoredData.shared.isAllowNotification, animated: true)
//        sw.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)

        cell.selectionStyle = .none
        return cell
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        switch indexPath.row {
        case 0:
            let vc = storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
            navigationController?.pushViewController(vc, animated: true)

        case 2:
            let vc = storyboard?.instantiateViewController(withIdentifier: "AboutViewController") as! AboutViewController
            navigationController?.pushViewController(vc, animated: true)
        case 3:
            let vc = storyboard?.instantiateViewController(withIdentifier: "termConditionViewController") as! termConditionViewController
            navigationController?.pushViewController(vc, animated: true)

        case 4:
        
//            let attrs = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.red]
//                   let str = NSAttributedString(string: "text", attributes: attrs)
//                   let print = UISimpleTextPrintFormatter(attributedText: str)
//
//                   let vc = UIActivityViewController(activityItems: [print], applicationActivities: nil)
//                   present(vc, animated: true)
            
            if let name = URL(string: "https://itunes.apple.com/us/app/myapp/id1455444072?ls=1&mt=8"), !name.absoluteString.isEmpty {
                let objectsToShare = [name]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

                self.present(activityVC, animated: true, completion: nil)
            }else  {
                // show alert for not available
            }
            
        case 5:
        
            let alertController = UIAlertController(title: "Hey There!", message: "Are you sure want to logout?", preferredStyle: UIAlertControllerStyle.alert) //Replace UIAlertControllerStyle.Alert by UIAlertControllerStyle.alert

            let DestructiveAction = UIAlertAction(title: "No, Thanks!", style: UIAlertActionStyle.destructive) {
                (result : UIAlertAction) -> Void in
                print("Destructive")
            }

            // Replace UIAlertActionStyle.Default by UIAlertActionStyle.default

            let okAction = UIAlertAction(title: "OK, Sure!", style: UIAlertActionStyle.default) {
                (result : UIAlertAction) -> Void in
                print("OK")

                self.callApi(Path.logout, method: .get, completionHandler: { (result) in

                    print(result)
                    StoredData.shared.rearViewController.logout()
                })

            }

            alertController.addAction(DestructiveAction)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
            
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        let lblOption = cell.viewWithTag(10) as! UILabel
        let sw = cell.viewWithTag(20) as! UISwitch

        let row = indexPath.row

        switch row {
        case 1:
            lblOption.text = arrOptions[indexPath.row]
            sw.isHidden = indexPath.row != 1
            sw.setOn(StoredData.shared.isAllowNotification, animated: true)
            sw.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)
//        case 2:
//
//            lblOption.font = .boldSystemFont(ofSize: 20)
//            lblOption.text = arrOptions[indexPath.row]
//            sw.isHidden = indexPath.row != 2
//            sw.setOn(StoredData.shared.isPrivate, animated: true)
//            sw.addTarget(self, action: #selector(switchValueDidChangeTerm(_:)), for: .valueChanged)
        default:
            break
        }
    }

    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {

            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {

            revealController.frontViewController.view.alpha = 1
            self.view.isUserInteractionEnabled = true
        }
    }

    @objc func switchValueDidChange(_ sender: UISwitch) {
        let param = [
            "isNotificationEnable": sender.isOn ? "1" : "0"
        ]
        
        callApi(Path.allowNotification, method: .post, param: param) { (result) in
            print(result)
            if result.getInt(key: "status") == 1
            {
                StoredData.shared.isAllowNotification = sender.isOn

                self.showMessage(message: result.getString(key: "message"), completionHandler: {
                })
            }
        }

    }
    
    @objc func switchValueDidChangeTerm(_ sender: UISwitch) {
        print("Swich Clicked")
        let param = [
            "IsPrivate": sender.isOn ? "1" : "0"
        ]
        
        callApi(Path.isPrivate, method: .post, param: param) { (result) in
            print(result)
            if result.getInt(key: "status") == 1
            {
                StoredData.shared.isPrivate = sender.isOn
                
                self.showMessage(message: result.getString(key: "message"), completionHandler: {
                })
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
