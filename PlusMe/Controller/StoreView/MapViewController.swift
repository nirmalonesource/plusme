//
//  MapViewController.swift
//  PlusMe
//
//  Created by My Mac on 15/05/19.
//  Copyright © 2019 Priyank Jotangiya. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    var product_latitude = ""
    var product_longitude = ""
    var arrMapStores = [String: Any]()
    var message = ""
    
    @IBOutlet weak var viewTitleShow: UIView!
    @IBOutlet weak var lblName: UILabel!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTitleShow.isHidden = true
        product_latitude = arrMapStores.getString(key: "StoreLatitude")
        product_longitude = arrMapStores.getString(key: "StoreLongiture")
              
        
        if product_latitude != "" && product_longitude != "" {
             mapView.mapType = MKMapType.standard
            let location = CLLocationCoordinate2D(latitude: Double(arrMapStores.getString(key: "StoreLatitude")) ?? 0.0,
                                                  longitude: Double(arrMapStores.getString(key: "StoreLongiture")) ?? 0.0)
            
           if (CLLocationCoordinate2DIsValid(location))
           {
            
            let span = MKCoordinateSpan(latitudeDelta: 0.003, longitudeDelta: 0.003)
            let region = MKCoordinateRegion(center: location, span: span)
            if(region.center.longitude == -180.00000000){
                print("Invalid region!")
            }
                
            else {
                mapView.setRegion(region, animated: true)
                let annotation = MKPointAnnotation()
                annotation.coordinate = location
                annotation.title = arrMapStores.getString(key: "StoreName")
                annotation.subtitle = arrMapStores.getString(key: "StoreAddress")
                mapView.addAnnotation(annotation)
            }
            
           }
            
           else {
            
            viewTitleShow.isHidden = true
            message = "Location Not Availabel"
            
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.viewTitleShow.bounds.size.width, height: self.viewTitleShow.bounds.size.height))
            noDataLabel.text = "\(message)."
            noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
            noDataLabel.textAlignment = .center
            lblName = noDataLabel
            //self.viewTitleShow.addSubview(noDataLabel)
            
        }
    }
        //StoreWebLink,StoreImage
            
        else {
            
            viewTitleShow.isHidden = true
            message = "Location Not Availabel"
            let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.viewTitleShow.bounds.size.width, height: self.viewTitleShow.bounds.size.height))
            noDataLabel.text = "\(message)."
            noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
            noDataLabel.textAlignment = .center
            lblName = noDataLabel
            //self.viewTitleShow.addSubview(noDataLabel)
        }
    }
    
    //MARK:- Button Action
    
    @IBAction func backActionClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
     let one = 0.8727/50
     
     let two = 180/3.14
     let degree = one * two
    */

}
