//
//  StoreTableViewCell.swift
//  PlusMe
//
//  Created by Priyank Jotangiya on 23/07/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit

class StoreTableViewCell: UITableViewCell {
    
    //ImageView
    @IBOutlet var Img_Item: UIImageView!
    @IBOutlet var Lbl_ItemHeader: UILabel!
    @IBOutlet var Lbl_ItemDescription: UILabel!
    @IBOutlet var Lbl_ItemLocation: UILabel!
    @IBOutlet weak var btnLocationOutlet: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
