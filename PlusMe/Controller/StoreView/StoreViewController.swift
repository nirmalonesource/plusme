//
//  StoreViewController.swift
//  PlusMe
//
//  Created by Priyank Jotangiya on 23/07/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit
import SWRevealViewController

class StoreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SWRevealViewControllerDelegate, UISearchBarDelegate , UIGestureRecognizerDelegate
{

    @IBOutlet weak var tableView: UITableView!
    var arrStores = [[String: Any]]()

    private var latitude = 30.7333
    private var longitude = 76.7794
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
     
    }

    override func viewWillAppear(_ animated: Bool) {
        
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "VisionBoardViewController") as! VisionBoardViewController
        nav.resetview()
        
        //NavigationBar
             self.navigationController?.navigationBar.isHidden = false;
             self.title = "Stores"
             
             navigationController?.navigationBar.barTintColor = UIColor.white
             navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
             self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }
             
             let menuButton = UIButton(type: .system)
             menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
             menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
             navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
             
            // let AccButton = UIButton(type: .system)
             let AccButton = SSBadgeButton()
                             if UserDefaults.standard.object(forKey: "notification") != nil {
                                 let noticount = UserDefaults.standard.string(forKey: "notification")
                                 AccButton.badge = noticount
                             }
                             else
                             {
                                  AccButton.badge = nil
                             }
             AccButton.setImage(#imageLiteral(resourceName: "Notification").withRenderingMode(.alwaysOriginal), for: .normal)
             AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
             AccButton.addTarget(self, action: #selector(AccButton(_:)), for: .touchUpInside)
             AccButton.tag = 1
             let barButtonItem = UIBarButtonItem(customView: AccButton)
             
             let btnPlus = UIButton(type: .system)
             btnPlus.setImage(#imageLiteral(resourceName: "addnew").withRenderingMode(.alwaysOriginal), for: .normal)
             btnPlus.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
             btnPlus.addTarget(self, action: #selector(btnPlusAction(_:)), for: .touchUpInside)
             let barButtonItem3 = UIBarButtonItem(customView: btnPlus)

             let AccButton2 = UIButton(type: .system)
             AccButton2.setImage(#imageLiteral(resourceName: "Search").withRenderingMode(.alwaysOriginal), for: .normal)
             AccButton2.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
             AccButton2.addTarget(self, action: #selector(AccButton2(_:)), for: .touchUpInside)
             let barButtonItem2 = UIBarButtonItem(customView: AccButton2)
             
             self.navigationItem.rightBarButtonItems = [barButtonItem, barButtonItem2, barButtonItem3]
             
             //SideBar
             if revealViewController() != nil
             {
                 self.revealViewController().delegate = self
                 menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
                 self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                 view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
             }
        
        tableView.tableFooterView = UIView()
        loadStore()
        self.view.layoutIfNeeded()
    }

    func loadStore()
    {
        callApi(Path.getStore, method: .get) { (result) in
            print(result)
            if result.getBool(key: "success")
            {
                self.arrStores = result.getArrayofDictionary(key: "data")
                self.arrResult = self.arrStores
                self.tableView.reloadData()
            }
        }
    }
    
    //MARK: - Actions👊🏻😠

    @objc func AccButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        if button.tag == 1
        {
            let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }

    @objc func btnPlusAction(_ button: UIButton) {
        performSegue(withIdentifier: "addStore", sender: nil)
    }
    
    @objc func AccButton2(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        navigationItem.rightBarButtonItems = nil

        let searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 200, height: 20))
        searchBar.placeholder = "Search"
        searchBar.delegate = self
        navigationItem.titleView = searchBar
        title = nil

        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        menuButton.addTarget(self, action: #selector(searchAction(_:)), for: .touchUpInside)
    }

    @objc func searchAction(_ button: UIButton) {

        navigationItem.titleView = nil
        title = "Home"

        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)

        //SideBar
        if revealViewController() != nil
        {
            self.revealViewController().delegate = self
            menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

      //  let AccButton = UIButton(type: .system)
        let AccButton = SSBadgeButton()
                        if UserDefaults.standard.object(forKey: "notification") != nil {
                            let noticount = UserDefaults.standard.string(forKey: "notification")
                            AccButton.badge = noticount
                        }
                        else
                        {
                             AccButton.badge = nil
                        }
        AccButton.setImage(#imageLiteral(resourceName: "Notification").withRenderingMode(.alwaysOriginal), for: .normal)
        AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        AccButton.addTarget(self, action: #selector(AccButton(_:)), for: .touchUpInside)
        AccButton.tag = 1
        let barButtonItem = UIBarButtonItem(customView: AccButton)

        let AccButton2 = UIButton(type: .system)
        AccButton2.setImage(#imageLiteral(resourceName: "Search").withRenderingMode(.alwaysOriginal), for: .normal)
        AccButton2.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        AccButton2.addTarget(self, action: #selector(AccButton2(_:)), for: .touchUpInside)
        let barButtonItem2 = UIBarButtonItem(customView: AccButton2)
        
        let btnPlus = UIButton(type: .system)
                    btnPlus.setImage(#imageLiteral(resourceName: "addnew").withRenderingMode(.alwaysOriginal), for: .normal)
                    btnPlus.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
                    btnPlus.addTarget(self, action: #selector(btnPlusAction(_:)), for: .touchUpInside)
                    let barButtonItem3 = UIBarButtonItem(customView: btnPlus)

        self.navigationItem.rightBarButtonItems = [barButtonItem, barButtonItem2,barButtonItem3]

        arrResult = arrStores
        tableView.reloadData()
    }
    //MARK: - Methods(Functions)☝🏻😳

    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {

            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {

            revealController.frontViewController.view.alpha = 1
            self.view.isUserInteractionEnabled = true
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! StoreTableViewCell
        cell.layoutIfNeeded()
        let store = arrResult[indexPath.row]

        cell.Lbl_ItemHeader.text = store.getString(key: "StoreName")
        cell.Lbl_ItemDescription.text = store.getString(key: "StoreDescritption")
        cell.Lbl_ItemLocation.text = store.getString(key: "StoreAddress")
        cell.Img_Item.setImage(store.getString(key: "StoreImage"))
        //cell.Lbl_ItemLocation.tag = indexPath.row
//        cell.btnLocationOutlet.tag = indexPath.row
//
       // let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handlePan))
        let tapGesture = UITapGestureRecognizer (target: self, action: #selector(imgTap(tapGesture:)))
        tapGesture.delegate = self
        cell.Img_Item.tag = indexPath.row
        cell.Img_Item.isUserInteractionEnabled = true
        cell.Img_Item.addGestureRecognizer(tapGesture)
        
        cell.layoutIfNeeded()
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let store = arrResult[indexPath.row]
         let vc = storyboard?.instantiateViewController(withIdentifier: "MapViewController") as? MapViewController
         vc?.arrMapStores = store
        self.navigationController?.pushViewController(vc!, animated: true)
     }
    
    /*
     let latitude = dictSingleListingData[0]["product_latitude"] as? String ?? ""
     let longitude = dictSingleListingData[0]["product_longitude"] as? String ?? ""
     if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
     if #available(iOS 10.0, *) {
     UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(latitude),\(longitude)&zoom=14&views=traffic&q=\(latitude),\(longitude)")!, options: [:], completionHandler: nil)
     } else {
     // Fallback on earlier versions
     }
     } else {
     print("Can't use comgooglemaps://")
     if #available(iOS 10.0, *) {
     UIApplication.shared.open(URL(string: "http://maps.google.com/maps?q=loc:\(latitude),\(longitude)&zoom=14&views=traffic")!, options: [:], completionHandler: nil)
     } else {
     // Fallback on earlier versions
     }
     }
 */
    @objc func imgTap(tapGesture: UITapGestureRecognizer) {
        //let imgView = tapGesture.view as! UIImageView
        // let idToMove = imgView.tag
         let store = arrResult[(tapGesture.view?.tag)!]
       print("store:",store)
//        guard let url = URL(string: store["StoreWebLink"] as! String) else { return }
//        UIApplication.shared.open(url)
        
        let urlString = store["StoreWebLink"] as! String
        if urlString.hasPrefix("https://") || urlString.hasPrefix("http://"){
               let myURL = URL(string: urlString)
             //  let myRequest = URLRequest(url: myURL!)
            UIApplication.shared.open(myURL!)
           }else {
               let correctedURL = "http://\(urlString)"
               let myURL = URL(string: correctedURL)
             //  let myRequest = URLRequest(url: myURL!)
                UIApplication.shared.open(myURL!)
           }

    }
    
    @objc func handlePan(_ gestureRecognizer: UITapGestureRecognizer) {
        
        let store = arrResult[(gestureRecognizer.view?.tag)!]
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "MapViewController") as? MapViewController
         vc?.arrMapStores = store
        self.navigationController?.pushViewController(vc!, animated: true)
        
        /*
        let latitude = store.getString(key: "StoreLatitude")
        let longitude = store.getString(key: "StoreLongiture")

         print("StoreLatitude:\(store.getString(key: "StoreLatitude"))")
         print("StoreLongiture:\(store.getString(key: "StoreLongiture"))")

        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(latitude),\(longitude)&zoom=14&views=traffic&q=\(latitude),\(longitude)")!, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        } else {
            print("Can't use comgooglemaps://")
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: "http://maps.google.com/maps?q=loc:\(latitude),\(longitude)&zoom=14&views=traffic")!, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }
       */
    }
    
    var arrResult = [[String: Any]]()
    
    //MARK: - Searchbar Delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        arrResult = arrStores.filter { (dict) -> Bool in

            if dict.getString(key: "StoreName").lowercased().contains(searchText.lowercased())
            {
                return true
            }
            return false
        }
        
        print(arrResult)
        if searchText == ""
        {
            arrResult = arrStores
        }
        
        tableView.reloadData()
    }
}
