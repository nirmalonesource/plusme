//
//  TermsViewController.swift
//  PlusMe
//
//  Created by rohit on 09/08/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit
import SWRevealViewController

class TermsViewController: UIViewController, SWRevealViewControllerDelegate {

    @IBOutlet weak var lblName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let lines = linesFromResourceForced(fileName: "terms.txt")
//        print(lines)
        
        //lblName.text = "\(lines)"
        
        if let filepath = Bundle.main.path(forResource: "terms", ofType: "txt") {
            do {
                let contents = try String(contentsOfFile: filepath)
                print(contents)
            } catch {
                // contents could not be loaded
            }
        } else {
            // example.txt not found!
        }
        
        // Do any additional setup after loading the view.
        self.title = "TERMS OF USE"

        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }

        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)

        //SideBar
        if revealViewController() != nil
        {
            self.revealViewController().delegate = self
            menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        let AccButton = UIButton(type: .system)
        AccButton.setImage(#imageLiteral(resourceName: "home").withRenderingMode(.alwaysOriginal), for: .normal)
        AccButton.tag = 1
        AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        AccButton.addTarget(self, action: #selector(AccButton(_:)), for: .touchUpInside)
        let barButtonItem = UIBarButtonItem(customView: AccButton)
        self.navigationItem.rightBarButtonItems = [barButtonItem]
    }
    
    //MARK:- TXT VIEW FILE FUNCTION
    func linesFromResourceForced(fileName: String) -> [String] {
        let path = Bundle.main.path(forResource: fileName, ofType: nil)!
        let content = try! String(contentsOfFile: path, encoding: String.Encoding.utf8)
        return content.components(separatedBy: "\n")
    }

    @objc func AccButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")

        if button.tag == 1
        {
            let home = TabBarViewController()
            self.navigationController?.pushViewController(home, animated: true)
        }
    }
    
    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {

            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            
        } else {
            revealController.frontViewController.view.alpha = 1
            self.view.isUserInteractionEnabled = true
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
