//
//  UserInfoViewController.swift
//  PlusMe
//
//  Created by rohit on 10/09/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit

class UserInfoViewController: UIViewController {

    @IBOutlet weak var imgUserImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblUserType: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblContacts: UILabel!
    @IBOutlet weak var lblWorkingHours: UILabel!
    @IBOutlet weak var lblFacebookLink: UILabel!
    @IBOutlet weak var lblInstagLink: UILabel!
    
    @IBOutlet weak var lblPrivacy: UILabel!
    @IBOutlet weak var switchPrivacy: UISwitch!
    
    @IBOutlet weak var lblEmailOutlet: UILabel!
    
    @IBOutlet weak var lblContactOutlet: UILabel!
    
    @IBOutlet weak var imgOutlet: UIImageView!
    
    var userInfo: [String: Any]!
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        navigationController?.navigationBar.isHidden = true
        title = "PROFILE"

        navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.2274509804, blue: 0.462745098, alpha: 1)
//        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }

        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        menuButton.addTarget(self, action: #selector(AccButton1(_:)), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)

        imgUserImage.roundView(.white, borderWidth: 2)
        imgUserImage.setImage(userInfo.getString(key: "UserProfileImage"))
        lblName.text = "\(userInfo.getString(key: "FirstName")) \(userInfo.getString(key: "LastName"))" + " "
        lblUserType.text = userInfo.getString(key: "RegisterUserTypeByName") + " "
        lblCompanyName.text = userInfo.getString(key: "CompanyName") + " "
        lblEmail.text = userInfo.getString(key: "Email") + " "
        lblContacts.text = userInfo.getString(key: "MobileNo") + " "
        lblWorkingHours.text = "\(userInfo.getString(key: "WorkingHours")) hours" + " "
        lblFacebookLink.text = userInfo.getString(key: "FacebookLink") + " "
        lblInstagLink.text = userInfo.getString(key: "InstragramLink") + " "
        
        let isPrivate = userInfo.getInt(key: "IsPrivate")
        
      
     //    let data = result.getDictionary(key: "user_data")
        
        if userInfo.getString(key: "Id") != StoredData.shared.userId
        {
            if isPrivate == 1 {
                      lblEmailOutlet.isHidden = true
                      lblContactOutlet.isHidden = true
                      imgOutlet.isHidden = false
                      lblEmail.text = "This account is private"
                      lblContacts.isHidden = true
                  }
            
            lblPrivacy.isHidden = true
            switchPrivacy.isHidden = true
        }
        else
        {
            lblEmailOutlet.isHidden = false
            lblContactOutlet.isHidden = false
            imgOutlet.isHidden = true
            // lblEmail.text = "This account is private"
            lblContacts.isHidden = false
            
            if isPrivate == 0 {
                     
                switchPrivacy.isOn = false
                   }
            else
            {
                 switchPrivacy.isOn = true
            }
        }
    }
    @IBAction func swich_changed(_ sender: UISwitch) {
        if sender.isOn {
            isprivateapi(isprivate: "1")
        }
        else
        {
            isprivateapi(isprivate: "0")
        }
    }
    
    func isprivateapi(isprivate:String)
    {
        
        let param = [
            "IsPrivate":isprivate
        ]

        callApi(Path.isPrivate, method: .post, param: param) { (result) in
            print(result)
            self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
//            if result.getBool(key: "success")
//            {
//
//            }
            
        }
    }

    
    override func viewDidAppear(_ animated: Bool) {
        imgUserImage.roundView()
    }

    @objc func AccButton1(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillDisappear(_ animated: Bool) {

//        navigationController?.navigationBar.isHidden = false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
