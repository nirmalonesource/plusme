//
//  ViewVisonBoardViewController.swift
//  PlusMe
//
//  Created by rohit on 30/09/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import UIKit
import SWRevealViewController

class ViewVisonBoardViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,SWRevealViewControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var txtCategory: UITextField!
    @IBOutlet weak var btnMoveToCloset: UIButton!

    var arrImages = [String]()
    var picker: UIPickerView!
    var arrCategoryItem = [String]()
    var arrCategory = [[String: Any]]()
    var selectedCategoryId = 0
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false;
        self.title = "Vision Board"

        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }

        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)

        //SideBar
        if revealViewController() != nil
        {
            self.revealViewController().delegate = self
            menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

        // Do any additional setup after loading the view.
        txtCategory.layer.borderColor = #colorLiteral(red: 0.9294117647, green: 0.2196078431, blue: 0.462745098, alpha: 1)
        txtCategory.layer.borderWidth = 1
        picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        txtCategory.inputView = picker
        txtCategory.delegate = self

        btnMoveToCloset.roundCorner()

        StoredData.shared.getCategory { (result) in
            self.arrCategoryItem.removeAll()
            self.arrCategory = result
            for item in result
            {
                self.arrCategoryItem.append(item.getString(key: "CategoryName"))
            }
            self.picker.reloadAllComponents()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        let fileManager = FileManager.default
        let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        do {
            let fileURLs = try fileManager.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
            // process files
            print(fileURLs)
            arrImages.removeAll()
            for url in fileURLs
            {
                arrImages.append(url.absoluteString)
            }
            collectionView.reloadData()
        } catch {
            print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
        }
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtCategory
        {
            txtCategory.text = arrCategoryItem[0]
            selectedCategoryId = arrCategory[0].getInt(key: "CategoryID")
        }
    }

    //MARK:- PICKER METHODS
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        return arrCategoryItem.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrCategoryItem[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        txtCategory.text = arrCategoryItem[row]
        selectedCategoryId = arrCategory[row].getInt(key: "CategoryID")
    }
    
    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {

            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            revealController.frontViewController.view.alpha = 1
            self.view.isUserInteractionEnabled = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   //MARK:- COLLETION VIEW METHODS
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrImages.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: "item", for: indexPath) as! ClosetImagesCollectionView
        item.ClosetImg.setImage(arrImages[indexPath.row])
        return item
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
//        let cell = collectionView.cellForItem(at: indexPath) as! ClosetImagesCollectionView
//
//        imageView.image = cell.ClosetImg.image
//        viewPopup.isHidden = false
    }

    //collectionViewLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = UIScreen.main.bounds.width
        return CGSize(width: width / 3 - 10, height: width / 3 - 10) // width & height are the same to make a square cell
    }

    @IBAction func btnHideAction(_ sender: UIButton) {
         viewPopup.isHidden = true
    }
    
    @IBAction func btnMoveToClosetAction(_ sender: UIButton) {

        if txtCategory.text!.isEmpty()
        {
            showMessage(message: "Please select cetegory.", completionHandler: nil)
        }
        else
        {
            let param: [String: Any] = [
                "CategoryID": selectedCategoryId,
            ]

            callApi(Path.moveToCloset, method: .post, param: param, data: [UIImagePNGRepresentation((imageView.image?.resize(scale: 0.7))!)!], dataKey: ["PostImage"]) { (result) in

                print(result)
                if result.getBool(key: "success")
                {
                    self.txtCategory.text = ""
                    self.selectedCategoryId = 0
                    self.showMessage(message: result.getString(key: "message"), completionHandler: {
                        self.viewPopup.isHidden = true
                    })
                }
                else
                {
                    self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
