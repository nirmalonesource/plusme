//
//  ViewController.swift
//  VisionBoard
//
//  Created by rohit on 27/09/18.
//  Copyright © 2018 rohit. All rights reserved.
//

import UIKit
import CoreGraphics
import Photos
import SWRevealViewController
import Alamofire

class VisionBoardViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, SWRevealViewControllerDelegate, ClosetImages, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UITabBarControllerDelegate, UIScrollViewDelegate , CropImageDelegate , UIGestureRecognizerDelegate {
  
    
    
    var GetcropImage = UIImage()
    var GetshapLayer =  CAShapeLayer()
    
    @IBOutlet weak var scrllVw: UIScrollView!
    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var viewCropper: UIView!
    @IBOutlet weak var imgCropper: UIImageView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var btnCrop: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var viewSavePopup: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var txtCategory: UITextField!
    @IBOutlet weak var btnMoveToCloset: UIButton!
    
    @IBOutlet weak var btnPlusOutlet: UIButton!
    
    @IBOutlet weak var btnEditOutlet: UIButton!
    
    var imgeViewArray = [ImageView]()
    var ShapeLayer = [CAShapeLayer]()

    @IBOutlet weak var vwShowLabel: UIView!
    @IBOutlet weak var imgScreenShot: UIImageView!
    
    var imgCropped: UIImageView!
    var arrImages = [UIImageView]()
    var isFromImagePicker = false
    var imageIndex = 1
    var imageFromCloset = false
    var closetImageId = 0
    var selectedCategoryId = 0
    var picker: UIPickerView!
    var arrCategoryItem = [String]()
    var arrCategory = [[String: Any]]()
    var imageDicArray = [[String: Any]]()
    
    //Update this for path line color
    let strokeColor:UIColor = UIColor.red
    
    //Update this for path line width
    let lineWidth:CGFloat = 2.0
    
    var path = UIBezierPath()
    var shapeLayer = CAShapeLayer()
    var croppedImage = UIImage()
    var isRotation = Bool()
  var  AccButton2 = UIButton()
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vwShowLabel.setBorder()
        vwShowLabel.setRadiusBorder(color: .darkGray)
        //vwShowLabel.isHidden = true
        // Do any additional setup after loading the view, typically from a nib.
        
        self.title = "Create Look"
        tabBarController?.delegate = self
        
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black,NSAttributedStringKey.font: UIFont(name: "Lato-Bold", size: 17)!]
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = UIColor.white
            navigationController?.navigationBar.standardAppearance = appearance;
            navigationController?.navigationBar.scrollEdgeAppearance =  navigationController?.navigationBar.standardAppearance
        } else {
            // Fallback on earlier versions
        }
        
        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "Menu").withRenderingMode(.alwaysOriginal), for: .normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        
        let AccButton = UIButton(type: .system)
        AccButton.setImage(#imageLiteral(resourceName: "home").withRenderingMode(.alwaysOriginal), for: .normal)
        AccButton.tag = 1
        AccButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        AccButton.addTarget(self, action: #selector(AccButton(_:)), for: .touchUpInside)
        let barButtonItem = UIBarButtonItem(customView: AccButton)
        //        self.navigationItem.rightBarButtonItems = [barButtonItem]
        AccButton2 = UIButton(type: .system)
        AccButton2.setImage(#imageLiteral(resourceName: "more").withRenderingMode(.alwaysOriginal), for: .normal)
        AccButton2.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        AccButton2.addTarget(self, action: #selector(AccButton2(_:)), for: .touchUpInside)
        AccButton2.isHidden = true
        let barButtonItem2 = UIBarButtonItem(customView: AccButton2)
        self.navigationItem.rightBarButtonItems = [barButtonItem2, barButtonItem]//, barButtonItem3]
        
        //SideBar
        if revealViewController() != nil
        {
            self.revealViewController().delegate = self
            menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.navigationController?.navigationBar.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            //            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        txtCategory.layer.borderColor = #colorLiteral(red: 0.9294117647, green: 0.2196078431, blue: 0.462745098, alpha: 1)
        txtCategory.layer.borderWidth = 1
        picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        txtCategory.inputView = picker
        txtCategory.delegate = self
        
        btnMoveToCloset.roundCorner()
        
        StoredData.shared.getCategory { (result) in
            self.arrCategoryItem.removeAll()
            self.arrCategory = result
            
            for item in result
            {
                self.arrCategoryItem.append(item.getString(key: "CategoryName"))
            }
            self.picker.reloadAllComponents()
        }
        
        // My Code Here
        //        let rotationGesture = UIRotationGestureRecognizer(target: self, action:#selector(self.rotationGesture(sender:)))
        //        imgPhoto.isUserInteractionEnabled = true
        //        imgPhoto.addGestureRecognizer(rotationGesture)
    }
    
//    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//        isFromImagePicker = false
//    }
//
//    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
//
//        isFromImagePicker = false
//        viewMain.isHidden = true
//        imgPhoto.image = nil
//    }
   
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        StoredData.shared.isEditClosetImage = false
        self.title = "Create Look"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnEditOutlet.isHidden = true
        
        if StoredData.shared.isEditClosetImage
        {
            for view in viewMain.subviews{
                view.removeFromSuperview()
            }
            AccButton2.isHidden = false
            vwShowLabel.isHidden = true
          //  btnEditOutlet.isHidden = false
            let origImage = UIImage(named: "EDIT02")
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnEditOutlet.setImage(tintedImage, for: .normal)
            btnEditOutlet.tintColor = UIColor.white
            
            self.title = "Edit Look"
//            viewMain.isHidden = false
//            let image1 = UIImageView()
//            image1.contentMode = .scaleAspectFit
//            image1.backgroundColor = UIColor.clear
//            image1.image = StoredData.shared.closetImage
//            image1.frame = CGRect(x: 0, y: 0, width: viewMain.frame.width, height: viewMain.frame.height)
//            viewMain.addSubview(image1)
//            self.view.layoutIfNeeded()
             editCropFun(crImage: StoredData.shared.closetImage)
            
        }
        else {
             self.title = "Create Look"
        }
        
        imgeViewArray.append(imgPhoto)
        ShapeLayer.append(shapeLayer)
        
        shapeLayer.masksToBounds = true
        shapeLayer.borderWidth = 1
        
        shapeLayer.borderColor = UIColor.red.cgColor
        btnPlusOutlet.layer.masksToBounds  = true
        btnPlusOutlet.layer.cornerRadius = btnPlusOutlet.frame.width/2
        btnEditOutlet.layer.masksToBounds  = true
        btnEditOutlet.layer.cornerRadius = btnEditOutlet.frame.width/2
        
        // btnPlusOutlet.titleLabel?.adjustsFontSizeToFitWidth = true
        btnPlusOutlet.titleLabel?.font = .boldSystemFont(ofSize: 30)
        imgPhoto.isHidden = true
        self.view.layoutIfNeeded()
    }
    
    //MARK:- Open Picker
    
    func PhotosPicker() {
        let imagePicker = UIImagePickerController()
        imageIndex = 1
        let actionSheet = UIAlertController(title: "Choose action", message: "(Maximum File Size Limit - 2 MB.)", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                imagePicker.delegate = self
                imagePicker.isEditing = true
                imagePicker.allowsEditing = true
                
                self.present(imagePicker, animated: true, completion: nil)
            }
                
            else
            {
                self.showMessage(message: "Camera Not Available", completionHandler: nil)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photos", style: .default, handler: { (action) in
            
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.delegate = self
            //imagePicker.isEditing = true
           // imagePicker.allowsEditing = true
            
            self.present(imagePicker, animated: true, completion: nil)
            
        }))
//        actionSheet.addAction(UIAlertAction(title: "My Closet", style: .default, handler: { (action) in
//
//            self.imageFromCloset = true
//
//            self.performSegue(withIdentifier: "chooseImage", sender: nil)
//
//        }))
        
        if StoredData.shared.isEditClosetImage != true
        {
            actionSheet.addAction(UIAlertAction(title: "Collage Maker", style: .default, handler: { (action) in
                
                self.isFromImagePicker = false
                self.performSegue(withIdentifier: "collageMaker", sender: nil)
                //            let storyboard = UIStoryboard(name: "Main2", bundle: nil)
                //            let vc = storyboard.instantiateViewController(withIdentifier: "navigation")
                //            self.navigationController?.pushViewController(vc, animated: true)
                
            }))
        }
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        
        present(actionSheet, animated: true, completion: nil)
        
    }
    
    //MARK:- Rotation + Zooming
  
//    func setup() {
//        // imgPhoto.isUserInteractionEnabled = true
//        scrllVw.delegate = self
//    }
    
//    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
//        // return imgPhoto
//        return viewMain
//    }
//
    //MARK:- Button Action
    
    @objc func AccButton(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        
        if button.tag == 1
        {
//            let home = TabBarViewController()
//            self.navigationController?.pushViewController(home, animated: true)
          //  let nav = self.storyboard?.instantiateViewController(withIdentifier: "MyFeedViewController") as! MyFeedViewController
            self.IBActionCancelCrop(button)
              self.tabBarController?.selectedIndex = 2
           // self.navigationController?.pushViewController(nav, animated: true)
        }
    }
    
    @objc func AccButton2(_ button: UIButton) {
        print("Button with tag: \(button.tag) clicked!")
        viewPopup.isHidden = false
    }
    
    @IBAction func btnPhotoAction(_ sender: UIButton) {
        viewPopup.isHidden = true
    //    imageIndex = sender.tag
//
//        let imagePicker = UIImagePickerController()
//
//        let actionSheet = UIAlertController(title: "Take image", message: nil, preferredStyle: .actionSheet)
//        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
//
//            if UIImagePickerController.isSourceTypeAvailable(.camera)
//            {
//                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
//                imagePicker.delegate = self
//                imagePicker.isEditing = true
//                imagePicker.allowsEditing = true
//                self.present(imagePicker, animated: true, completion: nil)
//            }
//            else
//            {
//                self.showMessage(message: "Camera Not Available", completionHandler: nil)
//            }
//        }))
//        actionSheet.addAction(UIAlertAction(title: "Photos", style: .default, handler: { (action) in
//
//            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
//            imagePicker.delegate = self
//            imagePicker.isEditing = true
//            imagePicker.allowsEditing = true
//            self.present(imagePicker, animated: true, completion: nil)
//
//        }))
//        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
//
//        }))
//
//        present(actionSheet, animated: true, completion: nil)
        
        PhotosPicker()
    }
    
    @IBAction func btnDeleteAction(_ sender: UIButton) {
        isRotation = false
        shapeLayer.removeFromSuperlayer()
        imgPhoto.layer.sublayers?.removeAll(keepingCapacity: true)
        path = UIBezierPath()
        shapeLayer = CAShapeLayer()
        addNewPathToImage()
        
        if arrImages.count > 0
        {
            arrImages.last?.removeFromSuperview()
            arrImages.removeLast()
            vwShowLabel.isHidden = false
        }
            
        else {
            vwShowLabel.isHidden = false
        }
    }
    
    @IBAction func btnSaveAction(_ sender: UIButton) {
        viewPopup.isHidden = true
        viewMain.isHidden = false
        
        for vw in viewMain.subviews as [UIView] {
            for btn in vw.subviews as [UIView] {
                btn.layer.borderColor = UIColor.clear.cgColor
            }
        }
        
        for vw in viewMain.subviews as [UIView] {
            for btn in vw.subviews as [UIView] {
                if let btn1 = btn as? UIButton {
                    btn1.isHidden = true
                    btn1.removeFromSuperview()
                }
            }
        }
        
        imgScreenShot.isHidden = true
        vwShowLabel.isHidden = true
        imgScreenShot.image = viewMain.takeScreenshot()
        
        let actionSheet = UIAlertController(title: "Save to?", message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Save To Visionboard", style: .default, handler: { (action) in
            
            UIGraphicsBeginImageContextWithOptions(self.viewMain.bounds.size, false, 0);
            
            self.viewMain.drawHierarchy(in: self.viewMain.bounds, afterScreenUpdates: true)
            
            let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            self.imageView.image = image
            self.viewSavePopup.isHidden = false
            
            if self.txtCategory.text!.isEmpty()
            {
                self.showMessage(message: "Please select category.", completionHandler: nil)
            }
            else
            {
//                self.updateToVisionBoard()
                
                let param: [String: Any] = [
                    "CategoryID": self.selectedCategoryId,
                    "Type" : "IMAGE"
                    ]
                
                self.callApi(Path.MoveToVisionBoard,
                             method: .post,
                             param: param,
                             data: [UIImagePNGRepresentation((self.imgScreenShot.image?.resize(scale: 1))!)!],
                             dataKey: ["PostImage"])
                { (result) in

                    print(result)

                    if result.getBool(key: "success")
                    {
                        self.closetImageId = 0
                        self.showMessage(message: result.getString(key: "message"), completionHandler: {
                            self.viewPopup.isHidden = true
                            self.isFromImagePicker = false

                            self.viewMain.isHidden = false
                            self.imgScreenShot.isHidden = true
                            self.vwShowLabel.isHidden = true
                            for view in self.viewMain.subviews {
                                view.removeFromSuperview()
                            }
                            self.imageDicArray.removeAll()
                        })
                        
                       let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyVisionBoardViewController") as? MyVisionBoardViewController
                        vc!.isfromlook = true
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }
                    else
                    {
                        self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                    }
                }
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Save New To MyCloset", style: .default, handler: { (action) in
            
            let param: [String: Any] = [
               // "BaseImageID": self.closetImageId,
                "Type" : "IMAGE",
                ]
            
            UIGraphicsBeginImageContextWithOptions(self.viewMain.bounds.size, false, 0);
            
            self.viewMain.drawHierarchy(in: self.viewMain.bounds, afterScreenUpdates: true)
            self.callApi(Path.saveToCloset,
                         method: .post,
                         param: param,
                         data: [UIImagePNGRepresentation((self.imgScreenShot.image?.resize(scale: 1))!)!],
                         dataKey: ["PostImage"])
            { (result) in
                
                print(result)
                if result.getBool(key: "success")
                {
                    self.closetImageId = 0
                    self.showMessage(message: result.getString(key: "message"), completionHandler: {
                        self.viewPopup.isHidden = true
                        self.isFromImagePicker = false
                        self.viewMain.isHidden = false
                        self.imgScreenShot.isHidden = true
                        self.vwShowLabel.isHidden = true
                        for view in self.viewMain.subviews{
                            view.removeFromSuperview()
                        }
                         self.imageDicArray.removeAll()
                        
                        let nav = self.storyboard?.instantiateViewController(withIdentifier: "MyClosetViewController") as! MyClosetViewController
                             //  nav.blogId = arrMyPost[indexPath.row].getString(key: "BlogId")
                         self.tabBarController?.selectedIndex = 0
                        self.navigationController?.pushViewController(nav, animated: true)
                       
                    })
                }
                else
                {
                    self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                }
            }
            
        }))
        
        if StoredData.shared.isfrommycloset
        {
            actionSheet.addAction(UIAlertAction(title: "Save To MyCloset", style: .default, handler: { (action) in
                   
                   let param: [String: Any] = [
                       "BaseImageID": StoredData.shared.ClosetID!,
                       "Type" : "IMAGE"
                       ]
                   
                   UIGraphicsBeginImageContextWithOptions(self.viewMain.bounds.size, false, 0);
                   
                   self.viewMain.drawHierarchy(in: self.viewMain.bounds, afterScreenUpdates: true)
                   self.callApi(Path.saveToCloset,
                                method: .post,
                                param: param,
                                data: [UIImagePNGRepresentation((self.imgScreenShot.image?.resize(scale: 1))!)!],
                                dataKey: ["PostImage"])
                   { (result) in
                       
                       print(result)
                       if result.getBool(key: "success")
                       {
                           self.closetImageId = 0
                           self.showMessage(message: result.getString(key: "message"), completionHandler: {
                               self.viewPopup.isHidden = true
                               self.isFromImagePicker = false
                               self.viewMain.isHidden = false
                               self.imgScreenShot.isHidden = true
                               self.vwShowLabel.isHidden = true
                               for view in self.viewMain.subviews{
                                   view.removeFromSuperview()
                               }
                                self.imageDicArray.removeAll()
                               
                               let nav = self.storyboard?.instantiateViewController(withIdentifier: "MyClosetViewController") as! MyClosetViewController
                            self.tabBarController?.selectedIndex = 0
                                    //  nav.blogId = arrMyPost[indexPath.row].getString(key: "BlogId")
                               self.navigationController?.pushViewController(nav, animated: true)
                           })
                       }
                       else
                       {
                           self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                       }
                   }
                   
               }))
        }
        
        if closetImageId != 0
        {
            actionSheet.addAction(UIAlertAction(title: "Save New to MyCloset", style: .default, handler: { (action) in
                
                let param: [String: Any] = [
                    //                "BaseImageID": self.closetImageId,
                    :]
                
                UIGraphicsBeginImageContextWithOptions(self.viewMain.bounds.size, false, 0);
                
                self.viewMain.drawHierarchy(in: self.viewMain.bounds, afterScreenUpdates: true)
                
                let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
                
                self.callApi(Path.saveToCloset, method: .post, param: param, data: [UIImagePNGRepresentation((self.imgScreenShot.image!.resize(scale: 0.9)))!], dataKey: ["PostImage"]) { (result) in
                    
                    print(result)
                    if result.getBool(key: "success")
                    {
                        self.closetImageId = 0
                        self.showMessage(message: result.getString(key: "message"), completionHandler: {
                            self.viewPopup.isHidden = true
                            self.isFromImagePicker = false
                            self.viewMain.isHidden = false
                            self.imgScreenShot.isHidden = true
                            
                            for view in self.viewMain.subviews{
                                view.removeFromSuperview()
                            }
                             self.imageDicArray.removeAll()
                            
                        })
                    }
                    else
                    {
                        self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                    }
                }
                
            }))
        }
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        
        present(actionSheet, animated: true, completion: nil)
    }
    
   @objc public func resetview()  {
//       self.viewPopup.isHidden = true
//        self.isFromImagePicker = false
//        self.viewMain.isHidden = false
//        self.imgScreenShot.isHidden = true
//        self.vwShowLabel.isHidden = true
//        for view in self.viewMain.subviews{
//            view.removeFromSuperview()
//        }
         self.imageDicArray.removeAll()
         //      self.view.bringSubview(toFront: vwShowLabel)
    }
    
    @IBAction func IBActionCancelCrop(_ sender: UIButton) {
        viewPopup.isHidden = true
          StoredData.shared.isfrommycloset = false
        isRotation = false
        shapeLayer.removeFromSuperlayer()
        imgPhoto.layer.sublayers?.removeAll(keepingCapacity: true)
        path = UIBezierPath()
        shapeLayer = CAShapeLayer()
        addNewPathToImage()
        vwShowLabel.isHidden = false
        self.view.bringSubview(toFront: vwShowLabel)
        resetCropping()
    }
    
    @IBAction func plusIconClicked(_ sender: UIButton) {
        PhotosPicker()
    }
    
    @IBAction func btnEditAction(_ sender: UIButton) {
        
        while let subview = viewMain.subviews.last {
            subview.removeFromSuperview()
        }
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "NewCropViewController") as? NewCropViewController
        vc?.isCrop = true
        vc?.crImage = StoredData.shared.closetImage
        vc?.delegate = self
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func btnHideAction(_ sender: UIButton) {
        viewSavePopup.isHidden = true
    }
    
    @IBAction func btnMoveToClosetAction(_ sender: UIButton) {
        
        if txtCategory.text!.isEmpty()
        {
            showMessage(message: "Please select cetegory.", completionHandler: nil)
        }
        else
        {
            let param: [String: Any] = [
                "CategoryID": selectedCategoryId,
                 "Type" : "IMAGE"
                ]
            
            callApi(Path.MoveToVisionBoard, method: .post, param: param, data: [UIImagePNGRepresentation((imageView.image?.resize(scale: 1))!)!], dataKey: ["PostImage"]) { (result) in
                
                print(result)
                if result.getBool(key: "success")
                {
                    self.txtCategory.text = ""
                    self.selectedCategoryId = 0
                    self.showMessage(message: result.getString(key: "message"), completionHandler: {
                        self.viewPopup.isHidden = true
                        self.viewSavePopup.isHidden = true
                        self.isFromImagePicker = false
                        
                        self.viewMain.isHidden = false
                        self.imgScreenShot.isHidden = true
                        
                        for view in self.viewMain.subviews{
                            view.removeFromSuperview()
                        }
                        self.imageDicArray.removeAll()
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyVisionBoardViewController") as? MyVisionBoardViewController
                                          vc!.isfromlook = true
                        self.navigationController?.pushViewController(vc!, animated: true)
                        
                    })
                    
                   
                }
                else
                {
                    self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                }
            }
        }
    }
    
    @IBAction func btnHideShowPopupAction(_ sender: UIButton) {
        
        viewPopup.isHidden = true
    }
    
    @IBAction func IBActionCropImage(_ sender: UIButton) {
        //        shapeLayer.fillColor = UIColor.black.cgColor
        //        imgPhoto.layer.mask = shapeLayer
        //        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CropingImageController") as! CropingImageController
        //        nextViewController.GetCropImage = imgPhoto
        //        nextViewController.shapeLayer1 = shapeLayer
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
        /*
         shapeLayer.fillColor = UIColor.black.cgColor
         imgCropper.layer.mask = shapeLayer
         
         cropImage()
         imgCropped = UIImageView(image: croppedImage)
         imgCropped.backgroundColor = .clear
         imgCropped.contentMode = .scaleAspectFit
         imgCropped.clipsToBounds = true
         viewMain.addSubview(imgCropped)
         arrImages.append(imgCropped)
         viewCropper.isHidden = true
         
         let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.panHandler(_:)))
         imgCropped.addGestureRecognizer(panGesture)
         imgCropped.isUserInteractionEnabled = true
         
         shapeLayer.removeFromSuperlayer()
         path = UIBezierPath()
         shapeLayer = CAShapeLayer()
         viewPopup.isHidden = true
         */
    }
   
    @objc func btnCellDeleteImageAction(sender: UIButton!)
    //@objc func btnCellDeleteImageAction(_ sender : UIButton)
    {
        let dict : [String : Any] = imageDicArray[sender.tag - 1]
        for vw in viewMain.subviews as [UIView] {
            if vw.tag == sender.tag
            {
                vw.removeFromSuperview()
               // imageDicArray = imageDicArray.filter{!NSDictionary(dictionary: $0).isEqual(to: dict)}
                print(imageDicArray)
            }
        }
    }
    
    @IBAction func btnSaveImageAction(_ sender: UIButton) {
        viewMain.isHidden = false

        for vw in viewMain.subviews as [UIView] {
            for btn in vw.subviews as [UIView] {
                //                if let v = btn as? UIView {
                btn.layer.borderColor = UIColor.clear.cgColor
                //                }
            }
        }
        
        for vw in viewMain.subviews as [UIView] {
            for btn in vw.subviews as [UIView] {
                if let btn1 = btn as? UIButton {
                    btn1.isHidden = true
                    btn1.removeFromSuperview()
                }
            }
        }
        
        imgScreenShot.isHidden = false
        vwShowLabel.isHidden = true
        imgScreenShot.image = viewMain.takeScreenshot()
        viewMain.isHidden = true
        
        for view in viewMain.subviews{
            view.removeFromSuperview()
        }
        
        imageDicArray.removeAll()
    }
    
    func resetCropping()
    {
        viewMain.isHidden = false
        
        for vw in viewMain.subviews as [UIView] {
            for btn in vw.subviews as [UIView] {
                //                if let v = btn as? UIView {
                btn.layer.borderColor = UIColor.clear.cgColor
                //                }
            }
        }
        
        for vw in viewMain.subviews as [UIView] {
            for btn in vw.subviews as [UIView] {
                if let btn1 = btn as? UIButton {
                    btn1.isHidden = true
                    btn1.removeFromSuperview()
                }
            }
        }
        
        imgScreenShot.isHidden = true
        
        imgScreenShot.image = viewMain.takeScreenshot()
        viewMain.isHidden = false
        
        for view in viewMain.subviews{
//            if let img = view as? UIImageView
//            {
//            }
//            else
//            {
                view.removeFromSuperview()
//            }
        }
        
        imageDicArray.removeAll()
    }
    
    //MARK:- API
    func updateToVisionBoard(){
//        if !isInternetAvailable(){
//            noInternetConnectionAlert(uiview: self)
//        }
//        else
//        {
//            SVProgressHUD.s
        
        let parameters: [String: Any] = [
            "CategoryID": self.selectedCategoryId,
             "Type" : "IMAGE"
            ] as [String : Any]
        
            let headers = ["Auth": StoredData.shared.Auth
        ]
        let imgdata =  UIImageJPEGRepresentation(imgScreenShot.image!, 0.7)

            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                multipartFormData.append(imgdata! , withName: "PostImage",fileName: "file.png", mimeType: "image/png")
                
                for (key, value) in parameters {
                    multipartFormData.append(((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!), withName: key)
                }
                
                //                multipartFormData.append(self.imgdata, withName: "photo", fileName: "image.png", mimeType: "image/png")
                
            },
                             usingThreshold: UInt64.init(),
                             to: "https://plusme.blenzabi.com/admin/api/web_service/MoveToVisionBoard",
                             method: .post,
                             headers:headers
            ) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print("Succesfully uploaded  = \(response)")
                        if let err = response.error{
                            
                        }
                        else
                        {
                            var resData : [String : AnyObject] = [:]
                            guard let data = response.result.value as? [String:AnyObject],
                                let _ = data["success"] as? Bool
                                else{
                                    print("Malformed data received from fetchAllRooms service")
                                    
                                    return
                            }
                            
                            resData = data
                            
                        }
                        
                    }
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    
                }
            }
//        }
    }
    //MARK:- Save
    
    func saveToGallery()
    {
        let alert = UIAlertController(title: "", message: "Save picture to gallery?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (action) in
            
            UIGraphicsBeginImageContextWithOptions(self.viewMain.bounds.size, false, 0);
            //        self.view.drawHierarchy(in: CGRect(x: 0, y: 0, width: viewMain.bounds.size.width, height: viewMain.bounds.size.height), afterScreenUpdates: true)
            self.viewMain.drawHierarchy(in: self.viewMain.bounds, afterScreenUpdates: true)
            
            let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIImageWriteToSavedPhotosAlbum((image), nil, nil, nil)
            
            // get the documents directory url
            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            print(documentsDirectory)
            // choose a name for your image
            let fileName = "\(Date().timeIntervalSince1970).jpg"
            // create the destination file url to save your image
            let fileURL = documentsDirectory.appendingPathComponent(fileName)
            // get your UIImage jpeg data representation and check if the destination file url already exists
            if let data = UIImageJPEGRepresentation(image, 1.0),
                !FileManager.default.fileExists(atPath: fileURL.path) {
                do {
                    // writes the image data to disk
                    try data.write(to: fileURL)
                    print("file saved")
                    self.showMessage(message: "File saved successfully.", completionHandler: nil)
                } catch {
                    print("error saving file:", error)
                }
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    
    
//     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//      imgCropper.image = info[UIImagePickerControllerOriginalImage] as? UIImage
//
//     self.dismiss(animated: true, completion: nil)
//     }
   
    
    // MARK: - ImagePicker delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        isFromImagePicker = true
//        let image = info[UIImagePickerControllerEditedImage] as! UIImage
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage

        let fixedImaage = UIImage(cgImage: image.cgImage!, scale: image.scale, orientation: image.imageOrientation)

        //        isImageSelected = true
        if imageIndex == 1
        {
            // imgPhoto.image = fixedImaage//.resize(scale: 0.25)
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewCropViewController") as! NewCropViewController
            nextViewController.GetCropImage.image = fixedImaage
            // nextViewController.shapeLayer1 = shapeLayer
            nextViewController.delegate = self
            self.navigationController?.pushViewController(nextViewController, animated: true)

            viewMain.isHidden = false
        }
        else
        {
            imgCropper.image = nil
            imgCropper.image = fixedImaage//.resize(scale: 0.25)
            viewCropper.isHidden = false
        }
AccButton2.isHidden = false
        vwShowLabel.isHidden = true
        viewPopup.isHidden = true
        picker.dismiss(animated: true, completion: nil)
    }
    
    func addNewPathToImage() {
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = strokeColor.cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = lineWidth
        imgPhoto.layer.addSublayer(shapeLayer)
    }
    
    func cropImage() {
        
        vwShowLabel.isHidden = true
        UIGraphicsBeginImageContextWithOptions(imgPhoto.bounds.size, false, 1)
        
        imgPhoto.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        self.croppedImage = newImage!
    }
    
    @objc func panHandler(_ pan: UIPanGestureRecognizer)
    {
        isRotation = true
        let translation = pan.translation(in: viewMain)
        imgCropped.center = CGPoint(x: imgCropped.center.x + translation.x, y: imgCropped.center.y + translation.y)
        pan.setTranslation(.zero, in: viewMain)
    }
    
    
    func revealController(_ revealController: SWRevealViewController, didMoveTo position: FrontViewPosition) {
        if case .right = position {
            
            revealController.frontViewController.view.alpha = 0.5
            self.view.isUserInteractionEnabled = false
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        } else {
            
            revealController.frontViewController.view.alpha = 1
            self.view.isUserInteractionEnabled = true
        }
    }
    
    //MARK:- Picker
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return arrCategoryItem.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrCategoryItem[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        txtCategory.text = arrCategoryItem[row]
        selectedCategoryId = arrCategory[row].getInt(key: "CategoryID")
    }
    
    func selectedUrl(url: String, id: Int) {
        
        self.isFromImagePicker = true
        imgPhoto.setImage(url)
        viewMain.isHidden = false
        closetImageId = id
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "chooseImage"
        {
            let destination = segue.destination as! ChooseFromClosetViewController
            destination.delegate = self
        }
    }
    
    //MARK:- Cropping
    
    func cropImage(cropImage: UIImage, ShapLayer: CAShapeLayer) {
        viewMain.isHidden = false
        imgPhoto.isHidden = true
        imageDicArray.append(["image" : cropImage , "layer" : ShapLayer])
        print("image: \(imageDicArray)")
        cropFun(crImage: cropImage, ShapLayer: ShapLayer)
    }
    
//    func editCropFun(crImage: UIImage) {
//        imgPhoto.isHidden = true
//        viewMain.isHidden = false
//        self.view.layoutIfNeeded()
//        let main1View = UIView() //UIView()
//        let image1 = UIImageView()
//
//        let cropVW = UIView() //UIView()
//        cropVW.backgroundColor = UIColor.clear
//        cropVW.frame = viewMain.frame
//        cropVW.clipsToBounds = true
//        btnPlusOutlet.titleLabel?.font = .boldSystemFont(ofSize: 20)
//
//        main1View.frame = CGRect.init(x: 0, y: 0, width: crImage.size.width, height: crImage.size.height)
//        main1View.backgroundColor = UIColor.clear
//        image1.frame = CGRect.init(x: 0, y: 0, width: viewMain.frame.width, height: viewMain.frame.height)
//
//        image1.isUserInteractionEnabled = true
//        image1.image = crImage
//        image1.layer.masksToBounds = true
//        image1.clipsToBounds = true
//
//        main1View.backgroundColor = UIColor.clear
//        image1.backgroundColor = UIColor.clear
//
//        main1View.frame = CGRect.init(x: 0, y: 0, width: viewMain.frame.width, height: viewMain.frame.height)
//        main1View.addSubview(image1)
//        main1View.addSubview(cropVW)
//        viewMain.addSubview(main1View)
//
//        let TapgestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handleTap(_:)))
//               TapgestureRecognizer.delegate = self
//           //    TapgestureRecognizer.cancelsTouchesInView = false
//               main1View.addGestureRecognizer(TapgestureRecognizer)
//
//        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
//        gestureRecognizer.delegate = self
//        gestureRecognizer.cancelsTouchesInView = false
//        main1View.addGestureRecognizer(gestureRecognizer)
//
//        //Enable multiple touch and user interaction for textfield
//        main1View.isUserInteractionEnabled = true
//        main1View.isMultipleTouchEnabled = true
//
//        //add pinch gesture
//        let pinchGesture = UIPinchGestureRecognizer(target: self, action:#selector(pinchRecognized(pinch:)))
//        pinchGesture.delegate = self
//        pinchGesture.cancelsTouchesInView = false
//        main1View.addGestureRecognizer(pinchGesture)
//
//        //add rotate gesture.
//        let rotate = UIRotationGestureRecognizer.init(target: self, action: #selector(handleRotate(recognizer:)))
//        rotate.delegate = self
//        rotate.cancelsTouchesInView = false
//        main1View.addGestureRecognizer(rotate)
//        self.view.layoutIfNeeded()
//    }
    
    func editCropFun(crImage: UIImage) {
        imgPhoto.isHidden = true
        viewMain.isHidden = false
        self.view.layoutIfNeeded()
        let main1View = UIView() //UIView()
        let image1 = UIImageView()
        
        let cropVW = UIView() //UIView()
        cropVW.backgroundColor = UIColor.clear
        
        cropVW.clipsToBounds = true
        btnPlusOutlet.titleLabel?.font = .boldSystemFont(ofSize: 20)
        
        main1View.backgroundColor = UIColor.clear
       
        
        image1.isUserInteractionEnabled = true
        image1.image = crImage
        image1.layer.masksToBounds = true
        image1.clipsToBounds = true
        
        main1View.backgroundColor = UIColor.clear
        image1.backgroundColor = UIColor.clear
        
        let heightInPoints = crImage.size.height
     //   let heightInPixels = heightInPoints * crImage.scale

        let widthInPoints = crImage.size.width
      //  let widthInPixels = widthInPoints * crImage.scale
        
        main1View.frame = CGRect.init(x: 0, y: 0, width: widthInPoints, height: heightInPoints)
        image1.frame = main1View.frame
        cropVW.frame = main1View.frame
        
        main1View.addSubview(image1)
        main1View.addSubview(cropVW)
        viewMain.addSubview(main1View)
        
        let TapgestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handleTap(_:)))
               TapgestureRecognizer.delegate = self
           //    TapgestureRecognizer.cancelsTouchesInView = false
               main1View.addGestureRecognizer(TapgestureRecognizer)
        
        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        gestureRecognizer.delegate = self
        gestureRecognizer.cancelsTouchesInView = false
        main1View.addGestureRecognizer(gestureRecognizer)
        
        //Enable multiple touch and user interaction for textfield
        main1View.isUserInteractionEnabled = true
        main1View.isMultipleTouchEnabled = true
        
        //add pinch gesture
        let pinchGesture = UIPinchGestureRecognizer(target: self, action:#selector(pinchRecognized(pinch:)))
        pinchGesture.delegate = self
        pinchGesture.cancelsTouchesInView = false
        main1View.addGestureRecognizer(pinchGesture)
        
        //add rotate gesture.
        let rotate = UIRotationGestureRecognizer.init(target: self, action: #selector(handleRotate(recognizer:)))
        rotate.delegate = self
        rotate.cancelsTouchesInView = false
        main1View.addGestureRecognizer(rotate)
        self.view.layoutIfNeeded()
    }
    
    func cropFun(crImage: UIImage, ShapLayer: CAShapeLayer) {
        viewMain.isHidden = false
        imgPhoto.isHidden = true
        
            self.view.layoutIfNeeded()
            let main1View = UIView() //UIView()
            let image1 = UIImageView()
            var buton1 = UIButton()
            
            let cropVW = UIView() //UIView()
            cropVW.backgroundColor = UIColor.clear
          
            cropVW.tag = imageDicArray.count
            cropVW.layer.borderWidth = 1
            cropVW.layer.borderColor = UIColor(red: 7.0/255, green: 181.0/255, blue: 208.0/255, alpha: 1.0).cgColor
            cropVW.clipsToBounds = true
            
            buton1 = UIButton(type: .system)
            buton1.frame = CGRect.init(x: cropVW.frame.origin.x, y: cropVW.frame.origin.y, width: 30, height: 30)
            buton1.setTitle("x", for: .normal)
            buton1.tag = imageDicArray.count
            buton1.layer.backgroundColor = UIColor(red: 7.0/255, green: 181.0/255, blue: 208.0/255, alpha: 1.0).cgColor
            buton1.setTitleColor(UIColor.white, for: .normal)
            buton1.layer.masksToBounds  = true
            buton1.layer.cornerRadius = buton1.frame.width/2
            btnPlusOutlet.titleLabel?.font = .boldSystemFont(ofSize: 20)
            //buton1.addTarget(self, action: #selector(btnCellDeleteImageAction(_:)), for: .touchUpInside)
            buton1.isUserInteractionEnabled = true
            buton1.isEnabled = true
            buton1.addTarget(self, action: #selector(btnCellDeleteImageAction(sender:)), for: .touchUpInside)
           
            main1View.backgroundColor = UIColor.clear
           
            
            image1.isUserInteractionEnabled = true
            image1.image = crImage
            
           
            image1.layer.masksToBounds = true
            image1.clipsToBounds = true
            
            main1View.tag = imageDicArray.count
            main1View.backgroundColor = UIColor.clear
            image1.backgroundColor = UIColor.clear
        
        let heightInPoints = crImage.size.height
     //   let heightInPixels = heightInPoints * crImage.scale

        let widthInPoints = crImage.size.width
      //  let widthInPixels = widthInPoints * crImage.scale
            
        main1View.frame = CGRect.init(x: 0, y: 0, width: widthInPoints, height: heightInPoints)
        image1.frame = main1View.frame
        cropVW.frame = main1View.frame
        
            main1View.addSubview(image1)
            main1View.addSubview(buton1)
            main1View.addSubview(cropVW)
            viewMain.addSubview(main1View)
            
            viewMain.bringSubview(toFront: main1View)
            main1View.bringSubview(toFront: buton1)
        
        let TapgestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handleTap(_:)))
               TapgestureRecognizer.delegate = self
           //    TapgestureRecognizer.cancelsTouchesInView = false
               main1View.addGestureRecognizer(TapgestureRecognizer)

            let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
            gestureRecognizer.delegate = self
            gestureRecognizer.cancelsTouchesInView = false
            main1View.addGestureRecognizer(gestureRecognizer)
            
            //Enable multiple touch and user interaction for textfield
            main1View.isUserInteractionEnabled = true
            main1View.isMultipleTouchEnabled = true
            
            //add pinch gesture
            let pinchGesture = UIPinchGestureRecognizer(target: self, action:#selector(pinchRecognized(pinch:)))
            pinchGesture.delegate = self
            pinchGesture.cancelsTouchesInView = false

            main1View.addGestureRecognizer(pinchGesture)
            
            //add rotate gesture.
            let rotate = UIRotationGestureRecognizer.init(target: self, action: #selector(handleRotate(recognizer:)))
            rotate.delegate = self
            rotate.cancelsTouchesInView = false

            main1View.addGestureRecognizer(rotate)
            
            self.view.layoutIfNeeded()
        }
    
//    func cropFun(crImage: UIImage, ShapLayer: CAShapeLayer) {
//        viewMain.isHidden = false
//        imgPhoto.isHidden = true
//
//        if let box : CGRect = (ShapLayer.path?.boundingBox) {
//            self.view.layoutIfNeeded()
//            let main1View = UIView() //UIView()
//            let image1 = UIImageView()
//            var buton1 = UIButton()
//
//            let cropVW = UIView() //UIView()
//            cropVW.backgroundColor = UIColor.clear
//            cropVW.frame = box
//            cropVW.tag = imageDicArray.count
//            cropVW.layer.borderWidth = 1
//            cropVW.layer.borderColor = UIColor.gray.cgColor
//            cropVW.clipsToBounds = true
//
//            buton1 = UIButton(type: .system)
//            buton1.frame = CGRect.init(x: cropVW.frame.origin.x, y: cropVW.frame.origin.y, width: 30, height: 30)
//            buton1.setTitle("x", for: .normal)
//            buton1.tag = imageDicArray.count
//            buton1.layer.backgroundColor = UIColor(red: 7.0/255, green: 181.0/255, blue: 208.0/255, alpha: 1.0).cgColor
//            buton1.setTitleColor(UIColor.white, for: .normal)
//            buton1.layer.masksToBounds  = true
//            buton1.layer.cornerRadius = buton1.frame.width/2
//            btnPlusOutlet.titleLabel?.font = .boldSystemFont(ofSize: 20)
//            //buton1.addTarget(self, action: #selector(btnCellDeleteImageAction(_:)), for: .touchUpInside)
//            buton1.isUserInteractionEnabled = true
//            buton1.isEnabled = true
//            buton1.addTarget(self, action: #selector(btnCellDeleteImageAction(sender:)), for: .touchUpInside)
//
//            main1View.frame = CGRect.init(x: 0, y: 0, width: crImage.size.width, height: crImage.size.height)
//            main1View.backgroundColor = UIColor.clear
//            image1.frame = CGRect.init(x: 0, y: 0, width: viewMain.frame.width, height: viewMain.frame.height)
//
//            image1.isUserInteractionEnabled = true
//            image1.image = crImage
//
//            var newShape = CAShapeLayer()
//            newShape = ShapLayer
//
//            image1.layer.mask = newShape
//            image1.layer.masksToBounds = true
//            image1.clipsToBounds = true
//
//            main1View.tag = imageDicArray.count
//            main1View.backgroundColor = UIColor.clear
//            image1.backgroundColor = UIColor.clear
//
//            main1View.frame = CGRect.init(x: 0, y: 0, width: viewMain.frame.width, height: viewMain.frame.height)
//            main1View.addSubview(image1)
//            main1View.addSubview(buton1)
//            main1View.addSubview(cropVW)
//            viewMain.addSubview(main1View)
//
//            viewMain.bringSubview(toFront: main1View)
//            main1View.bringSubview(toFront: buton1)
//
//            let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
//            gestureRecognizer.delegate = self
//            gestureRecognizer.cancelsTouchesInView = false
//            main1View.addGestureRecognizer(gestureRecognizer)
//
//            //Enable multiple touch and user interaction for textfield
//            main1View.isUserInteractionEnabled = true
//            main1View.isMultipleTouchEnabled = true
//
//            //add pinch gesture
//            let pinchGesture = UIPinchGestureRecognizer(target: self, action:#selector(pinchRecognized(pinch:)))
//            pinchGesture.delegate = self
//            pinchGesture.cancelsTouchesInView = false
//
//            main1View.addGestureRecognizer(pinchGesture)
//
//            //add rotate gesture.
//            let rotate = UIRotationGestureRecognizer.init(target: self, action: #selector(handleRotate(recognizer:)))
//            rotate.delegate = self
//            rotate.cancelsTouchesInView = false
//
//            main1View.addGestureRecognizer(rotate)
//
//            self.view.layoutIfNeeded()
//        }
            
//        else {
//           // self.showMessage(message: "Please Crop the image", completionHandler: nil)
//            let alert = UIAlertController(title: "Plus Me", message: "Please Crop the image", preferredStyle: .alert)
//
//            let action1 = UIAlertAction(title: "Delete", style: .default, handler: { (action) -> Void in
//            })
//
//            let action2 = UIAlertAction(title: "Ok", style: .default, handler: { (action) -> Void in
//            })
//
//                alert.addAction(action2)
//                alert.addAction(action1)
//           self.present(alert, animated: true, completion: nil)
//            return
//        }
 //   }
    
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//        return true
//    }
    
    
    func gestureRecognizer(_: UIGestureRecognizer,
                           shouldRecognizeSimultaneouslyWith shouldRecognizeSimultaneouslyWithGestureRecognizer:UIGestureRecognizer) -> Bool {
        return true
    }
    
     @objc func handleTap(_ TapgestureRecognizer: UITapGestureRecognizer)
     {
        viewMain.bringSubview(toFront: TapgestureRecognizer.view!)
    }
    
    @objc func handlePan(_ gestureRecognizer: UIPanGestureRecognizer) {
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            
            let translation = gestureRecognizer.translation(in: self.view)
            // note: 'view' is optional and need to be unwrapped
            gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x + translation.x, y: gestureRecognizer.view!.center.y + translation.y)
            gestureRecognizer.setTranslation(CGPoint.zero, in: self.view)
            viewMain.bringSubview(toFront: gestureRecognizer.view!)
            
        }
    }
    
    @objc func pinchRecognized(pinch: UIPinchGestureRecognizer) {
        
        if let view = pinch.view {
            view.transform = view.transform.scaledBy(x: pinch.scale, y: pinch.scale)
            pinch.scale = 1
            viewMain.bringSubview(toFront: pinch.view!)

        }
    }
    
    @objc func handleRotate(recognizer : UIRotationGestureRecognizer) {
        if let view = recognizer.view {
            view.transform = view.transform.rotated(by: recognizer.rotation)
            recognizer.rotation = 0
            viewMain.bringSubview(toFront: view)
        }
    }
    
//    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
//        sender?.view?.bringSubview(toFront: self.view)
//        sender?.view?.backgroundColor = UIColor.red
//    }
//
//    @objc func handlePinch(sender: UIPinchGestureRecognizer) {
////        if let view = recognizer.view {
//        let pinchScale: CGFloat = sender.scale
//        sender.view!.transform = sender.view!.transform.scaledBy(x: pinchScale, y: pinchScale)
//        sender.scale = 1.0
////        }
//    }
//
//    @objc func rotationGesture(sender:UIRotationGestureRecognizer)
//    {
//        isRotation = true
//        sender.view?.transform = ((sender.view?.transform)?.rotated(by: sender.rotation))!
//        sender.rotation = 0
//    }
    
}


/*
 extension UIImage {
 func crop( rect: CGRect) -> UIImage {
 var rect = rect
 rect.origin.x*=self.scale
 rect.origin.y*=self.scale
 rect.size.width*=self.scale
 rect.size.height*=self.scale
 
 let imageRef = self.cgImage!.cropping(to: rect)
 let image = UIImage(cgImage: imageRef!, scale: self.scale, orientation: self.imageOrientation)
 return image
 }
 }
 
 let myImage = UIImage(named: "Name")
 myImage?.crop(rect: CGRect(x: 0, y: 0, width: 50, height: 50))
 
 // For central
 
 let imageHeight = 100.0
 let width = 50.0
 let height = 50.0
 let origin = CGPoint(x: (imageWidth - width)/2, y: (imageHeight - height)/2)
 let size = CGSize(width: width, height: height)
 
 myImage?.crop(rect: CGRect(origin: origin, size: size))
 
 */

extension UIView {
    
    func takeScreenshot() -> UIImage {
        
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        
        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if (image != nil)
        {
            return image!
        }
        return UIImage()
    }
}
