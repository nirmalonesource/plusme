//
//  AppDelegate.swift
//  PlusMe
//
//  Created by Priyank Jotangiya on 20/07/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
// AIzaSyDYCSa5PlfE26GUx7kWXBjGU2813xM6nPE

import UIKit
import SWRevealViewController
import IQKeyboardManagerSwift
import OneSignal
import UserNotifications
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, OSSubscriptionObserver {

    var window: UIWindow?
    var viewController: SWRevealViewController?
    var getCloset = Bool()
    let kUserDefault = UserDefaults.standard
    //shared
    //Plus me_fashion_AdHoc
    class func shared() -> AppDelegate
    {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        (window?.rootViewController)?.self.callApi(Path.getNotificationList, method: .get, completionHandler: { (result) in
                            
                            print("Hey Result: \(result)")
                            if result.getBool(key: "status")
                            {
                                let arrNotification:NSArray = result.getArrayofDictionary(key: "data") as NSArray
                                
              
                               if arrNotification.count > 0 {
                                   UserDefaults.standard.set(String(arrNotification.count), forKey: "notification")
                               }
                               else
                               {
                                   UserDefaults.standard.removeObject(forKey: "notification")
                               }
                               
                            }
                           
                        })

//        StoredData.shared.getCategory()
        
        //PushMainView
        let mainStoryboardIpad = UIStoryboard(name: "Main", bundle: nil)
        let frontViewController :JoinViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "JoinViewController") as! JoinViewController
        StoredData.shared.rearViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "SideViewController") as? SideViewController
        let FrontNavigationController = UINavigationController.init(rootViewController: frontViewController)
        let revealController : SWRevealViewController = SWRevealViewController.init(rearViewController: StoredData.shared.rearViewController, frontViewController: FrontNavigationController)
        self.viewController = revealController
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = self.viewController
        self.window?.makeKeyAndVisible()

        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
                  // This block gets called when the user reacts to a notification received
                  let payload: OSNotificationPayload? = result?.notification.payload
                  
                  print("Message: ", payload!.body!)
                  print("badge number: ", payload?.badge ?? 0)
                  print("notification sound: ", payload?.sound ?? "No sound")
                  
                  if let additionalData = result!.notification.payload!.additionalData {
                      print("additionalData: ", additionalData)
                      
                      if let actionSelected = payload?.actionButtons {
                          print("actionSelected: ", actionSelected)
                      }
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let ProfileViewController = mainStoryboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                    let BlogDetailsVC = mainStoryboard.instantiateViewController(withIdentifier: "BlogDetailsVC") as! BlogDetailsVC
                    let BlogListDetailCommentView = mainStoryboard.instantiateViewController(withIdentifier: "BlogListDetailCommentView") as! BlogListDetailCommentView
                
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    
                    if additionalData.getInt(key: "type") == 6
                    {
                        
                        ProfileViewController.userId = "\(additionalData.getInt(key: "UserId"))"
                        ProfileViewController.isfromnotification = true
                        self.NavigateVC(controller: ProfileViewController)
                    }
                    
                        else if (additionalData.getInt(key: "type") == 8) || (additionalData.getInt(key: "type") == 9) || (additionalData.getInt(key: "type") == 12) {
                      
                            BlogDetailsVC.blogId = "\(additionalData.getInt(key: "ID"))"
                            BlogDetailsVC.isfromnotification = true
                            // nav.TYPE = "\(arrNotification[indexPath.row].getInt(key: "Type"))"
                            self.NavigateVC(controller: BlogDetailsVC)
                               }
                    else if additionalData.getInt(key: "type") == 10 {
            
                        BlogListDetailCommentView.postId1 = "\(additionalData.getInt(key: "ID"))"
                        BlogListDetailCommentView.dictData = additionalData as? [String:Any] ?? [:]
                        BlogListDetailCommentView.isfromnotification = true
                        self.NavigateVC(controller: BlogListDetailCommentView)
                    }
                        else if  (additionalData.getInt(key: "type") == 13){
                           
                            BlogListDetailCommentView.postId1 = "\(additionalData.getInt(key: "ID"))"
                            BlogListDetailCommentView.dictData = additionalData as? [String:Any] ?? [:]
                            BlogListDetailCommentView.isfromnotification = true
                            self.NavigateVC(controller: BlogListDetailCommentView)
                            }
                        
                    else
                    {
                        BlogDetailsVC.blogId = "\(additionalData.getInt(key: "ID"))"
                        BlogDetailsVC.TYPE = "\(additionalData.getInt(key: "type"))"
                        BlogDetailsVC.isfromnotification = true
                        self.NavigateVC(controller: BlogDetailsVC)
                    }
                    
                      // DEEP LINK from action buttons
                      if let actionID = result?.action.actionID {
                          // For presenting a ViewController from push notification action button
                          print("actionID = \(actionID)")
                      }
                  }
        }
        
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
//                                        appId: "4daad271-620c-499b-b959-546a3d399060",
                                        appId: "d9cc0ecb-15ff-4a1b-9bf6-eb66ac5a13a5",
                                        handleNotificationAction: notificationOpenedBlock,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
            
            if #available(iOS 10.0, *) {
                self.checkNotificationsAuthorizationStatus()
            } else {
                // Fallback on earlier versions
            }
        })
        
        let userId = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId
        if (userId != nil) {
            StoredData.shared.playerId = userId!
        }
               
        
        print("Current playerId \(userId)")
        
        IQKeyboardManager.sharedManager().enable = true
        
        OneSignal.add(self as OSSubscriptionObserver)
      
        //StatusBar
        UIApplication.shared.statusBarStyle = .default
        UINavigationBar.appearance().clipsToBounds = true
        
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        statusBar.backgroundColor = UIColor.white
        
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
               statusBar.backgroundColor = .white
               UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
                    statusBar.backgroundColor = UIColor.white
        }

      
        
        return true
    }
    
    func NavigateVC(controller:UIViewController)  {
        if StoredData.shared.isLogedin
        {
//            let navController = UINavigationController.init(rootViewController: controller)
//            //navController.isNavigationBarHidden = true
//            self.window?.rootViewController = navController
//            self.window?.makeKeyAndVisible()
            let mainStoryboardIpad = UIStoryboard(name: "Main", bundle: nil)
           // let frontViewController :JoinViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "JoinViewController") as! JoinViewController
            StoredData.shared.rearViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "SideViewController") as? SideViewController
            let FrontNavigationController = UINavigationController.init(rootViewController: controller)
            let revealController : SWRevealViewController = SWRevealViewController.init(rearViewController: StoredData.shared.rearViewController, frontViewController: FrontNavigationController)
            self.viewController = revealController
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = self.viewController
            self.window?.makeKeyAndVisible()
        }
      
    }
    
//    @available(iOS 10.0, *)
//    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//
//        completionHandler([.alert, .badge, .sound])
//
//        let userInfo:NSDictionary = notification.request.content.userInfo as NSDictionary
//        print(userInfo)
//        let dict:NSDictionary = userInfo["aps"] as! NSDictionary
//        let data:NSDictionary = dict["alert"] as! NSDictionary
//
//    }
    
    @available(iOS 10.0, *)
    private func checkNotificationsAuthorizationStatus() {
        let userNotificationCenter = UNUserNotificationCenter.current()
        userNotificationCenter.getNotificationSettings { (notificationSettings) in
            switch notificationSettings.authorizationStatus {
            case .authorized:
                print("The app is authorized to schedule or receive notifications.")
            case .denied:
                print("The app isn't authorized to schedule or receive notifications.")
                
            case .notDetermined:
                print("The user hasn't yet made a choice about whether the app is allowed to schedule notifications.")
            case .provisional:
                print("The application is provisionally authorized to post noninterruptive user notifications.")
                
            case .ephemeral:
                break
            }
        }
    }
    
     // After you add the observer on didFinishLaunching, this method will be called when the notification subscription property changes.
    
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
        }
        
        print("SubscriptionStateChange: \n\(String(describing: stateChanges))")
        
        //The player id is inside stateChanges. But be careful, this value can be nil if the user has not granted you permission to send notifications.
        if let playerId = stateChanges.to.userId {
            print("Current playerId \(playerId)")
            StoredData.shared.playerId = playerId
            //kUserDefault.set(playerId, forKey: "playerId")
            kUserDefault.synchronize()
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

