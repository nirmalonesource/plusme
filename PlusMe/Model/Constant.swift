//
//  Constant.swift
//  PlusMe
//
//  Created by rohit on 20/08/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//https://plusme.blenzabi.com/admin/postList

import Foundation

struct BasePath {
  //  static let path = "https://plusme.blenzabi.com/admin/api/web_service/"
    static let path = "https://plusmestyle.com/admin/api/web_service/"
   
}

struct Path
{
    static let register             = "\(BasePath.path)do_register"
    static let login                = "\(BasePath.path)login"
    static let forgotPassword       = "\(BasePath.path)ForgotPassword"
    static let logout               = "\(BasePath.path)logout"
    static let changePassword       = "\(BasePath.path)change_password"
    static let verifyLogin          = "\(BasePath.path)verify_otp"
    static let getUserProfile       = "\(BasePath.path)get_profile"
    static let editProfile          = "\(BasePath.path)edit_profile"
    static let myPost               = "\(BasePath.path)GetMyPosts"
    static let getBlog              = "\(BasePath.path)get_blog"
    static let categoryList         = "\(BasePath.path)GetCategoriesList"
    static let addToCloset          = "\(BasePath.path)AddToVisionBoard"
    static let getBlogDetail        = "\(BasePath.path)GetBlogDetail"
    static let addNewPost           = "\(BasePath.path)add_new_blog"
    static let myVisionBoardList    = "\(BasePath.path)GetMyVisionBoardList"
    static let getFollowing         = "\(BasePath.path)get_following"
    static let getFollowers         = "\(BasePath.path)get_followers"
    static let userFollow           = "\(BasePath.path)follow_unfollow_user"
    static let getStore             = "\(BasePath.path)GetStoreList"
    static let getMyFeed            = "\(BasePath.path)GetFeedsList" 
    static let addComment           = "\(BasePath.path)add_comment"
    static let getStylists          = "\(BasePath.path)GetStylists"
    static let getTrendsetrs        = "\(BasePath.path)GetTrendsetrs"
    static let getCountry           = "\(BasePath.path)GetCountry"
    static let getStates            = "\(BasePath.path)GetStates"
    static let getCity              = "\(BasePath.path)GetCity"
    static let likePost             = "\(BasePath.path)LikePost"
    static let moveToCloset         = "\(BasePath.path)AddToVisionBoard"
    static let addStore             = "\(BasePath.path)CompanyStore"
    static let MoveToVisionBoard    = "\(BasePath.path)MoveToVisionBoard"
    static let saveToCloset         = "\(BasePath.path)MoveToCloset"
    static let myClosetList        = "\(BasePath.path)GetMyClosetList"
    static let getNotificationList = "\(BasePath.path)GetNotificationList"
    static let allowNotification = "\(BasePath.path)AllowNotification"
    static let removeCloset = "\(BasePath.path)RemoveCloset"
    static let isPrivate = "\(BasePath.path)is_private"
    static let getBlogList = "\(BasePath.path)UserBlogsList"
    static let getBlogListDetail = "\(BasePath.path)UserBlogsListWithDetail"
    static let addBlogfeedComment = "\(BasePath.path)add_blogfeed_comment"
    static let addNewBlogFeed = "\(BasePath.path)add_new_blog_feed"
    static let getComment = "\(BasePath.path)get_comment"
    static let RemoveVisionBoard = "\(BasePath.path)RemoveVisionBoard"
    static let BlogReport = "\(BasePath.path)BlogReport"
    static let RemoveBlog = "\(BasePath.path)RemoveBlog"
    static let getblogfeed = "\(BasePath.path)get_blog_feed"
    static let RemovePost = "\(BasePath.path)RemovePost"
 

//    static let _ = "\(BasePath.path)" get_comment
//    static let _ = "\(BasePath.path)"
//    static let _ = "\(BasePath.path)"
}
