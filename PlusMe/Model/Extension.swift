//
//  Extension.swift
//  PlusMe
//
//  Created by rohit on 06/08/18.
//  Copyright © 2018 Priyank Jotangiya. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import NVActivityIndicatorView

enum APIType: String {
//      case options, get, head, post, put, patch, delete, trace, connect
    case options = "options"
    case get     = "get"
    case head    = "head"
    case post    = "post"
    case put     = "put"
    case patch   = "patch"
    case delete  = "delete"
    case trace   = "trace"
    case connect = "connect"
}

extension UIViewController: NVActivityIndicatorViewable
{
    func showMessage(message msg: String, completionHandler: (() -> Void)?)
    {
        let alert = UIAlertController(title: "PlusMe", message: msg
            , preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            completionHandler?()
        }))
        self.present(alert, animated: true, completion: nil)
    }

    func callApi(_ urlString: String,
                 method: HTTPMethod,
                 param: [String:Any] = [String: Any](),
                 message: String = "Loading...",
                 withLoader: Bool = true,
                 withHeader: Bool = true,
                 data: [Data] = [Data](),
                 dataKey: [String] = [String](),
                 completionHandler: @escaping ([String: Any]) -> ())
    {
        guard let url = URL(string: urlString) else { return }

        print("url: \(url)")
        print("param : \(param)")
        print("method : \(method)")
        if isReachabel
        {
            if withLoader
            {
                showProgressHud(message: message)
            }

            var header: HTTPHeaders? = nil
            if withHeader
            {
                header = ["Auth": StoredData.shared.Auth]
            }
            print("header : \(String(describing: header))")

            if dataKey.count > 0
            {
                Alamofire.upload(
                    multipartFormData: { multipartFormData in

                        for (key, value) in param
                        {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                        }
                        
                        for i in 0..<data.count
                        {
                            multipartFormData.append(data[i], withName: dataKey[i], fileName: dataKey[i] + ".png", mimeType: "image/png")
                        }
                },
                    to: urlString,
                    headers: header,
                    encodingCompletion: { encodingResult in

                        switch encodingResult {
                        case .success(let upload, _, _):
                            upload.responseJSON { result in

                                if withLoader
                                {
                                    self.hideProgressHud()
                                }

                                print(result)
                                print(result.result)
                                if let httpError = result.result.error
                                {
                                    print(NSString(data: result.data!, encoding: String.Encoding.utf8.rawValue) ?? "Exception")
                                    print(httpError._code)
                                }
                                
                                if result.result.isSuccess
                                {
                                    let response1 = result.result.value as! [String:Any]
                                    completionHandler(response1)
                                }
                            }
                        case .failure(let encodingError):
                            print(encodingError)
                        }
                })
            }
                
            else
            {
                Alamofire.request(url,
                                  method: method,
                                  parameters: param,
                                  headers: header)
                    .responseJSON { (result) in
                        if withLoader
                        {
                            self.hideProgressHud()
                        }
                        
                        if let httpError = result.result.error
                        {
                            print(NSString(data: result.data!, encoding: String.Encoding.utf8.rawValue) ?? "")
                            print(httpError._code)
                        }
                            
                        else  if  result.result.isSuccess
                        {
                            let response = result.result.value as! [String:Any]
                            if !response.getBool(key: "status") && response.getInt(key: "error_code") == -1
                            {
                                print(response)
                                self.showMessage(message: response.getString(key: "message"), completionHandler: {
                                    StoredData.shared.rearViewController.logout()
                                })
                            }
                                
                            else
                            {
                                completionHandler(response)
                            }
                        }
                }
            }
        }
    }

    func showProgressHud(message msg: String)
    {
        let size = CGSize(width: 50, height:50)
        startAnimating(size, message: msg, type: NVActivityIndicatorType(rawValue: 3)!)
    }

    func hideProgressHud()
    {
        stopAnimating()
    }

    var isReachabel: Bool
    {
        return NetworkReachabilityManager()!.isReachable
    }

    func addGesture(_ view: UIView, userId: Int)
    {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGesterHandeler(_:)))
        view.tag = userId
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(tapGesture)
    }

    @objc func tapGesterHandeler(_ sender: UITapGestureRecognizer)
    {
        let userID = sender.view!.tag

        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.userId = "\(userID)"
        navigationController?.pushViewController(vc, animated: true)
//        let navigationController = UINavigationController(rootViewController: vc)
//        StoredData.shared.re revealViewController()?.pushFrontViewController(navigationController, animated: true)
    }

}

extension UITextField
{
    func ChangePlaceholderColor(placeholder : String)
    {
        self.attributedPlaceholder = NSAttributedString(
            string: placeholder,
            attributes: [NSAttributedStringKey.foregroundColor: UIColor.init(red: 232/255.0, green: 59/255.0, blue: 119/255.0, alpha: 1)])
//        self.textColor = UIColor.init(red: 232/255.0, green: 59/255.0, blue: 119/255.0, alpha: 1)
    }
}


//MARK:- Image

extension UIImageView
{
    func changeImageViewImageColor(color : UIColor)
    {
        self.image = self.image!.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
    }
}

extension UIView
{
    func showEmptyMessage(message: String)
    {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 210, height: 21))
        label.text = message
        label.textColor = .black
        label.textAlignment = .center
        label.center = self.center
        self.addSubview(label)
    }

    func roundView(_ borderColor: UIColor = .lightGray, borderWidth: CGFloat = 0.5)
    {
        self.layer.borderWidth = borderWidth
        self.layer.masksToBounds = false
        self.layer.borderColor = borderColor.cgColor
        self.layer.cornerRadius = self.frame.size.height/2
        self.clipsToBounds = true
    }

    func roundCorner()
    {
        self.layer.borderWidth = 0.5
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.cornerRadius = 20
        self.clipsToBounds = true
    }

    func setBorder()
    {
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 0.5
        self.clipsToBounds = true
    }
    
    func roundCorner1()
    {
        self.layer.borderWidth = 0.5
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
    }

}

extension UIImage {

    func resize(scale:CGFloat)-> UIImage {
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: size.width*scale, height: size.height*scale)))
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.image = self
        UIGraphicsBeginImageContext(imageView.bounds.size)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
}

extension UIDevice {

    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }

        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod Touch 5"
            case "iPod7,1":                                 return "iPod Touch 6"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad6,11", "iPad6,12":                    return "iPad 5"
            case "iPad7,5", "iPad7,6":                      return "iPad 6"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }

        return mapToDevice(identifier: identifier)
    }()

}

extension UIImageView
{
    func setImage(_ url: String)
    {
        let url = URL(string: url)

        self.kf.setImage(with: url,
                     placeholder: nil,
                 //      placeholder: UIImage(named: "logo_without_bg"),
                     options: [.transition(ImageTransition.fade(1))],
                     progressBlock: { receivedSize, totalSize in
        },
                     completionHandler: { image, error, cacheType, imageURL in
        })
    }
}

extension String
{
    
        var decodeEmoji: String {
            if let encodeStr = NSString(cString: self.cString(using: .isoLatin1) ?? [Int8]() , encoding: String.Encoding.utf8.rawValue){
                      return encodeStr as String
                  }
                  return self
            
//                 NSString *ConvertedString = [NSString stringWithCString:[str cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
            
//          let ConvertedString = String(
//            cString: self.cString(using: String.Encoding(rawValue: String.Encoding.isoLatin1.rawValue))! ,
//            encoding: .utf8) ?? ""
            
           
        }
    
    var encodeEmoji: String{
      
        let data = self.data(using: String.Encoding.utf8);
                   let decodedStr = NSString(data: data!, encoding: String.Encoding.isoLatin1.rawValue)
                   if let str = decodedStr{
                       return str as String
                   }
                   return self
        
//        let dataenc = self.data(using: .utf8)
//        var encodevalue: String? = nil
//        if let dataenc = dataenc {
//            encodevalue = String(
//                data: dataenc,
//                encoding: .isoLatin1)
//        }
//        return encodevalue ?? ""
    }
    
    
    func validPhoneNo() -> Bool
    {
        let phoneRegEx = "[1-9][0-9]{9,14}"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegEx)
        return phoneTest.evaluate(with: self)
    }

    func isEmpty() -> Bool
    {
        let strText = self.trimmingCharacters(in: CharacterSet.whitespaces)
        return strText.isEmpty
    }

    func trimming() -> String
    {
        let strText = self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return strText
    }
    
    func isValidEmail() -> Bool {

        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"

        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)

        return emailTest.evaluate(with: self)
    }

    func toDate() -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: self)!
    }
}

func convertDateString(dateString : String!, fromFormat sourceFormat : String!, toFormat desFormat : String!) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = sourceFormat
    let date = dateFormatter.date(from: dateString) ?? Date()
    dateFormatter.dateFormat = desFormat
    return dateFormatter.string(from: date)
}

extension Date
{
    func timeAgoSinceDate() -> String {

        let calendar = NSCalendar.current
        let date1 = calendar.startOfDay(for: self)
        let date2 = calendar.startOfDay(for: Date())

        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]

        let components = calendar.dateComponents(unitFlags, from: date1, to: date2)// components(flags,
        if (components.year! >= 2)
        {
            return "\(components.year!) years ago"
        }
        else if (components.year! >= 1)
        {
            return "1 year ago"
        }
        else if (components.month! >= 2)
        {
            return "\(components.month!) months ago"
        }
        else if (components.month! >= 1)
        {
            return "1 month ago"
        }
        else if (components.weekOfYear! >= 2)
        {
            return "\(components.weekOfYear!) weeks ago"
        }
        else if (components.weekOfYear! >= 1)
        {
            return "1 week ago"
        }
        else if (components.day! > 1 && components.day! < 7)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEEE"
            return "\(dateFormatter.string(from: self))"
        }
        else if (components.day! >= 2)
        {
            return "\(components.day!) days ago"
        }
        else if (components.day! == 1)
        {
            return "Yesterday"
        }
        else
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            return "\(dateFormatter.string(from: self))"
        }
    }

    func timeAgoSinceDate1() -> String
    {
        let calendar = NSCalendar.current

        let date1 = calendar.startOfDay(for: self)
        let date2 = calendar.startOfDay(for: Date())

        let unitFlags: Set<Calendar.Component> = [.day]
        //        let flags = NSCalendar.Unit.day
        let components = calendar.dateComponents(unitFlags, from: date1, to: date2)// components(flags, fromDate: date1, toDate: date2, options: [])

        if (components.day! >= 7)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM MM, yyyy"
            return "\(dateFormatter.string(from: self))"
        }
        else if (components.day! > 1)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEEE"
            return "\(dateFormatter.string(from: self))"
        }
        else if (components.day! == 1)
        {
            return "Yesterday"
        }
        else
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            return "\(dateFormatter.string(from: self))"
        }
    }

    func timeAgoSinceDate2() -> String
    {
        let calendar = NSCalendar.current

        let date1 = calendar.startOfDay(for: self)
        let date2 = calendar.startOfDay(for: Date())

        let unitFlags: Set<Calendar.Component> = [.day]
        //        let flags = NSCalendar.Unit.day
        let components = calendar.dateComponents(unitFlags, from: date1, to: date2)// components(flags, fromDate: date1, toDate: date2, options: [])

        if (components.day! >= 7)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMMM MM, yyyy"
            return "\(dateFormatter.string(from: self))"
        }
        else if (components.day! > 1)
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEEE"
            return "\(dateFormatter.string(from: self))"
        }
        else if (components.day! == 1)
        {
            return "Yesterday"
        }
        else
        {
            return "Today"
        }
    }

    func time() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        return "\(dateFormatter.string(from: self))"
    }
}
