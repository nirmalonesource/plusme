//
//  StoredData.swift
//  MatchPlay
//
//  Created by rohit on 10/08/18.
//  Copyright © 2018 rohit. All rights reserved.
//

import UIKit

class StoredData: NSObject {

    static let shared = StoredData()

    var rearViewController :SideViewController!
    
    var userId: String
    {
        return UserDefaults.standard.value(forKey: "Id") as? String ?? ""
    }
    
    var email: String
    {
        return UserDefaults.standard.value(forKey: "Email") as! String
    }

    var name: String
    {
        return (UserDefaults.standard.value(forKey: "FirstName") as! String) + " " + (UserDefaults.standard.value(forKey: "LastName") as! String)
    }

    var userProfile: String
    {
        return UserDefaults.standard.value(forKey: "UserProfileImage") as? String ?? ""
    }

    var userType: String
    {
        return UserDefaults.standard.value(forKey: "RegisterUserType") as! String
    }

    var Auth: String
    {
        return UserDefaults.standard.value(forKey: "Auth") as? String ?? ""
    }

    var udid: String
    {
        return UIDevice.current.identifierForVendor!.uuidString
    }

    var isLogedin: Bool
    {
        return UserDefaults.standard.value(forKey: "Id") != nil
    }

    var arrCategoryList = [[String: Any]]()
    func getCategory(completion: @escaping ([[String: Any]]) -> ())
    {
        if arrCategoryList.count > 0
        {
            completion(arrCategoryList)
        }
        else
        {
            UIViewController().callApi(Path.categoryList, method: .get) { (result) in

                print(result)
                if result.getBool(key: "success")
                {
                    self.arrCategoryList = result.getArrayofDictionary(key: "data")
                    completion(self.arrCategoryList)
                }
                else
                {
                    completion([[String: Any]]())
                }
            }
        }
    }

    var selectedCateogryId = 0
    var selectedCateogryIndex = 0

    var isEditClosetImage = false
    var isfrommycloset = false
    var closetImage: UIImage!
    var isAllowNotification = false
    var isPrivate = false
    var deviceToken: String!
    var ClosetID: String!
    var playerId: String!

    //    {
//        set { self.deviceToken = newValue }
//        get { return self.deviceToken }
//    }
}
