//
//  CollageFilterViewController.swift
//  Collage
//
//  Created by Soham Bhattacharjee on 15/02/17.
//  Copyright © 2017 Soham Bhattacharjee. All rights reserved.
//

import UIKit
import PKHUD

class CollageFilterViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    // MARK: Variables
    var imageArray: [UIImage] = []
    var imageViewArray: [CollageImageView] = []
    var collageType: Int = 0

    var selectedCategoryId = 0
    var picker: UIPickerView!
    var arrCategoryItem = [String]()
    var arrCategory = [[String: Any]]()

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var viewSavePopup: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var txtCategory: UITextField!
    @IBOutlet weak var btnMoveToCloset: UIButton!

    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if collageType != 0 {
            updateUI()
        }


        txtCategory.layer.borderColor = #colorLiteral(red: 0.9294117647, green: 0.2196078431, blue: 0.462745098, alpha: 1)
        txtCategory.layer.borderWidth = 1
        picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        txtCategory.inputView = picker
        txtCategory.delegate = self

        btnMoveToCloset.roundCorner()

        StoredData.shared.getCategory { (result) in
            self.arrCategoryItem.removeAll()
            self.arrCategory = result
            for item in result
            {
                self.arrCategoryItem.append(item.getString(key: "CategoryName"))
            }
            self.picker.reloadAllComponents()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UI Update
    func updateUI() {
        
        title = "your Collage"
        let leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(back))
        let rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveCollage))
        navigationItem.leftBarButtonItem = leftBarButtonItem
        navigationItem.rightBarButtonItem = rightBarButtonItem
        
        if let collageView = CollageView.loadFromNibNamed(nibNamed: "CollageType\(collageType)", bundle: nil) as? CollageView {
            collageView.frame = CGRect(x: 0.0, y: 0.0, width: containerView.frame.size.width, height: containerView.frame.size.height)
            collageView.btnClick.isHidden = true
            containerView.addSubview(collageView)
            insertImage(view: collageView)
        }
    }
    @objc func back() {
        _ = navigationController?.popViewController(animated: true)
    }
    @objc  func saveCollage() {


        let actionSheet = UIAlertController(title: "Save to visionboard?", message: nil, preferredStyle: .actionSheet)

        actionSheet.addAction(UIAlertAction(title: "Save to Visionboard", style: .default, handler: { (action) in


            let image = self.containerView.getSnapshotImage()
            self.imageView.image = image
            self.viewSavePopup.isHidden = false


        }))


        actionSheet.addAction(UIAlertAction(title: "Save to MyCloset", style: .default, handler: { (action) in

//            let param: [String: Any] = [
//                //                "BaseImageID": self.closetImageId,
//                :]
            
            
            let param: [String: Any] = [
                               //  "BaseImageID": self.closetImageId,
                                 "Type" : "IMAGE"
                                 ]
            let image = self.containerView.getSnapshotImage()

            self.callApi(Path.saveToCloset, method: .post, param: param, data: [UIImagePNGRepresentation((image.resize(scale: 0.80)))!], dataKey: ["PostImage"]) { (result) in

                print(result)
                if result.getBool(key: "success")
                {
                    self.showMessage(message: result.getString(key: "message"), completionHandler: {
//                        self.viewSavePopup.isHidden = true
                        for vc in self.navigationController!.viewControllers.reversed()
                        {
                            if vc is VisionBoardViewController
                            {
                                self.navigationController?.popToViewController(vc, animated: true)
                            }
                        }
                    })
                }
                else
                {
                    self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                }
            }

        }))


        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in

        }))

        present(actionSheet, animated: true, completion: nil)
    }

    @IBAction func btnHideAction(_ sender: UIButton) {

        viewSavePopup.isHidden = true
    }
    @IBAction func btnMoveToClosetAction(_ sender: UIButton) {

        if txtCategory.text!.isEmpty()
        {
            showMessage(message: "Please select cetegory.", completionHandler: nil)
        }
        else
        {
            let param: [String: Any] = [
                "CategoryID": selectedCategoryId,
                 "Type" : "IMAGE"
                ]

            callApi(Path.MoveToVisionBoard, method: .post, param: param, data: [UIImagePNGRepresentation((imageView.image?.resize(scale: 0.80))!)!], dataKey: ["PostImage"]) { (result) in

                print(result)
                if result.getBool(key: "success")
                {
                    self.txtCategory.text = ""
                    self.selectedCategoryId = 0
                    self.showMessage(message: result.getString(key: "message"), completionHandler: {
                        self.viewSavePopup.isHidden = true
                        for vc in self.navigationController!.viewControllers.reversed()
                        {
                            if vc is VisionBoardViewController
                            {
                                self.navigationController?.popToViewController(vc, animated: true)
                            }
                        }
                    })
                }
                else
                {
                    self.showMessage(message: result.getString(key: "message"), completionHandler: nil)
                }
            }
        }
    }

    func saveToGallary()
    {
        let image = containerView.getSnapshotImage()
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        //        HUD.flash((.label("Image saved to Camera Roll")), delay: 1)

        // get the documents directory url
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        print(documentsDirectory)
        // choose a name for your image
        let fileName = "\(Date().timeIntervalSince1970).jpg"
        // create the destination file url to save your image
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        // get your UIImage jpeg data representation and check if the destination file url already exists
        if let data = UIImageJPEGRepresentation(image, 1.0),
            !FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                // writes the image data to disk
                try data.write(to: fileURL)
                print("file saved")
                self.showMessage(message: "File saved successfully.", completionHandler: {
                    for vc in self.navigationController!.viewControllers.reversed()
                    {
                        if vc is VisionBoardViewController
                        {
                            self.navigationController?.popToViewController(vc, animated: true)
                        }
                    }
                })
            } catch {
                print("error saving file:", error)
            }
        }

    }
    func insertImage(view: CollageView) {
        getSubviewsOfView(v: view)
        
        if imageArray.count == imageViewArray.count {
            for (index, image) in imageArray.enumerated() {
                let imageViewSize = imageViewArray[index].frame.size
                if imageViewSize.width > image.size.width {
                    imageViewArray[index].contentMode = .scaleAspectFit
                }
                else {
                    imageViewArray[index].contentMode = .scaleAspectFill
                }
                imageViewArray[index].image = image
            }
            
        }
        print(imageViewArray)
    }
    func getSubviewsOfView(v: UIView) {
        for subview in v.subviews {
            if subview is CollageImageView {
                imageViewArray.append(subview as! CollageImageView)
            }
            else {
                getSubviewsOfView(v: subview)
            }
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        return arrCategoryItem.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrCategoryItem[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        txtCategory.text = arrCategoryItem[row]
        selectedCategoryId = arrCategory[row].getInt(key: "CategoryID")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
